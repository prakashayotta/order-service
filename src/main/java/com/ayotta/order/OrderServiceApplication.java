package com.ayotta.order;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication; 
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.ayotta.order.config.MessageQProperties;
import com.sun.messaging.ConnectionConfiguration;
import com.sun.messaging.ConnectionFactory;

@SpringBootApplication
public class OrderServiceApplication {

	@Autowired
	MessageQProperties properties;
	
	public static void main(String[] args) {
		SpringApplication.run(OrderServiceApplication.class, args);
	}
	
	@Bean
	public ConnectionFactory connection() {
		ConnectionFactory remoteJms = new ConnectionFactory();
		try {
			remoteJms.setProperty(ConnectionConfiguration.imqAddressList, properties.getHost());
			remoteJms.setProperty(ConnectionConfiguration.imqReconnectEnabled, "true");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return remoteJms;
	}

}
