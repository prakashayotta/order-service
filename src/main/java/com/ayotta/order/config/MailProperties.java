package com.ayotta.order.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MailProperties {

	@Value("${mail.aog.to}")
	private String mailAogTo;
	@Value("${mail.from}")
	private String mailFrom;
	@Value("${schema.mail.to}")
	private String schemaMailTo;  
	@Value("${spring.mail.host}")
	private String mailHost;
	
	public String getMailAogTo() {
		return mailAogTo;
	}
	public void setMailAogTo(String mailAogTo) {
		this.mailAogTo = mailAogTo;
	}
	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
	public String getSchemaMailTo() {
		return schemaMailTo;
	}
	public void setSchemaMailTo(String schemaMailTo) {
		this.schemaMailTo = schemaMailTo;
	}
	public String getMailHost() {
		return mailHost;
	}
	public void setMailHost(String mailHost) {
		this.mailHost = mailHost;
	}
	
	
	
}
