package com.ayotta.order.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageQProperties {

	@Value("${imq.name}")
	private String outQName;
	
	@Value("${imq.host}")
	private String host;
	
	
	public String getOutQName() {
		return outQName;
	}
	public void setOutQName(String outQName) {
		this.outQName = outQName;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
}
