package com.ayotta.order.model;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class CustomOrderValidator implements ConstraintValidator<CustomOrderValidation,Order> {

	private static final Logger logger = LoggerFactory.getLogger(CustomOrderValidator.class);
	
	@Override
	public void initialize(CustomOrderValidation constraintAnnotation) {
		logger.debug("CustomOrderValidation :" + constraintAnnotation);
	}
	
	@Override
	public boolean isValid(Order value, ConstraintValidatorContext context) {
		// TODO Auto-generated method stub
		logger.debug("ORDER OBJ :" + value.toString());
		return false;
	}

}
