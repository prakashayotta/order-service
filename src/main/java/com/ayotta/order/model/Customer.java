package com.ayotta.order.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@Entity
@Table(name = "eta_sap_customers")
@NamedNativeQueries({
	@NamedNativeQuery(query = " select distinct c.active_flag , c.customer_id id, c.customer_name name, "
			+" c.customer_account_number account_no, c.dropshipment_allowed_flag dropShipAllowedFlag "
		    +" from eta_sap_customers c, "
		    +" eta_sap_c_relation r "
		    +" where upper(c.customer_name) like :customerName "
		    +" and upper(c.customer_number) like :customerNumber "
		    +" and    c.customer_id = r.customer_id "
		    +" and    r.site_type   = 'SOLD_TO' " 
		    +" order by c.customer_name asc "
	, name = "findEtaSapCust", resultSetMapping="etaSapCustMapping"),
})
@SqlResultSetMapping(name = "etaSapCustMapping", entities = @EntityResult(
	entityClass = Customer.class, 
	fields = {
			@FieldResult(name = "activeFlag", column = "active_flag"),
			@FieldResult(name = "customerId", column = "id"),
			@FieldResult(name = "customerName", column = "name"),
			@FieldResult(name = "customerNumber", column = "account_no"),
			@FieldResult(name = "dropshipmentAllowedFlag", column = "dropShipAllowedFlag"),
	}
))
public class Customer {
	
//	@Column (name="SOLAR_CUSTOMER_NUMBER") String solarCustomerNumber;
//	@Column (name="SOLAR_CUSTOMER_NAME") String solarCustomerName;
//	@Column (name="LAST_UPDATE_DATE") Timestamp lastUpdateDate;
//	@Column (name="CREATED_BY") String createdBy;
//	@Column (name="CREATION_DATE") Timestamp creationDate;
	@Column (name="ACTIVE_FLAG") String activeFlag;
//	@Column (name="TAX_REFERENCE") String taxReference;
//	@Column (name="COUNTRY") String country;
//	@Column (name="CREDIT_HOLD") String creditHold;
//	@Column (name="CREDIT_LIMIT_CURR") String creditLimitCurr;
//	@Column (name="CREDIT_LIMIT") Long creditLimit;
//	@Column (name="PAYMENT_TERMS") String paymentTerms;
	@Column (name="DROPSHIPMENT_ALLOWED_FLAG") String dropshipmentAllowedFlag;
//	@Column (name="CUSTOMER_ACCOUNT_NUMBER") String customerAccountNumber;
	@Column (name="CUSTOMER_NUMBER") String customerNumber;
	@Column (name="CUSTOMER_NAME") String customerName;
//	@Column (name="DELEGATED_PARTNER_REF") String delegatedPartRef;
	@Id
	@Column (name="CUSTOMER_ID") Long customerId;
	public String getCustomerName() {
		return customerName + "("+ customerNumber+ ")";
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
//	public String getSolarCustomerNumber() {
//		return solarCustomerNumber;
//	}
//	public void setSolarCustomerNumber(String solarCustomerNumber) {
//		this.solarCustomerNumber = solarCustomerNumber;
//	}
//	public String getSolarCustomerName() {
//		return solarCustomerName;
//	}
//	public void setSolarCustomerName(String solarCustomerName) {
//		this.solarCustomerName = solarCustomerName;
//	}
//	public Timestamp getLastUpdateDate() {
//		return lastUpdateDate;
//	}
//	public void setLastUpdateDate(Timestamp lastUpdateDate) {
//		this.lastUpdateDate = lastUpdateDate;
//	}
//	public String getCreatedBy() {
//		return createdBy;
//	}
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}
//	public Timestamp getCreationDate() {
//		return creationDate;
//	}
//	public void setCreationDate(Timestamp creationDate) {
//		this.creationDate = creationDate;
//	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
//	public String getTaxReference() {
//		return taxReference;
//	}
//	public void setTaxReference(String taxReference) {
//		this.taxReference = taxReference;
//	}
//	public String getCountry() {
//		return country;
//	}
//	public void setCountry(String country) {
//		this.country = country;
//	}
//	public String getCreditHold() {
//		return creditHold;
//	}
//	public void setCreditHold(String creditHold) {
//		this.creditHold = creditHold;
//	}
//	public String getCreditLimitCurr() {
//		return creditLimitCurr;
//	}
//	public void setCreditLimitCurr(String creditLimitCurr) {
//		this.creditLimitCurr = creditLimitCurr;
//	}
//	public Long getCreditLimit() {
//		return creditLimit;
//	}
//	public void setCreditLimit(Long creditLimit) {
//		this.creditLimit = creditLimit;
//	}
//	public String getPaymentTerms() {
//		return paymentTerms;
//	}
//	public void setPaymentTerms(String paymentTerms) {
//		this.paymentTerms = paymentTerms;
//	}
	public String getDropshipmentAllowedFlag() {
		return dropshipmentAllowedFlag;
	}
	public void setDropshipmentAllowedFlag(String dropshipmentAllowedFlag) {
		this.dropshipmentAllowedFlag = dropshipmentAllowedFlag;
	}
//	public String getCustomerAccountNumber() {
//		return customerAccountNumber;
//	}
//	public void setCustomerAccountNumber(String customerAccountNumber) {
//		this.customerAccountNumber = customerAccountNumber;
//	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
//	public String getDelegatedPartRef() {
//		return delegatedPartRef;
//	}
//	public void setDelegatedPartRef(String delegatedPartRef) {
//		this.delegatedPartRef = delegatedPartRef;
//	}
	
	
}
