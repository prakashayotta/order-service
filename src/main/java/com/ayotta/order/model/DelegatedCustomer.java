package com.ayotta.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="")
@NamedNativeQueries({
	@NamedNativeQuery(query = "SELECT C.CUSTOMER_NAME,C.CUSTOMER_ID ,C.DELEGATED_PARTNER_REF CIC "
			+ " FROM ETA_SAP_CUSTOMERS C, ETA_DELEGATED_PARTNERS P "
			+ " WHERE P.ETA_ENDUSER_ID = C.CUSTOMER_ID AND P.ETA_CUSTOMER_ID = :customerId "
			+ " AND UPPER(C.CUSTOMER_NAME) LIKE :custName AND UPPER(C.DELEGATED_PARTNER_REF) LIKE :cic "
			+ " AND (VALID_FROM <= TRUNC(SYSDATE) OR VALID_FROM IS NULL) AND (VALID_TO >= TRUNC(SYSDATE) OR VALID_TO IS NULL)  "
	, name = "findDelegatedCust", resultSetMapping="delegatedCustMapping"),
})
@SqlResultSetMapping(name = "delegatedCustMapping", entities = @EntityResult(
	entityClass = DelegatedCustomer.class, 
	fields = {
			@FieldResult(name = "customerName", column = "CUSTOMER_NAME"),
			@FieldResult(name = "customerId", column = "CUSTOMER_ID"),
			@FieldResult(name = "delegatedPartRef", column = "CIC")

	}
))
public class DelegatedCustomer {

	@Id
	@ApiModelProperty(notes = "Customer Id cannot be empty",required = true)
	@Column (name="CUSTOMER_ID") Long customerId;
	
	@ApiModelProperty(notes = "Enter the delegated customer name")
	@Column (name="CUSTOMER_NAME") String customerName;
	
	@ApiModelProperty(notes = "Enter the CIC code")
	@Column (name="CIC") String delegatedPartRef;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getDelegatedPartRef() {
		return delegatedPartRef;
	}
	public void setDelegatedPartRef(String delegatedPartRef) {
		this.delegatedPartRef = delegatedPartRef;
	}
}
