package com.ayotta.order.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;


@Entity
@Table(name = "")
@NamedNativeQueries({
	@NamedNativeQuery(query = "SELECT   ES.*,EBS.BLOCKED_CODE BLOCKED_STATUS, EC.ORGANIZATION FROM   ETA_SAP_C_SALES ES, eta_cage_codes EC, " +
			  "  ETA_CUST_BLOCKED_STATUSES EBS WHERE   EC.CSCORGID = ES.ORGANIZATION_ID AND EC.DIVISION = ES.DIVISION_LINE " +
			  " AND ES.BLOCKED_STATUS_ID = EBS.BLOCKED_STATUS_ID(+) AND ES.CUSTOMER_ID = :customerId "
	, name = "findSapCSales", resultSetMapping="etaSapCSalesMapping"),
})
@SqlResultSetMapping(name = "etaSapCSalesMapping", entities = @EntityResult(
	entityClass = EtaSapCSales.class, 
	fields = {
			@FieldResult(name = "custSalesId", column = "CUST_SALES_ID"),
			@FieldResult(name = "customerId", column = "CUSTOMER_ID"),
			@FieldResult(name = "organizationId", column = "ORGANIZATION_ID"),
			@FieldResult(name = "distributionChannel", column = "DISTRIBUTION_CHANNEL"),
			@FieldResult(name = "divisionLine", column = "DIVISION_LINE"),
			@FieldResult(name = "civilMilitaryIndicator", column = "CIVIL_MILITARY_INDICATOR"),
			@FieldResult(name = "customerClass", column = "CUSTOMER_CLASS"),
			@FieldResult(name = "priceList", column = "PRICE_LIST"),
			@FieldResult(name = "keyCustomer", column = "KEY_CUSTOMER"),
			@FieldResult(name = "paymentTerms", column = "PAYMENT_TERMS"),
			@FieldResult(name = "defaultPlant", column = "DEFAULT_PLANT"),
			@FieldResult(name = "activeFlag", column = "ACTIVE_FLAG"),
			@FieldResult(name = "sourceSystem", column = "SOURCE_SYSTEM"),
			@FieldResult(name = "blockedStatus", column = "BLOCKED_STATUS"),
			@FieldResult(name = "organization", column = "ORGANIZATION"),
	}
))
public class EtaSapCSales {

	@Id
	@Column(name = "CUST_SALES_ID") 
	private Long custSalesId ;
	@Column(name = "CUSTOMER_ID") 
	private String customerId ;
	@Column(name = "ORGANIZATION_ID") 
	private String organizationId ;
	@Column(name = "DISTRIBUTION_CHANNEL") 
	private String distributionChannel ;
	@Column(name = "DIVISION_LINE") 
	private String divisionLine ;
	@Column(name = "CIVIL_MILITARY_INDICATOR") 
	private String civilMilitaryIndicator ;
	@Column(name = "CUSTOMER_CLASS") 
	private String customerClass ;
	@Column(name = "PRICE_LIST") 
	private String priceList ;
	@Column(name = "KEY_CUSTOMER") 
	private String keyCustomer ;
	@Column(name = "PAYMENT_TERMS") 
	private String paymentTerms ;
	@Column(name = "DEFAULT_PLANT") 
	private String defaultPlant ;
	@Column(name = "ACTIVE_FLAG") 
	private String activeFlag ;
	@Column(name = "SOURCE_SYSTEM") 
	private String sourceSystem ;

//	private String createdBy ;
//	private Timestamp creationDate ;
//	private Timestamp lastUpdateDate ;
	
	private String blockedStatus ;
	private String organization;
	
	public Long getCustSalesId() {
		return custSalesId;
	}
	public void setCustSalesId(Long custSalesId) {
		this.custSalesId = custSalesId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getOrganizationId() {
		return organizationId;
	}
	public void setOrganizationId(String organizationId) {
		this.organizationId = organizationId;
	}
	public String getDistributionChannel() {
		return distributionChannel;
	}
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	public String getDivisionLine() {
		return divisionLine;
	}
	public void setDivisionLine(String divisionLine) {
		this.divisionLine = divisionLine;
	}
	public String getCivilMilitaryIndicator() {
		return civilMilitaryIndicator;
	}
	public void setCivilMilitaryIndicator(String civilMilitaryIndicator) {
		this.civilMilitaryIndicator = civilMilitaryIndicator;
	}
	public String getCustomerClass() {
		return customerClass;
	}
	public void setCustomerClass(String customerClass) {
		this.customerClass = customerClass;
	}
	public String getPriceList() {
		return priceList;
	}
	public void setPriceList(String priceList) {
		this.priceList = priceList;
	}
	public String getKeyCustomer() {
		return keyCustomer;
	}
	public void setKeyCustomer(String keyCustomer) {
		this.keyCustomer = keyCustomer;
	}
	public String getPaymentTerms() {
		return paymentTerms;
	}
	public void setPaymentTerms(String paymentTerms) {
		this.paymentTerms = paymentTerms;
	}
	public String getDefaultPlant() {
		return defaultPlant;
	}
	public void setDefaultPlant(String defaultPlant) {
		this.defaultPlant = defaultPlant;
	}
	public String getActiveFlag() {
		return activeFlag;
	}
	public void setActiveFlag(String activeFlag) {
		this.activeFlag = activeFlag;
	}
	public String getSourceSystem() {
		return sourceSystem;
	}
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}
//	public String getCreatedBy() {
//		return createdBy;
//	}
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}
//	public Timestamp getCreationDate() {
//		return creationDate;
//	}
//	public void setCreationDate(Timestamp creationDate) {
//		this.creationDate = creationDate;
//	}
//	public Timestamp getLastUpdateDate() {
//		return lastUpdateDate;
//	}
//	public void setLastUpdateDate(Timestamp lastUpdateDate) {
//		this.lastUpdateDate = lastUpdateDate;
//	}
	public String getBlockedStatus() {
		return blockedStatus;
	}
	public void setBlockedStatus(String blockedStatus) {
		this.blockedStatus = blockedStatus;
	}
	public String getOrganization() {
		return organization;
	}
	public void setOrganization(String organization) {
		this.organization = organization;
	}
	
//	@Override
//	public String toString() {
//		return "EtaSapCSales [custSalesId=" + custSalesId + ", customerId="
//				+ customerId + ", organizationId=" + organizationId
//				+ ", distributionChannel=" + distributionChannel
//				+ ", divisionLine=" + divisionLine
//				+ ", civilMilitaryIndicator=" + civilMilitaryIndicator
//				+ ", customerClass=" + customerClass + ", priceList="
//				+ priceList + ", keyCustomer=" + keyCustomer
//				+ ", paymentTerms=" + paymentTerms + ", defaultPlant="
//				+ defaultPlant + ", activeFlag=" + activeFlag
//				+ ", sourceSystem=" + sourceSystem + ", createdBy=" + createdBy
//				+ ", creationDate=" + creationDate + ", lastUpdateDate="
//				+ lastUpdateDate + ", blockedStatus=" + blockedStatus
//				+ ", organization=" + organization + "]";
//	}	
	
}