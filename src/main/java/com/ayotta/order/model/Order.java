package com.ayotta.order.model;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MapKey;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.NumberFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Table(name="xmp_web_order_headers")
//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//@CustomOrderValidation
public class Order {

	@Column(name="ORDER_HEADER_ID")
	@Id
	@SequenceGenerator(name="order_header_gen", sequenceName="ORDER_HEADER_ID_S",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="order_header_gen")
	@ApiModelProperty(notes = "The database generated Order header ID",hidden = true)
	private Integer orderHeaderId ;
	
	@Column(name="CUSTOMERID_CUSTOMER_ID")
	@NotNull(message = "Customer id is mandatory")
	@ApiModelProperty(notes = "Customer Id cannot be empty",required = true)
	private Integer customerId ;
	
	@Column(name="CUSTOMERNAME_CUSTOMER_NAME")
	@ApiModelProperty(hidden = true)
	@Size(max = 100, message ="customerName length can't be greater than 100 characters")
	private String customerName ;
	
	@Column(name="CPO_CUSTOMER_PO_NUMBER")
	@NotBlank(message = "Customer purchase order is mandatory")
	@ApiModelProperty(notes = "Enter your unique Purchase Order Reference Number.\nThis number will be used to identify your order within our sales order system",required = true)
	@Size(max = 70, message ="customerPoNumber length can't be greater than 70 characters")
	private String customerPoNumber ;
	
//	@Transient
//	@Column(name="CUSTOMERREF_CUSTOMER_REF")
//	private String customerRef ;
	
	
	@Column(name="ICR_ORDER_CURRENCY")
	@NotBlank(message = "Currency is mandatory")
	@Size(max = 3, message ="orderCurrency length can't be greater than 3 characters")
	@ApiModelProperty(notes = "Enter one of the currencies (EUR-Euro,GBP-Pound Sterling,USD-United States Dollar).Ex:USD \nThis currency must match the currency in your Safran Landing Systems Price Catalogue for the part you wish to procure.",required = true)
	private String orderCurrency ;
	
	@Column(name="DSIND_DROPSHIP_INDICATOR")
	@ApiModelProperty(notes = "Enter 'Y' if drop ship indicator is applicable for the customer")
	@Size(max = 1, message ="dropshipIndicator length can't be greater than 1 characters")
	private String dropshipIndicator ;
	
	@Column(name="DSCUST_DROPSHIP_CUSTOMER")
	@Size(max = 240, message ="dropshipCustomer length can't be greater than 240 characters")
	private String dropshipCustomer ;
	
	@Size(max = 240, message ="dropshipAddress1 length can't be greater than 240 characters")
	@Column(name="DSADD1_DROPSHIP_ADDRESS_1")
	private String dropshipAddress1 ;
	
	@Column(name="DSADD2_DROPSHIP_ADDRESS_2")
	@Size(max = 240, message ="dropshipAddress2 length can't be greater than 240 characters")
	private String dropshipAddress2 ;
	
	@Column(name="DSADD3_DROPSHIP_ADDRESS_3")
	@Size(max = 240, message ="dropshipAddress3 length can't be greater than 240 characters")
	private String dropshipAddress3 ;
	
	@Column(name="DSADD4_DROPSHIP_ADDRESS_4")
	@Size(max = 240, message ="dropshipAddress4 length can't be greater than 240 characters")
	private String dropshipAddress4 ;
	
	@Column(name="DSCTRY_DROPSHIP_COUNTRY")
	@Size(max = 240, message ="dropshipCountry length can't be greater than 240 characters")
	private String dropshipCountry ;
	
	@Column(name="DSCTRYCODE_DROPSHIP_CTRY_CODE")
	@Size(max = 240, message ="dropshipCtryCode length can't be greater than 240 characters")
	private String dropshipCtryCode ;
	
	@Column(name="DSCPO_DROPSHIP_CPO")
	@Size(max = 240, message ="dropshipCpo length can't be greater than 240 characters")
	private String dropshipCpo ;
	
//	@Transient
//	@Column(name="DSDELID_DROPSHIP_DEL_VIA_ID")
//	private Integer dropshipDelViaId ;
	
//	@Transient
//	@Column(name="RETURNEMAIL_RETURN_EMAIL")
//	private String returnEmail ;
	
//	@Transient
//	@Column(name="STATUS")
//	private String status ;
	
//	@Transient
//	@Column(name="STATUS_MESSAGE")
//	private String statusMessage ;
	
	@Column(name="LAST_UPDATE_DATE")
	@ApiModelProperty(hidden = true)
	private Timestamp lastUpdateDate ;
	
//	@Transient
//	@Column(name="LAST_UPDATED_BY")
//	private String lastUpdatedBy ;
	
	@Column(name="SHT_SHIP_TO_SITE_ID")
//	@NotNull(message = "ShiptoSiteId is mandatory")
	private Integer shipToSiteId ;
	
//	@Transient
//	@Column(name="DELIVER_TO_SITE_ID")
//	private Integer deliverToSiteId ;
//	
//	@Transient
//	@Column(name="DROPSHIP_AUTHORISED")
//	private String dropShipAuthorised;
	
	@Column(name="SPL_SUPPLIER_CODE")
	@ApiModelProperty(hidden = true)
	private String supplierCode ;
	
	@Column(name="DS_ZIP")
	@Size(max = 10, message ="dsZip length can't be greater than 10 characters")
	private String dsZip;
	
	@Column(name="DS_REGN")
	@Size(max = 3, message ="dsZip length can't be greater than 3 characters")
	private String dsRegn;
	
//	@Column(name="")
//	private String regionDesc;
//	@Transient
//	@Column(name="TRANSPZONE")
//	private String transpzone;
	
//	@Transient
//	@Column(name="ACCEPT_DUPLICATE_PART")
//	private String acceptDuplicatePart;
	
	@Column(name="CREATED_DATE")
	@ApiModelProperty(hidden = true)
	private Timestamp createdDate ;
	
//	@Transient
//	@Column(name="CREATED_BY")
//	private String createdBy;
	
	@Column(name="SUBMITTED_DATE")
	@ApiModelProperty(hidden = true)
	private Timestamp submittedDate;
	
//	@Transient
//	@Column(name="SUBMITTED_BY")
//	private String submittedBy;
	
	@Transient
	@OneToMany(mappedBy = "order",targetEntity=OrderLine.class,cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@Fetch(FetchMode.JOIN)
	private Set<OrderLine> orderLine = new HashSet<OrderLine>();
	
	public Integer getOrderHeaderId() {
		return orderHeaderId;
	}

	public void setOrderHeaderId(Integer orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerPoNumber() {
		return customerPoNumber;
	}

	public void setCustomerPoNumber(String customerPoNumber) {
		this.customerPoNumber = customerPoNumber;
	}

//	public String getCustomerRef() {
//		return customerRef;
//	}
//
//	public void setCustomerRef(String customerRef) {
//		this.customerRef = customerRef;
//	}

	public String getOrderCurrency() {
		return orderCurrency;
	}

	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}

	public String getDropshipIndicator() {
		return dropshipIndicator;
	}

	public void setDropshipIndicator(String dropshipIndicator) {
		this.dropshipIndicator = dropshipIndicator;
	}

	public String getDropshipCustomer() {
		return dropshipCustomer;
	}

	public void setDropshipCustomer(String dropshipCustomer) {
		this.dropshipCustomer = dropshipCustomer;
	}

	public String getDropshipAddress1() {
		return dropshipAddress1;
	}

	public void setDropshipAddress1(String dropshipAddress1) {
		this.dropshipAddress1 = dropshipAddress1;
	}

	public String getDropshipAddress2() {
		return dropshipAddress2;
	}

	public void setDropshipAddress2(String dropshipAddress2) {
		this.dropshipAddress2 = dropshipAddress2;
	}

	public String getDropshipAddress3() {
		return dropshipAddress3;
	}

	public void setDropshipAddress3(String dropshipAddress3) {
		this.dropshipAddress3 = dropshipAddress3;
	}

	public String getDropshipAddress4() {
		return dropshipAddress4;
	}

	public void setDropshipAddress4(String dropshipAddress4) {
		this.dropshipAddress4 = dropshipAddress4;
	}

	public String getDropshipCountry() {
		return dropshipCountry;
	}

	public void setDropshipCountry(String dropshipCountry) {
		this.dropshipCountry = dropshipCountry;
	}

	public String getDropshipCtryCode() {
		return dropshipCtryCode;
	}

	public void setDropshipCtryCode(String dropshipCtryCode) {
		this.dropshipCtryCode = dropshipCtryCode;
	}

	public String getDropshipCpo() {
		return dropshipCpo;
	}

	public void setDropshipCpo(String dropshipCpo) {
		this.dropshipCpo = dropshipCpo;
	}

//	public Integer getDropshipDelViaId() {
//		return dropshipDelViaId;
//	}
//
//	public void setDropshipDelViaId(Integer dropshipDelViaId) {
//		this.dropshipDelViaId = dropshipDelViaId;
//	}
//
//	public String getReturnEmail() {
//		return returnEmail;
//	}
//
//	public void setReturnEmail(String returnEmail) {
//		this.returnEmail = returnEmail;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getStatusMessage() {
//		return statusMessage;
//	}
//
//	public void setStatusMessage(String statusMessage) {
//		this.statusMessage = statusMessage;
//	}

	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

//	public String getLastUpdatedBy() {
//		return lastUpdatedBy;
//	}
//
//	public void setLastUpdatedBy(String lastUpdatedBy) {
//		this.lastUpdatedBy = lastUpdatedBy;
//	}

	public Integer getShipToSiteId() {
		return shipToSiteId;
	}

	public void setShipToSiteId(Integer shipToSiteId) {
		this.shipToSiteId = shipToSiteId;
	}

//	public Integer getDeliverToSiteId() {
//		return deliverToSiteId;
//	}
//
//	public void setDeliverToSiteId(Integer deliverToSiteId) {
//		this.deliverToSiteId = deliverToSiteId;
//	}
//
//	public String getDropShipAuthorised() {
//		return dropShipAuthorised;
//	}
//
//	public void setDropShipAuthorised(String dropShipAuthorised) {
//		this.dropShipAuthorised = dropShipAuthorised;
//	}

	public String getDsZip() {
		return dsZip;
	}

	public void setDsZip(String dsZip) {
		this.dsZip = dsZip;
	}

	public String getDsRegn() {
		return dsRegn;
	}

	public void setDsRegn(String dsRegn) {
		this.dsRegn = dsRegn;
	}

//	public String getTranspzone() {
//		return transpzone;
//	}
//
//	public void setTranspzone(String transpzone) {
//		this.transpzone = transpzone;
//	}

//	public String getAcceptDuplicatePart() {
//		return acceptDuplicatePart;
//	}
//
//	public void setAcceptDuplicatePart(String acceptDuplicatePart) {
//		this.acceptDuplicatePart = acceptDuplicatePart;
//	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

//	public String getCreatedBy() {
//		return createdBy;
//	}
//
//	public void setCreatedBy(String createdBy) {
//		this.createdBy = createdBy;
//	}

	public Timestamp getSubmittedDate() {
		return submittedDate;
	}

	public void setSubmittedDate(Timestamp submittedDate) {
		this.submittedDate = submittedDate;
	}

//	public String getSubmittedBy() {
//		return submittedBy;
//	}
//
//	public void setSubmittedBy(String submittedBy) {
//		this.submittedBy = submittedBy;
//	}

	public Set<OrderLine> getOrderLine() {
		return orderLine;
	}

	public void setOrderLine(Set<OrderLine> orderLine) {
		this.orderLine = orderLine;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	@Override
	public String toString() {
		return "Order [orderHeaderId=" + orderHeaderId + ", customerId=" + customerId + ", customerName=" + customerName
				+ ", customerPoNumber=" + customerPoNumber + ", orderCurrency=" + orderCurrency + ", dropshipIndicator="
				+ dropshipIndicator + ", dropshipCustomer=" + dropshipCustomer + ", dropshipAddress1="
				+ dropshipAddress1 + ", dropshipAddress2=" + dropshipAddress2 + ", dropshipAddress3=" + dropshipAddress3
				+ ", dropshipAddress4=" + dropshipAddress4 + ", dropshipCountry=" + dropshipCountry
				+ ", dropshipCtryCode=" + dropshipCtryCode + ", dropshipCpo=" + dropshipCpo + ", lastUpdateDate="
				+ lastUpdateDate + ", shipToSiteId=" + shipToSiteId + ", supplierCode=" + supplierCode + ", dsZip="
				+ dsZip + ", dsRegn=" + dsRegn + ", createdDate=" + createdDate + ", submittedDate=" + submittedDate
				+ "]";
	}
}
