/**
File 	:	 OrderEntryBean.java
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:01 BST 2009
Description:	 
 */
package com.ayotta.order.model;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
public class OrderEntryBean {
	
	Locale locale;
	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	// editable fields
	private String cpo;
	private String pnr;
	@Id
	@NotNull(message = "Part's id is mandatory")
	private Integer itemId;

	private String qty;
	private Timestamp ssd;
	
	private String ssdStr;
	private String spr;
	private String ipf;
	private String ipc;
	private String ipa;
	private String ipn;
	private String dsp;
	private String dsa;
	private String sht; // ship to address

	private String qtn;
	private double qtp;

	private String acn;
	private String rem;
	// default value fields
	private String cic;
	private String uom;
	private String unitPrice;
	private String sdc;
	private String pri; // values AOG,WSP,USR.
	private String foc;
	private String dsh;
	private String dsa1;
	private String dsa2;
	private String dsa3;
	private String dsa4;
	private String dsc;
	private String dscCode;

	private String dscpo;
	private String dsv;
	
	@NotBlank(message = "Currency is mandatory")
	private String icr;
	@NotNull(message = "Customer id is mandatory")
	private Integer custId;
	@NotBlank(message = "Customer name is mandatory")
	private String customerName;
	
	private String distChannel;
	private String salesOrg;
	private String vendorCode;
	
	private String delgRef;
	private Boolean isDelgFlag;
	private Boolean isAogOrderFlag = false;
	@ElementCollection
	List<String> selectedDivisionLine = new ArrayList<String>();
	
	public String getIcr() {
		return icr;
	}

	public void setIcr(String icr) {
		this.icr = icr;
	}

	public String getCpo() {
		return cpo;
	}

	public void setCpo(String cpo) {
		this.cpo = cpo;
	}

	public String getPnr() {
		return pnr;
	}

	public void setPnr(String pnr) {
		this.pnr = pnr;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getQty() {
		return qty;
	}

	public void setQty(String qty) {
		this.qty = qty;
	}

	public Timestamp getSsd() {
		return ssd;
	}

	public void setSsd(Timestamp ssd) {
		this.ssd = ssd;
	}

	public String getSsdStr() {
		return ssdStr;
	}

	public void setSsdStr(String ssdStr) {
		this.ssdStr = ssdStr;
	}

	public String getSpr() {
		return spr;
	}

	public void setSpr(String spr) {
		this.spr = spr;
	}

	public String getIpf() {
		return ipf;
	}

	public void setIpf(String ipf) {
		this.ipf = ipf;
	}

	public String getIpc() {
		return ipc;
	}

	public void setIpc(String ipc) {
		this.ipc = ipc;
	}

	public String getIpa() {
		return ipa;
	}

	public void setIpa(String ipa) {
		this.ipa = ipa;
	}

	public String getIpn() {
		return ipn;
	}

	public void setIpn(String ipn) {
		this.ipn = ipn;
	}

	public String getDsp() {
		return dsp;
	}

	public void setDsp(String dsp) {
		this.dsp = dsp;
	}

	public String getDsa() {
		return dsa;
	}

	public void setDsa(String dsa) {
		this.dsa = dsa;
	}

	public String getSht() {
		return sht;
	}

	public void setSht(String sht) {
		this.sht = sht;
	}

	public String getQtn() {
		return qtn;
	}

	public void setQtn(String qtn) {
		this.qtn = qtn;
	}

	public double getQtp() {
		return qtp;
	}

	public void setQtp(double qtp) {
		this.qtp = qtp;
	}

	public String getAcn() {
		return acn;
	}

	public void setAcn(String acn) {
		this.acn = acn;
	}

	public String getRem() {
		return rem;
	}

	public void setRem(String rem) {
		this.rem = rem;
	}

	public String getCic() {
		return cic;
	}

	public void setCic(String cic) {
		this.cic = cic;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(String unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getSdc() {
		return sdc;
	}

	public void setSdc(String sdc) {
		this.sdc = sdc;
	}

	public String getPri() {
		return pri;
	}

	public void setPri(String pri) {
		this.pri = pri;
	}

	public String getFoc() {
		return foc;
	}

	public void setFoc(String foc) {
		this.foc = foc;
	}

	public String getDsh() {
		return dsh;
	}

	public void setDsh(String dsh) {
		this.dsh = dsh;
	}

	public String getDsa1() {
		return dsa1;
	}

	public void setDsa1(String dsa1) {
		this.dsa1 = dsa1;
	}

	public String getDsa2() {
		return dsa2;
	}

	public void setDsa2(String dsa2) {
		this.dsa2 = dsa2;
	}

	public String getDsa3() {
		return dsa3;
	}

	public void setDsa3(String dsa3) {
		this.dsa3 = dsa3;
	}

	public String getDsa4() {
		return dsa4;
	}

	public void setDsa4(String dsa4) {
		this.dsa4 = dsa4;
	}

	public String getDsc() {
		return dsc;
	}

	public void setDsc(String dsc) {
		this.dsc = dsc;
	}

	public String getDscCode() {
		return dscCode;
	}

	public void setDscCode(String dscCode) {
		this.dscCode = dscCode;
	}

	public String getDscpo() {
		return dscpo;
	}

	public void setDscpo(String dscpo) {
		this.dscpo = dscpo;
	}

	public String getDsv() {
		return dsv;
	}

	public void setDsv(String dsv) {
		this.dsv = dsv;
	}

	public int getCustId() {
		return custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	public String getDelgRef() {
		return delgRef;
	}

	public void setDelgRef(String delgRef) {
		this.delgRef = delgRef;
	}

	public Boolean getIsDelgFlag() {
		return isDelgFlag;
	}

	public void setIsDelgFlag(Boolean isDelgFlag) {
		this.isDelgFlag = isDelgFlag;
	}

	public Boolean getIsAogOrderFlag() {
		return isAogOrderFlag;
	}

	public void setIsAogOrderFlag(Boolean isAogOrderFlag) {
		this.isAogOrderFlag = isAogOrderFlag;
	}

	public List<String> getSelectedDivisionLine() {
		return selectedDivisionLine;
	}

	public void setSelectedDivisionLine(List<String> selectedDivisionLine) {
		this.selectedDivisionLine = selectedDivisionLine;
	}
	
	
}