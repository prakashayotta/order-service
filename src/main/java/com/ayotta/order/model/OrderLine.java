package com.ayotta.order.model;

import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name="xmp_web_order_lines")
public class OrderLine {
	
	@Column(name="ORDER_LINE_ID")
	@Id
	@SequenceGenerator(name="order_line_gen", sequenceName="ORDER_LINE_ID_S",allocationSize=1)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="order_line_gen")
	@ApiModelProperty(notes = "The database generated Order line ID",hidden = true)
	private Integer orderLineId ;
	
//	@Column(name="ORDER_HEADER_ID")
//	private Integer orderHeaderId ;
	
	@Column(name="LIN_CUSTOMER_LINE_NUMBER")
	@ApiModelProperty(notes = "Customer Purchase Order Line Number cannot be empty",required = true)
	@NotBlank(message = "Customer line number is mandatory")
	@Size(max = 10, message ="customerLineNumber length can't be greater than 10 characters")
	private String customerLineNumber ;
	
	@Column(name="PNR_PART_NUMBER")
	@ApiModelProperty(notes = "Part number cannot be empty",required = true)
	@Size(max = 40, message ="partNumber length can't be greater than 40 characters")
	private String partNumber ;
	
	@Column(name="PART_ID")
	@ApiModelProperty(hidden = true)
	private Integer partId ;
	
	@Column(name="UNP_UNIT_PRICE")
	@ApiModelProperty(notes = "Unit price of the part", hidden = true)
	private Float unitPrice ;

	@Column(name="ICR_ORDER_CURRENCY")
	@ApiModelProperty(hidden = true)
	@Size(max = 100, message ="orderCurrency length can't be greater than 100 characters")
	private String orderCurrency ;
	
	@Column(name="QTO_ORDERED_QUANTITY")
	@ApiModelProperty(notes = "Order quantity cannot be empty",required = true)
	@NotBlank(message = "Order Quantity is mandatory")
	private String orderedQuantity ;
	
	@Column(name="UNT_UNIT_OF_MEASURE")
	@ApiModelProperty(hidden = true)
	@Size(max = 20, message ="untUnitOfMeasure length can't be greater than 20 characters")
	private String untUnitOfMeasure ;
	
//	@Column(name="SSD_PROMISE_DATE")
//	private Timestamp ssdPromiseDate ;
	
	@Column(name="SHT_SHIP_TO_SITE_ID")
	private Integer shipToSiteId ;
	
//	@Transient
//	@Column(name="QTN_QUOTE_REFERENCE")
//	private String qtnQuoteReference ;
//	
//	@Transient
//	@Column(name="QTP_QUOTE_PRICE")
//	private Integer qtpQuotePrice ;
	
//	@Transient
//	@Column(name="SDC_SHIP_BEFORE_FLAG")
//	private String sdcShipBeforeFlag ;
	
	@Column(name="PRI_SHIPMENT_PRIORITY")
	@ApiModelProperty(hidden = true)
	private String priShipmentPriority ;
	
//	@Transient
//	@Column(name="ACN_AIRCRAFT_REGISTRATION_NUM")
//	private String acnAircraftRegistrationNum ;
	
	@Column(name="PDP_PRICE_DISCOUNT_PERCENTAGE")
	@ApiModelProperty(notes = "Enter 'Y' if free of charge is applicable for the part")
	private String freeOfCharge ;
	
//	@Transient
//	@Column(name="LSE_LEASE_INDICATOR")
//	private String lseLeaseIndicator ;
	
	@Column(name="INP_INITIAL_PROVISIONING_FLAG")
	@ApiModelProperty(notes = "Enter 'Y' if initial provisioning is applicable for the part")
	@Size(max = 1, message ="initialProvisioning length can't be greater than 1 characters")
	private String initialProvisioning;
	
//	@Transient
//	@Column(name="SPR_SPECIAL_RULE_INDICATOR")
//	private String sprSpecialRuleIndicator ;
	
	@Column(name="REM_CUSTOMER_REMARKS")
	@ApiModelProperty(notes = "Remarks contains more than 2000 characters")
	@Size(max = 2000, message ="customerRemarks length can't be greater than 2000 characters")
	private String customerRemarks ;
	
//	@Transient
//	@Column(name="STATUS")
//	private String status ;
//	
//	@Transient
//	@Column(name="STATUS_MESSAGE")
//	private String statusMessage ;
	
	@Column(name="LAST_UPDATE_DATE")
	@ApiModelProperty(hidden = true)
	private Timestamp lastUpdateDate ;
	
//	@Transient
//	@Column(name="LAST_UPDATED_BY")
//	private String lastUpdatedBy ;
	
	@Column(name="SSD_PROMISE_DATE")
	@ApiModelProperty(notes = "Enter the date (dd-Mon-yyyy) you require the part to be shipped by",required = true)
	private String promiseDateStr;
	
//	@Transient
//	@Column(name="RETURN_TO_SERVICE_DATE")
//	private Timestamp returnToServiceDate ;
	
//	@Transient
//	private String returnToServiceDateStr;
	
	@Column(name="DSCUST_DROPSHIP_CUSTOMER")
	@Size(max = 40, message ="dropshipCustomer length can't be greater than 40 characters")
	private String dropshipCustomer ;
	
	@Column(name="DSADD1_DROPSHIP_ADDRESS_1")
	@Size(max = 40, message ="dropshipAddress1 length can't be greater than 40 characters")
	private String dropshipAddress1 ;
	
	@Column(name="DSADD2_DROPSHIP_ADDRESS_2")
	@Size(max = 40, message ="dropshipAddress2 length can't be greater than 40 characters")
	private String dropshipAddress2 ;
	
	@Column(name="DSADD3_DROPSHIP_ADDRESS_3")
	@Size(max = 40, message ="dropshipAddress3 length can't be greater than 40 characters")
	private String dropshipAddress3 ;
	
	@Column(name="DSADD4_DROPSHIP_ADDRESS_4")
	@Size(max = 40, message ="dropshipAddress4 length can't be greater than 40 characters")
	private String dropshipAddress4 ;
	
	@Column(name="DSCTRY_DROPSHIP_COUNTRY")
	@Size(max = 100, message ="dropshipCountry length can't be greater than 100 characters")
	private String dropshipCountry ;
	
	@Column(name="DSCTRYCODE_DROPSHIP_CTRY_CODE")
	@Size(max = 3, message ="dropshipCtryCode length can't be greater than 3 characters")
	private String dropshipCtryCode ;
	
	@Column(name="DSCPO_DROPSHIP_CPO")
	@Size(max = 50, message ="dropshipCpo length can't be greater than 50 characters")
	private String dropshipCpo ;
	
	@Column(name="CTN_CONTRACT_NUMBER")
	@ApiModelProperty(notes = "Safran agreement (contract) reference")
	@Size(max = 100, message ="contractNumber length can't be greater than 100 characters")
	private String contractNumber;
	
//	@Transient
//	@Column(name="IP_AUTHORISED")
//	private String ipAuthorised ;
	
	@Column(name="DSIND_DROPSHIP_INDICATOR")
	@ApiModelProperty(notes = "Enter 'Y' if drop ship indicator is applicable for the customer")
	@Size(max = 1, message ="dropshipIndicator length can't be greater than 1 characters")
	private String dropshipIndicator ;
	
	@Column(name="DS_ZIP")
	@Size(max = 10, message ="dsZip length can't be greater than 10 characters")
	private String dsZip;
	
	@Column(name="DS_REGN")
	@Size(max = 3, message ="dsRegn length can't be greater than 3 characters")
	private String dsRegn;
	
//	@Column(name="")
//	private String regionDesc;
	
//	@Transient
//	@Column(name="TRANSPZONE")
//	private String transpzone;
	
	@Column(name="DELEGATED_ENDUSER_REF")
	@Size(max = 20, message ="delgRef length can't be greater than 20 characters")
	private String delgRef;
	
	@Transient
	@ApiModelProperty(hidden = true)
	private Boolean isDelgFlag;
	
	@Transient
	@ApiModelProperty(hidden = true)
	private Boolean isAogOrderFlag = false; 
	
	@ManyToOne(cascade=CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinColumn(name = "ORDER_HEADER_ID", referencedColumnName = "ORDER_HEADER_ID", nullable = false)
	private Order order = new Order();
	
	public Integer getOrderLineId() {
		return orderLineId;
	}

	public void setOrderLineId(Integer orderLineId) {
		this.orderLineId = orderLineId;
	}


	public Integer getPartId() {
		return partId;
	}

	public void setPartId(Integer partId) {
		this.partId = partId;
	}

	public String getUntUnitOfMeasure() {
		return untUnitOfMeasure;
	}

	public void setUntUnitOfMeasure(String untUnitOfMeasure) {
		this.untUnitOfMeasure = untUnitOfMeasure;
	}

//	public Timestamp getSsdPromiseDate() {
//		return ssdPromiseDate;
//	}
//
//	public void setSsdPromiseDate(Timestamp ssdPromiseDate) {
//		this.ssdPromiseDate = ssdPromiseDate;
//	}

//	public String getQtnQuoteReference() {
//		return qtnQuoteReference;
//	}
//
//	public void setQtnQuoteReference(String qtnQuoteReference) {
//		this.qtnQuoteReference = qtnQuoteReference;
//	}
//
//	public Integer getQtpQuotePrice() {
//		return qtpQuotePrice;
//	}
//
//	public void setQtpQuotePrice(Integer qtpQuotePrice) {
//		this.qtpQuotePrice = qtpQuotePrice;
//	}
//
//	public String getSdcShipBeforeFlag() {
//		return sdcShipBeforeFlag;
//	}
//
//	public void setSdcShipBeforeFlag(String sdcShipBeforeFlag) {
//		this.sdcShipBeforeFlag = sdcShipBeforeFlag;
//	}

	public String getPriShipmentPriority() {
		return priShipmentPriority;
	}

	public void setPriShipmentPriority(String priShipmentPriority) {
		this.priShipmentPriority = priShipmentPriority;
	}

//	public String getAcnAircraftRegistrationNum() {
//		return acnAircraftRegistrationNum;
//	}
//
//	public void setAcnAircraftRegistrationNum(String acnAircraftRegistrationNum) {
//		this.acnAircraftRegistrationNum = acnAircraftRegistrationNum;
//	}
//
//	public String getLseLeaseIndicator() {
//		return lseLeaseIndicator;
//	}
//
//	public void setLseLeaseIndicator(String lseLeaseIndicator) {
//		this.lseLeaseIndicator = lseLeaseIndicator;
//	}
//
//	public String getSprSpecialRuleIndicator() {
//		return sprSpecialRuleIndicator;
//	}
//
//	public void setSprSpecialRuleIndicator(String sprSpecialRuleIndicator) {
//		this.sprSpecialRuleIndicator = sprSpecialRuleIndicator;
//	}
//
//	public String getStatus() {
//		return status;
//	}
//
//	public void setStatus(String status) {
//		this.status = status;
//	}
//
//	public String getStatusMessage() {
//		return statusMessage;
//	}
//
//	public void setStatusMessage(String statusMessage) {
//		this.statusMessage = statusMessage;
//	}

	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}

	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

//	public String getLastUpdatedBy() {
//		return lastUpdatedBy;
//	}
//
//	public void setLastUpdatedBy(String lastUpdatedBy) {
//		this.lastUpdatedBy = lastUpdatedBy;
//	}
//
//	public Timestamp getReturnToServiceDate() {
//		return returnToServiceDate;
//	}
//
//	public void setReturnToServiceDate(Timestamp returnToServiceDate) {
//		this.returnToServiceDate = returnToServiceDate;
//	}
//
//	public String getReturnToServiceDateStr() {
//		return returnToServiceDateStr;
//	}
//
//	public void setReturnToServiceDateStr(String returnToServiceDateStr) {
//		this.returnToServiceDateStr = returnToServiceDateStr;
//	}
//
//	public String getIpAuthorised() {
//		return ipAuthorised;
//	}
//
//	public void setIpAuthorised(String ipAuthorised) {
//		this.ipAuthorised = ipAuthorised;
//	}

	public String getDsZip() {
		return dsZip;
	}

	public void setDsZip(String dsZip) {
		this.dsZip = dsZip;
	}

	public String getDsRegn() {
		return dsRegn;
	}

	public void setDsRegn(String dsRegn) {
		this.dsRegn = dsRegn;
	}

//	public String getTranspzone() {
//		return transpzone;
//	}
//
//	public void setTranspzone(String transpzone) {
//		this.transpzone = transpzone;
//	}

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public String getCustomerLineNumber() {
		return customerLineNumber;
	}

	public void setCustomerLineNumber(String customerLineNumber) {
		this.customerLineNumber = customerLineNumber;
	}

	public String getPartNumber() {
		return partNumber;
	}

	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}

	public String getOrderedQuantity() {
		return orderedQuantity;
	}

	public void setOrderedQuantity(String orderedQuantity) {
		this.orderedQuantity = orderedQuantity;
	}

	public Integer getShipToSiteId() {
		return shipToSiteId;
	}

	public void setShipToSiteId(Integer shipToSiteId) {
		this.shipToSiteId = shipToSiteId;
	}

	public String getCustomerRemarks() {
		return customerRemarks;
	}

	public void setCustomerRemarks(String customerRemarks) {
		this.customerRemarks = customerRemarks;
	}

	public String getPromiseDateStr() {
		return promiseDateStr;
	}

	public void setPromiseDateStr(String promiseDateStr) {
		this.promiseDateStr = promiseDateStr;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getDelgRef() {
		return delgRef;
	}

	public void setDelgRef(String delgRef) {
		this.delgRef = delgRef;
	}

	public Boolean getIsDelgFlag() {
		return isDelgFlag;
	}

	public void setIsDelgFlag(Boolean isDelgFlag) {
		this.isDelgFlag = isDelgFlag;
	}

	public Boolean getIsAogOrderFlag() {
		return isAogOrderFlag;
	}

	public void setIsAogOrderFlag(Boolean isAogOrderFlag) {
		this.isAogOrderFlag = isAogOrderFlag;
	}

	public String getDropshipIndicator() {
		return dropshipIndicator;
	}

	public void setDropshipIndicator(String dropshipIndicator) {
		this.dropshipIndicator = dropshipIndicator;
	}

	public String getDropshipCustomer() {
		return dropshipCustomer;
	}

	public void setDropshipCustomer(String dropshipCustomer) {
		this.dropshipCustomer = dropshipCustomer;
	}

	public String getDropshipAddress1() {
		return dropshipAddress1;
	}

	public void setDropshipAddress1(String dropshipAddress1) {
		this.dropshipAddress1 = dropshipAddress1;
	}

	public String getDropshipAddress2() {
		return dropshipAddress2;
	}

	public void setDropshipAddress2(String dropshipAddress2) {
		this.dropshipAddress2 = dropshipAddress2;
	}

	public String getDropshipAddress3() {
		return dropshipAddress3;
	}

	public void setDropshipAddress3(String dropshipAddress3) {
		this.dropshipAddress3 = dropshipAddress3;
	}

	public String getDropshipAddress4() {
		return dropshipAddress4;
	}

	public void setDropshipAddress4(String dropshipAddress4) {
		this.dropshipAddress4 = dropshipAddress4;
	}

	public String getDropshipCountry() {
		return dropshipCountry;
	}

	public void setDropshipCountry(String dropshipCountry) {
		this.dropshipCountry = dropshipCountry;
	}

	public String getDropshipCtryCode() {
		return dropshipCtryCode;
	}

	public void setDropshipCtryCode(String dropshipCtryCode) {
		this.dropshipCtryCode = dropshipCtryCode;
	}

	public String getDropshipCpo() {
		return dropshipCpo;
	}

	public void setDropshipCpo(String dropshipCpo) {
		this.dropshipCpo = dropshipCpo;
	}

	public Float getUnitPrice() {
		return unitPrice;
	}

	public void setUnitPrice(Float unitPrice) {
		this.unitPrice = unitPrice;
	}

	public String getOrderCurrency() {
		return orderCurrency;
	}

	public void setOrderCurrency(String orderCurrency) {
		this.orderCurrency = orderCurrency;
	}

	public String getFreeOfCharge() {
		return freeOfCharge;
	}

	public void setFreeOfCharge(String freeOfCharge) {
		this.freeOfCharge = freeOfCharge;
	}

	public String getInitialProvisioning() {
		return initialProvisioning;
	}

	public void setInitialProvisioning(String initialProvisioning) {
		this.initialProvisioning = initialProvisioning;
	}

//	public Integer getOrderHeaderId() {
//		return orderHeaderId;
//	}
//
//	public void setOrderHeaderId(Integer orderHeaderId) {
//		this.orderHeaderId = orderHeaderId;
//	}

	
}
