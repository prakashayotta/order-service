/**
File 	:	 Parameters.java
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:02 BST 2009
Description:	 
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.ayotta.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author Raj
 */
@Entity
@Table(name="XXM_SP_PARAMS")
public class Parameters {
	@Id
	@Column(name="PARAMETER_ID")
	private Integer parameterId;
	@Column(name="PARAMETER_NAME")
	private String parameterName;
	@Column(name="PARAMETER_VALUE")
	private String parameterValue;

	public Integer getParameterId() {
		return parameterId;
	}

	public void setParameterId(Integer parameterId) {
		this.parameterId = parameterId;
	}

	public String getParameterName() {
		return parameterName;
	}

	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}

	public String getParameterValue() {
		return parameterValue;
	}

	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
}