package com.ayotta.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "")
@NamedNativeQueries({
	@NamedNativeQuery(query = "SELECT DISTINCT P.PART_ID ID " + " ,PM.SAP_PART_UID ERP_ITEMUID "
			+ " ,NVL(P.CROSS_REF_ATA,P.PART_NUMBER) PNT " + " ,P.CROSS_REF_ATA AMENDMENT_PART_NO "
			+ " ,P.DIVISION_LINE " + " ,P.MATERIAL_GROUP " + " ,P.PART_DESCRIPTION DESCRIPTION " + " ,P.PART_OWNER "
			+ " ,P.PRIMARY_UOM_CODE " + " ,P.STANDARD_PART " + " ,DECODE(P.CAT, 'Z4',2, 1) CAT1_INDICATOR "
			+ " ,PS.MAIN_AC_CODE " + " ,PS.MAIN_AC_PROGRAM " + " ,PS.DEFAULT_SHIP_WAREHOUSE "
			+ " ,PS.CATALOGUE_LEAD_TIME " + " ,DECODE(NVL(MILITARY_USE,'N'),'Y','MILITARY','CIVIL') MILITARY_USE "
			+ " FROM   ETA_SAP_PARTS  P " + " ,ETA_PARTS_MAP  PM " + " ,ETA_PART_SALES PS " + " ,ETA_CAGE_CODES EC "
			+ " WHERE  P.PART_ID = PS.PART_ID " + " AND    P.PART_ID = PM.PART_ID "
			+ " AND    EC.CSCORGID = PS.ORGANIZATION_ID "
			+ " AND    PS.DISTRIBUTION_CHANNEL = :distChannel "
			+ " AND   ( P.PART_NUMBER  LIKE :partNumber OR P.CROSS_REF_ATA LIKE :partNumber)"
			+ " AND   P.DIVISION_LINE IN (:vendorCode) "
//			+ " AND    EC.VENDOR_CODE IN (:vendorCode) "
//			+ " AND P.DIVISION_LINE IN (SELECT DIVISION FROM ETA_CAGE_CODES"
//			+ " WHERE VENDOR_CODE IN (:vendorCode)) "
	, name = "findParts",resultSetMapping="partsMapping"),
	
	@NamedNativeQuery(query = " SELECT   DISTINCT " + " p1.related_part_id itemid, "
			+ " p1.reciprocal reciprocal_flag, " + " p2.part_number partno, " + " p3.part_id PARENT, "
			+ " p2.part_description description, "
			+ " DECODE (p1.interchangeability, 'EC01', '1', 'EC02', '2', '0') icc, "
			+ " DECODE (P2.CAT, 'Z4',  2, 1 ) AS un_number_id , p2.cross_ref_ata amended_part_no" +
			" FROM   eta_related_parts p1, " +
			" eta_sap_parts p2, " + " (    SELECT   mriv1.part_id, mriv1.related_part_id, ROWNUM ord "
			+ " FROM   eta_related_parts mriv1 " + " START WITH   mriv1.part_id IN (:itemIds) "
			+ " CONNECT BY   NOCYCLE PRIOR mriv1.related_part_id = mriv1.part_id) " + " p3 "
			+ " WHERE       p1.related_part_id = p2.part_id " + " AND p1.related_part_id = p3.related_part_id "
			+ " AND p1.part_id = p3.part_id "
			+ " AND NVL(P1.VALID_FROM, SYSDATE) <= SYSDATE "
			+ " AND NVL(P1.VALID_TO, SYSDATE) >= SYSDATE "
	, name = "findSuperSeededParts", resultSetMapping="superSeededPartsMapping"),

})
@SqlResultSetMapping(name = "partsMapping", entities = @EntityResult(
	entityClass = Part.class, 
	fields = {
			@FieldResult(name = "itemId", column = "ID"),
			@FieldResult(name = "partNumber", column = "PNT"),
			@FieldResult(name = "partDescription", column = "DESCRIPTION"),
//			@FieldResult(name = "inventoryCategory", column = "CAT1_INDICATOR"),
//			@FieldResult(name = "amendedPartNo", column = "AMENDMENT_PART_NO"),
			
	}
))
@SqlResultSetMapping(name = "superSeededPartsMapping", entities = @EntityResult(
		entityClass = Part.class, 
		fields = {
				@FieldResult(name = "itemId", column = "itemId"),
				@FieldResult(name = "partNumber", column = "partNo"),
				@FieldResult(name = "partDescription", column = "DESCRIPTION"),
//				@FieldResult(name = "inventoryCategory", column = "UN_NUMBER_ID"),
//				@FieldResult(name = "parentItemId", column = "PARENT"),
//				@FieldResult(name = "icc", column = "icc"),
				
		}
	))
public class Part {
	
	@Id
    Integer itemId;
	String partNumber ;
	String partDescription ;
//	Integer inventoryCategory ;
//	Long partId ;
//	String amendment;
//	boolean amended;
//	String amendedPartNo;
//	Integer parentItemId = 0;
//	Integer icc = 0;
	
//	public Long getPartId() {
//		return partId;
//	}
//	public void setPartId(Long partId) {
//		this.partId = partId;
//	}
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
//	public Integer getInventoryCategory() {
//		return inventoryCategory;
//	}
//	public void setInventoryCategory(Integer inventoryCategory) {
//		this.inventoryCategory = inventoryCategory;
//	}
//	public boolean isAmended() {
//		return amended;
//	}
//	public void setAmended(boolean amended) {
//		this.amended = amended;
//	}
//	public String getAmendedPartNo() {
//		return amendedPartNo;
//	}
//	public void setAmendedPartNo(String amendedPartNo) {
//		this.amendedPartNo = amendedPartNo;
//	}
//	public Integer getParentItemId() {
//		return parentItemId;
//	}
//	public void setParentItemId(Integer parentItemId) {
//		this.parentItemId = parentItemId;
//	}
//	public Integer getIcc() {
//		return icc;
//	}
//	public void setIcc(Integer icc) {
//		this.icc = icc;
//	}
//	public String getAmendment() {
//		return amendment;
//	}
//	public void setAmendment(String amendment) {
//		this.amendment = amendment;
//	}
}
