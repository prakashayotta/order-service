package com.ayotta.order.model;

import java.text.NumberFormat;
import java.util.ArrayList;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "")
@NamedNativeQueries({
	
	@NamedNativeQuery(query = "SELECT   P.PART_ID ID,P.PART_NUMBER PNT, P.PART_DESCRIPTION DESCRIPTION, P.INVENTORY_CATEGORY, "
			+ " DECODE (P.CAT, 'Z4',  2, 1 ) UN_NUMBER_ID, "
			+ " TO_NUMBER (NVL (PS.CATALOGUE_LEAD_TIME, 30)) LEAD_TIME, PS.MAIN_AC_PROGRAM AC_PROGRAM, "
			+ " 'GLO' WAREHOUSE,  'EA' UOM,  WS.WARRANTY_LEVEL,   S.SUB_INVENTORY,   S.LOCATOR,   S.ONHAND_QUANTITY,P.DIVISION_LINE, P.CROSS_REF_ATA AMENDMENT_PART_NO, PS.MINIMUM_ORDER_QUANTITY, PS.STANDARD_PACKAGE_QUANTITY" 
			+ " FROM   ETA_STOCK S, ETA_SAP_PARTS P, ETA_PART_SALES PS, ETA.XXM_F7X_WARRANTY_STOCK WS  "
			+ " WHERE NVL(PS.DISTRIBUTION_CHANNEL,:distChannel) = :distChannel AND NVL(PS.ORGANIZATION_ID, :orgId) = :orgId AND P.PART_ID = S.PART_ID(+) AND P.PART_ID = :itemId AND PS.PART_ID (+) = P.PART_ID and P.PART_NUMBER = WS.PART_NUMBER (+) AND rownum < 2"
	, name = "findPartDetails", resultSetMapping="partDetailsMapping"),
	
	@NamedNativeQuery(query = "select third_party_supplier from eta_sap_parts where part_id = :itemId and rownum < 2"
	, name = "findResellerInfo"),
	
	@NamedNativeQuery(query = "SELECT TPS_CUST_ID FROM ETA_SAP_PARTS WHERE PART_ID = :itemId AND ROWNUM < 2"
	, name = "findThirdPartyId"),
	
	@NamedNativeQuery(query = "SELECT SUM(ORDERED_QUANTITY)  QTY FROM ETA_ORDER_LINES L , ETA_PART_SALES S "
			+ " WHERE L. PART_ID = :itemId AND L.PART_ID = S.PART_ID AND  PROMISE_DATE > SYSDATE AND PROMISE_DATE <= SYSDATE+S.CATALOGUE_LEAD_TIME "
			+ " AND UPPER(ORDER_STATUS) NOT IN ('CLOSED', 'CANCELLED', 'SHIPPED') "
	, name = "findDuesOut"),
	
	@NamedNativeQuery(query = " SELECT SUM(shipped_quantity) total_sales  FROM   eta_deliveries d ,eta_delivery_lines l "
			+" WHERE  l.part_id  = :itemId AND  d.delivery_id  = l.delivery_id AND     d.shipping_date >= (SYSDATE - 730)"
	, name = "findTotalSales"),
	
	@NamedNativeQuery(query = " SELECT CUSTOMER_ID FROM ETA_SAP_CUSTOMERS WHERE DELEGATED_PARTNER_REF= :delgPartnerRef "
	, name = "findDelgPartnerCustId"),
	
	@NamedNativeQuery(query = " SELECT NVL(CUSTOMER_NUMBER,CUSTOMER_ACCOUNT_NUMBER) FROM ETA_SAP_CUSTOMERS WHERE CUSTOMER_ID = :custId "
	, name = "findCustNumber"),
	
	@NamedNativeQuery(query = " select eta.getorg(:vendorCode) from dual "
	, name = "findOrgId"),
	
	@NamedNativeQuery(query = " SELECT  STRATEGY_GROUP FROM ETA_PART_PLANT P, ETA_PART_SALES S, ETA_ORGANIZATIONS O"
			+ " WHERE S.PART_ID = :itemId AND S.PART_ID = P.PART_ID AND O.ORGANIZATION_ID = P.ORGANIZATION_ID"
			+ " AND S.DEFAULT_SHIP_WAREHOUSE= O.ORGANIZATION_NAME AND ORGANIZATION_TYPE='Plant' "
	, name = "findStrategyGroup"),
	
	@NamedNativeQuery(query = " SELECT COUNT(*) FROM   ETA_SAP_CUST_ADDR_SITE_INFO WHERE  CUSTOMER_ID = (SELECT SAP_CUSTOMER_UID FROM ETA_CUSTOMER_MAP WHERE CUSTOMER_ID = :custId) "
			+ "AND    SITE_USAGE LIKE :divLine||'%' AND   (ATTRIBUTE1  = :airProg "
			+ "OR ATTRIBUTE2  = :airProg OR ATTRIBUTE3  = :airProg OR ATTRIBUTE4  = :airProg "
			+ "OR ATTRIBUTE5  = :airProg OR ATTRIBUTE6  = :airProg OR ATTRIBUTE7  = :airProg "
			+ "OR ATTRIBUTE9  = :airProg OR ATTRIBUTE10 = :airProg OR ATTRIBUTE11 = :airProg "
			+ "OR ATTRIBUTE12 = :airProg OR ATTRIBUTE13 = :airProg OR ATTRIBUTE14 = :airProg "
			+ "OR ATTRIBUTE15 = :airProg OR ATTRIBUTE16 = :airProg OR ATTRIBUTE17 = :airProg "
			+ "OR ATTRIBUTE18 = :airProg OR ATTRIBUTE19 = :airProg OR ATTRIBUTE20 = :airProg) "
	, name = "findFleet"),
	
})

@SqlResultSetMapping(name = "partDetailsMapping", entities = @EntityResult(
		entityClass = PartDetails.class, 
		fields = {
				@FieldResult(name = "itemId", column = "ID"),
				@FieldResult(name = "partNumber", column = "PNT"),
				@FieldResult(name = "partDescription", column = "DESCRIPTION"),
				@FieldResult(name = "warehouse", column = "WAREHOUSE"),
				@FieldResult(name = "leadTime", column = "LEAD_TIME"),
				@FieldResult(name = "uom", column = "UOM"),
				@FieldResult(name = "warrantyStock", column = "WARRANTY_LEVEL"),
				@FieldResult(name = "unitType", column = "INVENTORY_CATEGORY"),
				@FieldResult(name = "airProg", column = "AC_PROGRAM"),
				@FieldResult(name = "divisionLine", column = "DIVISION_LINE"),
				@FieldResult(name = "amendedPartNo", column = "AMENDMENT_PART_NO"),
				@FieldResult(name = "minimumOrderQuantity", column = "MINIMUM_ORDER_QUANTITY"),
				@FieldResult(name = "standardPackageQuantity", column = "STANDARD_PACKAGE_QUANTITY"),

				
		}
	))
public class PartDetails {

	@Id
    Integer itemId;
	String partNumber ;
	String partDescription ;
	String warehouse;
	Integer leadTime;
	String uom;
	Integer warrantyStock = 0;
	String unitType;
	String airProg;
	String divisionLine;
	String amendedPartNo;
	Integer minimumOrderQuantity;
	Integer standardPackageQuantity;

	@Transient
	String reseller;
	@Transient
	Long tpsCustId;
	@Transient
	Integer totalStock;
	@Transient
	double stockRatio;
	@Transient
	boolean isTps;
	@Transient
	String partStrategyGrp;
	@Transient
	Double price;
	@Transient
	String formattedPrice;
	@Transient
	String icr;
	@Transient
	boolean publish = true;
	@Transient
	Boolean isAogOrderFlag;
	@Transient
	Boolean isDelgFlag;
	@Transient
	String priceMsg;
	@Transient
	@ElementCollection
	private ArrayList<Stock> stockList;
	@Transient
	String availability;
	public Integer getItemId() {
		return itemId;
	}
	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartDescription() {
		return partDescription;
	}
	public void setPartDescription(String partDescription) {
		this.partDescription = partDescription;
	}
	public String getWarehouse() {
		return warehouse;
	}
	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}
	public Integer getLeadTime() {
		return leadTime;
	}
	public void setLeadTime(Integer leadTime) {
		this.leadTime = leadTime;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Integer getWarrantyStock() {
		return warrantyStock;
	}
	public void setWarrantyStock(Integer warrantyStock) {
		this.warrantyStock = warrantyStock;
	}
	public String getUnitType() {
		return unitType;
	}
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}
	public String getAirProg() {
		return airProg;
	}
	public void setAirProg(String airProg) {
		this.airProg = airProg;
	}
	public String getDivisionLine() {
		return divisionLine;
	}
	public void setDivisionLine(String divisionLine) {
		this.divisionLine = divisionLine;
	}
	public String getAmendedPartNo() {
		return amendedPartNo;
	}
	public void setAmendedPartNo(String amendedPartNo) {
		this.amendedPartNo = amendedPartNo;
	}
	public Integer getMinimumOrderQuantity() {
		return minimumOrderQuantity;
	}
	public void setMinimumOrderQuantity(Integer minimumOrderQuantity) {
		this.minimumOrderQuantity = minimumOrderQuantity;
	}
	public Integer getStandardPackageQuantity() {
		return standardPackageQuantity;
	}
	public void setStandardPackageQuantity(Integer standardPackageQuantity) {
		this.standardPackageQuantity = standardPackageQuantity;
	}
	public String getReseller() {
		return reseller;
	}
	public void setReseller(String reseller) {
		this.reseller = reseller;
	}
	public Long getTpsCustId() {
		return tpsCustId;
	}
	public void setTpsCustId(Long tpsCustId) {
		this.tpsCustId = tpsCustId;
	}
	public ArrayList<Stock> getStockList() {
		return stockList;
	}
	public void setStockList(ArrayList<Stock> stockList) {
		this.stockList = stockList;
	}
	public Integer getTotalStock() {
		return totalStock;
	}
	public void setTotalStock(Integer totalStock) {
		this.totalStock = totalStock;
	}
	public double getStockRatio() {
		return stockRatio;
	}
	public void setStockRatio(double stockRatio) {
		this.stockRatio = stockRatio;
	}
	public boolean isTps() {
		return isTps;
	}
	public void setTps(boolean isTps) {
		this.isTps = isTps;
	}
	public String getPartStrategyGrp() {
		return partStrategyGrp;
	}
	public void setPartStrategyGrp(String partStrategyGrp) {
		this.partStrategyGrp = partStrategyGrp;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMinimumFractionDigits(2);
		nf.setMaximumFractionDigits(2);
		setFormattedPrice(nf.format(price));
	}
	public String getFormattedPrice() {
		return formattedPrice;
	}
	public void setFormattedPrice(String formattedPrice) {
		this.formattedPrice = formattedPrice;
	}
	public String getIcr() {
		return icr;
	}
	public void setIcr(String icr) {
		this.icr = icr;
	}
	public boolean isPublish() {
		return publish;
	}
	public void setPublish(boolean publish) {
		this.publish = publish;
	}
	public Boolean getIsAogOrderFlag() {
		return isAogOrderFlag;
	}
	public void setIsAogOrderFlag(Boolean isAogOrderFlag) {
		this.isAogOrderFlag = isAogOrderFlag;
	}
	public Boolean getIsDelgFlag() {
		return isDelgFlag;
	}
	public void setIsDelgFlag(Boolean isDelgFlag) {
		this.isDelgFlag = isDelgFlag;
	}
	public String getPriceMsg() {
		return priceMsg;
	}
	public void setPriceMsg(String priceMsg) {
		this.priceMsg = priceMsg;
	}
	public String getAvailability() {
		return availability;
	}
	public void setAvailability(String availability) {
		this.availability = availability;
	}
	@Override
	public String toString() {
		return "PartDetails [itemId=" + itemId + ", partNumber=" + partNumber + ", partDescription=" + partDescription
				+ ", warehouse=" + warehouse + ", leadTime=" + leadTime + ", uom=" + uom + ", warrantyStock="
				+ warrantyStock + ", unitType=" + unitType + ", airProg=" + airProg + ", divisionLine=" + divisionLine
				+ ", amendedPartNo=" + amendedPartNo + ", minimumOrderQuantity=" + minimumOrderQuantity
				+ ", standardPackageQuantity=" + standardPackageQuantity + ", reseller=" + reseller + ", tpsCustId="
				+ tpsCustId + ", totalStock=" + totalStock + ", stockRatio=" + stockRatio + ", isTps=" + isTps
				+ ", partStrategyGrp=" + partStrategyGrp + ", price=" + price + ", formattedPrice=" + formattedPrice
				+ ", icr=" + icr + ", publish=" + publish + ", isAogOrderFlag=" + isAogOrderFlag + ", isDelgFlag="
				+ isDelgFlag + ", stockList=" + stockList + "]";
	}

}
