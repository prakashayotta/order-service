/**
File 	:	 PartPriceBean.java
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:01 BST 2009
Description:	 
 */
package com.ayotta.order.model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PartPriceBean {
	@Id
	private Integer itemId;
	private Integer customerId;
	private String currency;
	private Integer priceListId;
	private Double price;
	private String priceStr;
	private Integer availableStock;
	private Double unitPartPriceLimit;
	private Double exchangeRate;
	private String unitPartPriceCurr;
	private boolean exceedsLimit;
	private Integer leadTime;
	private String reseller;
	private String uom;
	private double stockRatio;
	private String divisionLine;
	private Integer moq;
	private Integer spq;
	private String description;
	private boolean isTps;
	private Boolean isDelgFlag;
	private Boolean isAogOrderFlag;
	
	

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public void setExceedsLimit(boolean exceedsLimit) {
		this.exceedsLimit = exceedsLimit;
	}

	public boolean isExceedsLimit() {
		return exceedsLimit;
	}

	public Double getUnitPartPriceLimit() {
		return unitPartPriceLimit;
	}

	public void setUnitPartPriceLimit(Double unitPartPriceLimit) {
		this.unitPartPriceLimit = unitPartPriceLimit;
	}

	public String getUnitPartPriceCurr() {
		return unitPartPriceCurr;
	}

	public void setUnitPartPriceCurr(String unitPartPriceCurr) {
		this.unitPartPriceCurr = unitPartPriceCurr;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public Integer getPriceListId() {
		return priceListId;
	}

	public void setPriceListId(Integer priceListId) {
		this.priceListId = priceListId;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getPrice() {
		return price;
	}

	public void setPriceStr(String priceStr) {
		this.priceStr = priceStr;
	}

	public String getPriceStr() {
		return priceStr;
	}

	public void setAvailableStock(Integer availableStock) {
		this.availableStock = availableStock;
	}

	public Integer getAvailableStock() {
		return availableStock;
	}

	public void setExchangeRate(Double exchangeRate) {
		this.exchangeRate = exchangeRate;
	}

	public Double getExchangeRate() {
		return exchangeRate;
	}

	public void setLeadTime(Integer leadTime) {
		this.leadTime = leadTime;
	}

	public Integer getLeadTime() {
		return leadTime;
	}

	public void setReseller(String reseller) {
		this.reseller = reseller;
	}

	public String getReseller() {
		return reseller;
	}

	public double getStockRatio() {
		return stockRatio;
	}

	public void setStockRatio(double stockRatio) {
		this.stockRatio = stockRatio;
	}

	public String getDivisionLine() {
		return divisionLine;
	}

	public void setDivisionLine(String divisionLine) {
		this.divisionLine = divisionLine;
	}

	public Integer getMoq() {
		return moq;
	}

	public void setMoq(Integer moq) {
		this.moq = moq;
	}

	public Integer getSpq() {
		return spq;
	}

	public void setSpq(Integer spq) {
		this.spq = spq;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isTps() {
		return isTps;
	}

	public void setTps(boolean isTps) {
		this.isTps = isTps;
	}
	
	public Boolean getIsDelgFlag() {
		return isDelgFlag;
	}

	public void setIsDelgFlag(Boolean isDelgFlag) {
		this.isDelgFlag = isDelgFlag;
	}

	public Boolean getIsAogOrderFlag() {
		return isAogOrderFlag;
	}

	public void setIsAogOrderFlag(Boolean isAogOrderFlag) {
		this.isAogOrderFlag = isAogOrderFlag;
	}

}