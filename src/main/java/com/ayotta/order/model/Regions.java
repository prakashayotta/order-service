package com.ayotta.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ETA_REGIONS")
public class Regions {
	
	@Column(name="REGION_CODE")
	private String regionCode;
	@Column(name="COUNTRY_CODE")
	private String countryCode;
	@Column(name="DESCRIPTION")
	@Id
	private String name;
	
	public String getRegionCode() {
		return regionCode;
	}
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	

}
