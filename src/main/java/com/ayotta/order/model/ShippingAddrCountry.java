package com.ayotta.order.model;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "")
@NamedNativeQueries({
	@NamedNativeQuery(query = "SELECT   DISTINCT EC.TERRITORY_SHORT_NAME NAME, EC.TERRITORY_CODE CODE, " +
		   " ET.ZIP_LENGTH LENGTH, ET.ZIP_RULE RULE, ET.RULE_EXP EXP, ET.TRANSPORTATION_ZONE TRANSPZONE "+
		   "FROM   ETA_COUNTRIES EC, ETA_TERRITORIES ET "+
		   "WHERE   EC.TERRITORY_CODE IS NOT NULL "+
		   "AND EC.TERRITORY_SHORT_NAME IS NOT NULL "+
		   "AND ET.TERRITORY_CODE IS NOT NULL "+ 
		   "AND EC.TERRITORY_CODE =ET.TERRITORY_CODE "+
		   "AND UPPER(EC.TERRITORY_SHORT_NAME) = :name "
	, name = "findShipAddrCountry", resultSetMapping="shippingAddrCountryMapping"),
})
@SqlResultSetMapping(name = "shippingAddrCountryMapping", entities = @EntityResult(
		entityClass = ShippingAddrCountry.class, 
		fields = {
				@FieldResult(name = "name", column = "NAME"),
				@FieldResult(name = "code", column = "CODE"),
				@FieldResult(name = "length", column = "LENGTH"),
				@FieldResult(name = "rule", column = "RULE"),
				@FieldResult(name = "rule_exp", column = "EXP"),
				@FieldResult(name = "transpzone", column = "TRANSPZONE"),
				
		}
	))
public class ShippingAddrCountry {
	
	@Id
	public String name;
	public String code;
	public String length;
	public String rule;
	public String rule_exp;
	public String transpzone;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getLength() {
		return length;
	}
	public void setLength(String length) {
		this.length = length;
	}
	public String getRule() {
		return rule;
	}
	public void setRule(String rule) {
		this.rule = rule;
	}
	public String getRule_exp() {
		return rule_exp;
	}
	public void setRule_exp(String rule_exp) {
		this.rule_exp = rule_exp;
	}
	public String getTranspzone() {
		return transpzone;
	}
	public void setTranspzone(String transpzone) {
		this.transpzone = transpzone;
	}
	@Override
	public String toString() {
		return "ShippingAddrCountry [name=" + name + ", code=" + code + ", length=" + length + ", rule=" + rule
				+ ", rule_exp=" + rule_exp + ", transpzone=" + transpzone + "]";
	}

}
