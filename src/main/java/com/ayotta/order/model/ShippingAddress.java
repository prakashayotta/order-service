package com.ayotta.order.model;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;

@Entity
@Table(name = "")
@NamedNativeQueries({
	@NamedNativeQuery(query = "select rownum, x.* from (SELECT distinct \r\n" + 
			"                  CA.ADDRESS_LINE1 SHT1,\r\n" + 
			"                  CA.ADDRESS_LINE2 SHT2,\r\n" + 
			"                  CA.ADDRESS_LINE3 SHT3,\r\n" + 
			"                  CA.ADDRESS_LINE4 SHT4,\r\n" + 
			"                  CA.ADDRESS_LINE5 SHT5,\r\n" + 
			"                  CA.POSTAL_CODE POSTAL_CODE,\r\n" + 
			"                  CA.STATE STATE,\r\n" + 
			"                  CA.COUNTRY COUNTRY,\r\n" + 
			"                  CA.SITE_ID SITE_ID,\r\n" + 
			"                  CA.ADDRESS_ID SITE,\r\n" + 
			"                  '' PARTY,\r\n" + 
			"                  NVL (CA.SITE_ID, 0) EXPORT,\r\n" + 
			"                  REL.DIVISION_LINE\r\n" + 
			"         FROM   ETA_SAP_C_RELATION REL, ETA_SAP_CUSTOMER_ADDRESSES CA\r\n" + 
			"         WHERE       REL.P_CUSTOMER_ID = CA.SITE_ID\r\n" + 
			"         AND REL.SITE_TYPE IN ('SHIP TO', 'SHIP_TO')\r\n" + 
			"         AND CA.ACTIVE_FLAG = 'Y'\r\n" + 
			"         AND REL.CUSTOMER_ID = :customerId\r\n" + 
			"         AND REL.DIVISION_LINE IN (:supplierCode)) x "
	, name = "findShipToAddress", resultSetMapping="shippingAddrMapping"),
	@NamedNativeQuery(query = "select rownum, x.* from (SELECT distinct \r\n" + 
			"                  CA.ADDRESS_LINE1 SHT1,\r\n" + 
			"                  CA.ADDRESS_LINE2 SHT2,\r\n" + 
			"                  CA.ADDRESS_LINE3 SHT3,\r\n" + 
			"                  CA.ADDRESS_LINE4 SHT4,\r\n" + 
			"                  CA.ADDRESS_LINE5 SHT5,\r\n" + 
			"                  CA.POSTAL_CODE POSTAL_CODE,\r\n" + 
			"                  CA.STATE STATE,\r\n" + 
			"                  CA.COUNTRY COUNTRY,\r\n" + 
			"                  CA.SITE_ID SITE_ID,\r\n" + 
			"                  CA.ADDRESS_ID SITE,\r\n" + 
			"                  '' PARTY,\r\n" + 
			"                  NVL (CA.SITE_ID, 0) EXPORT,\r\n" + 
			"                  REL.DIVISION_LINE\r\n" + 
			"         FROM   ETA_SAP_C_RELATION REL, ETA_SAP_CUSTOMER_ADDRESSES CA\r\n" + 
			"         WHERE       REL.P_CUSTOMER_ID = CA.SITE_ID\r\n" + 
			"         AND REL.SITE_TYPE IN ('SHIP TO', 'SHIP_TO')\r\n" + 
			"         AND CA.ACTIVE_FLAG = 'Y'\r\n" + 
			"         AND REL.CUSTOMER_ID = ?1 \r\n" + 
			"         AND REL.DIVISION_LINE IN (?2)\r\n" +
			"         AND CA.ADDRESS_ID = ?3) x "
	, name = "fetchShipToAddress", resultSetMapping="shippingAddrMapping"),
	@NamedNativeQuery(query = "select rownum, x.* from (SELECT distinct \r\n" + 
			"                  CA.ADDRESS_LINE1 SHT1,\r\n" + 
			"                  CA.ADDRESS_LINE2 SHT2,\r\n" + 
			"                  CA.ADDRESS_LINE3 SHT3,\r\n" + 
			"                  CA.ADDRESS_LINE4 SHT4,\r\n" + 
			"                  CA.ADDRESS_LINE5 SHT5,\r\n" + 
			"                  CA.POSTAL_CODE POSTAL_CODE,\r\n" + 
			"                  CA.STATE STATE,\r\n" + 
			"                  CA.COUNTRY COUNTRY,\r\n" + 
			"                  CA.SITE_ID SITE_ID,\r\n" + 
			"                  CA.ADDRESS_ID SITE,\r\n" + 
			"                  '' PARTY,\r\n" + 
			"                  NVL (CA.SITE_ID, 0) EXPORT,\r\n" + 
			"                  REL.DIVISION_LINE\r\n" + 
			"         FROM   ETA_SAP_C_RELATION REL, ETA_SAP_CUSTOMER_ADDRESSES CA\r\n" + 
			"         WHERE       REL.P_CUSTOMER_ID = CA.SITE_ID\r\n" + 
			"         AND REL.SITE_TYPE IN ('SHIP TO', 'SHIP_TO')\r\n" + 
			"         AND CA.ACTIVE_FLAG = 'Y'\r\n" + 
			"         AND REL.CUSTOMER_ID = :customerId\r\n" + 
			"         AND REL.DIVISION_LINE = :division) x "
	, name = "findShipToAddressByDivision", resultSetMapping="shippingAddrMapping"),
})
@SqlResultSetMapping(name = "shippingAddrMapping", entities = @EntityResult(
	entityClass = ShippingAddress.class, 
	fields = {
			@FieldResult(name = "addressLine1", column = "SHT1"),
			@FieldResult(name = "addressLine2", column = "SHT2"),
			@FieldResult(name = "addressLine3", column = "SHT3"),
			@FieldResult(name = "addressLine4", column = "SHT4"),
			@FieldResult(name = "addressLine5", column = "SHT5"),
			@FieldResult(name = "postalCode", column = "POSTAL_CODE"),
			@FieldResult(name = "state", column = "STATE"),
			@FieldResult(name = "country", column = "COUNTRY"),
			@FieldResult(name = "siteId", column = "SITE_ID"),
			@FieldResult(name = "site", column = "SITE"),
			@FieldResult(name = "divisionLine", column = "DIVISION_LINE"),
			@FieldResult(name = "rowNum", column = "ROWNUM"),
			
	}
))
public class ShippingAddress {
    
	public String addressLine1;  
	public String addressLine2;
	public String addressLine3;
	public String addressLine4;
	public String addressLine5;
	public String postalCode;
    public String state;
    public String country;
    public String siteId;
    public Integer site;
   
    @Id
    private Integer rowNum;
    public String divisionLine;
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getAddressLine3() {
		return addressLine3;
	}
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}
	public String getAddressLine4() {
		return addressLine4;
	}
	public void setAddressLine4(String addressLine4) {
		this.addressLine4 = addressLine4;
	}
	public String getAddressLine5() {
		return addressLine5;
	}
	public void setAddressLine5(String addressLine5) {
		this.addressLine5 = addressLine5;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public Integer getSite() {
		return site;
	}
	public void setSite(Integer site) {
		this.site = site;
	}
	public Integer getRowNum() {
		return rowNum;
	}
	public void setRowNum(Integer rowNum) {
		this.rowNum = rowNum;
	}
	public String getDivisionLine() {
		return divisionLine;
	}
	public void setDivisionLine(String divisionLine) {
		this.divisionLine = divisionLine;
	}
}
