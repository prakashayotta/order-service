/**
File 	:	 Stock.java
Author	:	 Raj Varadarajan @ Sopra Group Limited.
Date	:	 Thu Jul 02 08:22:02 BST 2009
Description:	 
 */
package com.ayotta.order.model;

import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "")
@NamedNativeQueries({
	
	@NamedNativeQuery(query = "SELECT   O.WAREHOUSE_CODE ORGANIZATION_CODE, S.SUB_INVENTORY SUBINVENTORY_NAME,"
			+ " S.LOCATOR, NVL (S.ONHAND_QUANTITY, 0) STOCK_QTY, NVL (PP.AOG_STOCK, 0) AOG_STOCK, SALEABLE_INDICATOR,"
			+ " SALEABLE_DESCRIPTION, NVL(DISPLAY_IN_PORTAL, 'N') DISPLAY_IN_PORTAL, PP.PART_ID FROM   ETA_STOCK S, ETA_PART_PLANT PP,"
			+ " ETA_ORGANIZATIONS O, ETA_SECONDARY_INVENTORIES I WHERE       S.ORGANIZATION_ID = PP.ORGANIZATION_ID"
			+ " AND S.PART_ID = :itemId AND S.ORGANIZATION_ID = O.ORGANIZATION_ID AND NVL (S.ONHAND_QUANTITY, -1) > 0"
			+ " AND S.ORGANIZATION_ID IN  (SELECT   ORGANIZATION_ID FROM   ETA_ORGANIZATIONS O WHERE  "
			+ " SUBSTR (ORGANIZATION_NAME, LENGTH (ORGANIZATION_NAME), 1) = PP.OWNING_OEM AND UPPER (O.ORGANIZATION_TYPE) = 'PLANT')"
			+ " AND S.SUB_INVENTORY = I.SECONDARY_INVENTORY AND I.ORGANIZATION_ID = S.ORGANIZATION_ID  AND S.PART_ID = PP.PART_ID"
	, name = "findStockPart", resultSetMapping="stockPartMapping"),
	
})

@SqlResultSetMapping(name = "stockPartMapping", entities = @EntityResult(
		entityClass = Stock.class, 
		fields = {
				@FieldResult(name = "organizationCode", column = "ORGANIZATION_CODE"),
				@FieldResult(name = "subinventoryName", column = "SUBINVENTORY_NAME"),
//				@FieldResult(name = "bin", column = ""),
//				@FieldResult(name = "saleableIndicator", column = ""),
//				@FieldResult(name = "saleableDescription", column = ""),
				@FieldResult(name = "stockQty", column = "STOCK_QTY"),
				@FieldResult(name = "aogStock", column = "AOG_STOCK"),
				@FieldResult(name = "subinventory", column = "SUBINVENTORY_NAME"),
				@FieldResult(name = "displayInPortal", column = "DISPLAY_IN_PORTAL"),
				@FieldResult(name = "salableIndicator", column = "SALEABLE_INDICATOR"),
				@FieldResult(name = "itemId", column = "PART_ID"),
		}
	))
public class Stock {

	@Id
	private Integer itemId;
	private String organizationCode;
	private String subinventoryName;
//	private String bin;
//	private String saleableIndicator;
//	private String saleableDescription;
	private Integer stockQty;
	private Integer aogStock;
	@Transient
	private boolean displayForExternal = true;
	
	private String subinventory;
	private String displayInPortal;
	private String salableIndicator;


	public void setOrganizationCode(String organizationCode) {
		this.organizationCode = organizationCode;
	}

	public String getOrganizationCode() {
		return organizationCode;
	}

	public void setSubinventoryName(String subinventoryName) {
		this.subinventoryName = subinventoryName;
	}

	public String getSubinventoryName() {
		return subinventoryName;
	}

//	public void setBin(String bin) {
//		this.bin = bin;
//	}
//
//	public String getBin() {
//		return bin;
//	}
//
//	public void setSaleableIndicator(String saleableIndicator) {
//		this.saleableIndicator = saleableIndicator;
//	}
//
//	public String getSaleableIndicator() {
//		return saleableIndicator;
//	}
//
//	public void setSaleableDescription(String saleableDescription) {
//		this.saleableDescription = saleableDescription;
//	}
//
//	public String getSaleableDescription() {
//		return saleableDescription;
//	}

	public void setStockQty(Integer stockQty) {
		this.stockQty = stockQty;
	}

	public Integer getStockQty() {
		return stockQty;
	}

	public boolean subEquals(Stock newStock) {
		
		if (newStock == null  || this.organizationCode == null ||this.subinventoryName == null ){
			return false;
		}
		if (this.organizationCode.equals(newStock.getOrganizationCode())
				&& this.subinventoryName.equals(newStock.getSubinventoryName()))
			return true;
		return false;
	}


	public void setDisplayForExternal(boolean displayForExternal) {
		this.displayForExternal = displayForExternal;
	}

	public boolean isDisplayForExternal() {
		return displayForExternal;
	}

	public Integer getAogStock() {
		return aogStock;
	}

	public void setAogStock(Integer aogStock) {
		this.aogStock = aogStock;
	}

	public String getSubinventory() {
		return subinventory;
	}

	public void setSubinventory(String subinventory) {
		this.subinventory = subinventory;
	}

	public String getDisplayInPortal() {
		return displayInPortal;
	}

	public void setDisplayInPortal(String displayInPortal) {
		this.displayInPortal = displayInPortal;
	}

	public String getSalableIndicator() {
		return salableIndicator;
	}

	public void setSalableIndicator(String salableIndicator) {
		this.salableIndicator = salableIndicator;
	}

}