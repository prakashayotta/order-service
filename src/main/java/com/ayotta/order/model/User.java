package com.ayotta.order.model;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.Table;

@Entity
@Table(name="XXM_SP_USERS")
public class User {
	
	@Id
	@Column (name="USR_ID")
	private Integer usrId ;
	@Column (name="USR_FIRST_NAME")
	private String usrFirstName ;
	@Column (name="USR_LAST_NAME")
	private String usrLastName ;
	@Column (name="USR_NAME")
	private String usrName ;
	@Column (name="USR_PWD")
	private String usrPwd ;
	@Column (name="USR_EMAIL")
	private String usrEmail ;
	@Column (name="USR_PHONE")
	private String usrPhone ;
	@Column (name="USR_CUSTOMER_ID")
	private Integer usrCustomerId ;
	@Column (name="USR_START_DATE")
	private Timestamp usrStartDate ;
	@Column (name="USR_END_DATE")
	private Timestamp usrEndDate ;
	@Column (name="USR_FAX")
	private String usrFax ;
	@Column (name="APPROVER")
	private String approver ;
	@Column (name="APPROVAL_DATE")
	private Timestamp approvalDate ;
	@Column (name="USR_TYPE")
	private String usrType ;
	@Column (name="SPEC_USER")
	private String specUser ;
	@Column (name="CIC_CODE")
	private String cicCode ;
	@Column (name="COMMENTS")
	private String comments ;
	@Column (name="USR_LAST_LOGIN")
	private Timestamp usrLastLogin ;
	@Column (name="PASSWD_RESET_DATE")
	private Timestamp passwdResetDate ;
	@Column (name="USR_PURCHASING_LIMIT")
	private Integer usrPurchasingLimit ;
	@Column (name="USR_PURCHASING_CURR")
	private String usrPurchasingCurr ;
	@Column (name="USR_INT_DIAL_CODE")
	private String usrIntDialCode ;
	@Column (name="USR_PHONE_EXT")
	private String usrPhoneExt ;
	@Column (name="USR_MOBILE_PHONE")
	private String usrMobilePhone ;
	@Column (name="USR_MOBILE_INT_DIAL_CODE")
	private String usrMobileIntDialCode ;
	@Column (name="JOBTITLE")
	private String jobtitle ;
	@Column (name="LEGACY_CUSTOMER_ID")
	private Integer legacyCustomerId ;
	
	public Integer getUsrId() {
		return usrId;
	}
	public void setUsrId(Integer usrId) {
		this.usrId = usrId;
	}
	public String getUsrFirstName() {
		return usrFirstName;
	}
	public void setUsrFirstName(String usrFirstName) {
		this.usrFirstName = usrFirstName;
	}
	public String getUsrLastName() {
		return usrLastName;
	}
	public void setUsrLastName(String usrLastName) {
		this.usrLastName = usrLastName;
	}
	public String getUsrName() {
		return usrName;
	}
	public void setUsrName(String usrName) {
		this.usrName = usrName;
	}
	public String getUsrPwd() {
		return usrPwd;
	}
	public void setUsrPwd(String usrPwd) {
		this.usrPwd = usrPwd;
	}
	public String getUsrEmail() {
		return usrEmail;
	}
	public void setUsrEmail(String usrEmail) {
		this.usrEmail = usrEmail;
	}
	public String getUsrPhone() {
		return usrPhone;
	}
	public void setUsrPhone(String usrPhone) {
		this.usrPhone = usrPhone;
	}
	public Integer getUsrCustomerId() {
		return usrCustomerId;
	}
	public void setUsrCustomerId(Integer usrCustomerId) {
		this.usrCustomerId = usrCustomerId;
	}
	public Timestamp getUsrStartDate() {
		return usrStartDate;
	}
	public void setUsrStartDate(Timestamp usrStartDate) {
		this.usrStartDate = usrStartDate;
	}
	public Timestamp getUsrEndDate() {
		return usrEndDate;
	}
	public void setUsrEndDate(Timestamp usrEndDate) {
		this.usrEndDate = usrEndDate;
	}
	public String getUsrFax() {
		return usrFax;
	}
	public void setUsrFax(String usrFax) {
		this.usrFax = usrFax;
	}
	public String getApprover() {
		return approver;
	}
	public void setApprover(String approver) {
		this.approver = approver;
	}
	public Timestamp getApprovalDate() {
		return approvalDate;
	}
	public void setApprovalDate(Timestamp approvalDate) {
		this.approvalDate = approvalDate;
	}
	public String getUsrType() {
		return usrType;
	}
	public void setUsrType(String usrType) {
		this.usrType = usrType;
	}
	public String getSpecUser() {
		return specUser;
	}
	public void setSpecUser(String specUser) {
		this.specUser = specUser;
	}
	public String getCicCode() {
		return cicCode;
	}
	public void setCicCode(String cicCode) {
		this.cicCode = cicCode;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public Timestamp getUsrLastLogin() {
		return usrLastLogin;
	}
	public void setUsrLastLogin(Timestamp usrLastLogin) {
		this.usrLastLogin = usrLastLogin;
	}
	public Timestamp getPasswdResetDate() {
		return passwdResetDate;
	}
	public void setPasswdResetDate(Timestamp passwdResetDate) {
		this.passwdResetDate = passwdResetDate;
	}
	public Integer getUsrPurchasingLimit() {
		return usrPurchasingLimit;
	}
	public void setUsrPurchasingLimit(Integer usrPurchasingLimit) {
		this.usrPurchasingLimit = usrPurchasingLimit;
	}
	public String getUsrPurchasingCurr() {
		return usrPurchasingCurr;
	}
	public void setUsrPurchasingCurr(String usrPurchasingCurr) {
		this.usrPurchasingCurr = usrPurchasingCurr;
	}
	public String getUsrIntDialCode() {
		return usrIntDialCode;
	}
	public void setUsrIntDialCode(String usrIntDialCode) {
		this.usrIntDialCode = usrIntDialCode;
	}
	public String getUsrPhoneExt() {
		return usrPhoneExt;
	}
	public void setUsrPhoneExt(String usrPhoneExt) {
		this.usrPhoneExt = usrPhoneExt;
	}
	public String getUsrMobilePhone() {
		return usrMobilePhone;
	}
	public void setUsrMobilePhone(String usrMobilePhone) {
		this.usrMobilePhone = usrMobilePhone;
	}
	public String getUsrMobileIntDialCode() {
		return usrMobileIntDialCode;
	}
	public void setUsrMobileIntDialCode(String usrMobileIntDialCode) {
		this.usrMobileIntDialCode = usrMobileIntDialCode;
	}
	public String getJobtitle() {
		return jobtitle;
	}
	public void setJobtitle(String jobtitle) {
		this.jobtitle = jobtitle;
	}
	public Integer getLegacyCustomerId() {
		return legacyCustomerId;
	}
	public void setLegacyCustomerId(Integer legacyCustomerId) {
		this.legacyCustomerId = legacyCustomerId;
	}
	@Override
	public String toString() {
		return "User [usrId=" + usrId + ", usrFirstName=" + usrFirstName + ", usrLastName=" + usrLastName + ", usrName="
				+ usrName + ", usrPwd=" + usrPwd + ", usrEmail=" + usrEmail + ", usrPhone=" + usrPhone
				+ ", usrCustomerId=" + usrCustomerId + ", usrStartDate=" + usrStartDate + ", usrEndDate=" + usrEndDate
				+ ", usrFax=" + usrFax + ", approver=" + approver + ", approvalDate=" + approvalDate + ", usrType="
				+ usrType + ", specUser=" + specUser + ", cicCode=" + cicCode + ", comments=" + comments
				+ ", usrLastLogin=" + usrLastLogin + ", passwdResetDate=" + passwdResetDate + ", usrPurchasingLimit="
				+ usrPurchasingLimit + ", usrPurchasingCurr=" + usrPurchasingCurr + ", usrIntDialCode=" + usrIntDialCode
				+ ", usrPhoneExt=" + usrPhoneExt + ", usrMobilePhone=" + usrMobilePhone + ", usrMobileIntDialCode="
				+ usrMobileIntDialCode + ", jobtitle=" + jobtitle + ", legacyCustomerId=" + legacyCustomerId + "]";
	}
	
	
}
