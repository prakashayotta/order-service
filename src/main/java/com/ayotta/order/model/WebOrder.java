package com.ayotta.order.model;

import java.util.List;

public class WebOrder {

	private XmpWebOrderHeaders orderHdr;

	private List<XmpWebOrderLines>  orderLines;

	public XmpWebOrderHeaders getOrderHdr() {
		return orderHdr;
	}

	public void setOrderHdr(XmpWebOrderHeaders orderHdr) {
		this.orderHdr = orderHdr;
	}

	public List<XmpWebOrderLines> getOrderLines() {
		return orderLines;
	}

	public void setOrderLines(List<XmpWebOrderLines> orderLines) {
		this.orderLines = orderLines;
	}

	@Override
	public String toString() {
		return "WebOrder [orderHdr=" + orderHdr + ", orderLines=" + orderLines
				+ "]";
	}
}