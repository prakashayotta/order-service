package com.ayotta.order.model;

import java.io.Serializable;
import java.sql.Timestamp;

public class XmpWebOrderHeaders implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2704915718350072474L;
	
	private Integer orderHeaderId ;
	private Integer customeridCustomerId ;
	private String customernameCustomerName ;
	private String cpoCustomerPoNumber ;
	private String customerrefCustomerRef ;
	private String icrOrderCurrency ;
	private String dsindDropshipIndicator ;
	private String dscustDropshipCustomer ;
	private String dsadd1DropshipAddress1 ;
	private String dsadd2DropshipAddress2 ;
	private String dsadd3DropshipAddress3 ;
	private String dsadd4DropshipAddress4 ;
	private String dsctryDropshipCountry ;
	private String dsctrycodeDropshipCtryCode ;
	private String dscpoDropshipCpo ;
	private Integer dsdelidDropshipDelViaId ;
	private String returnemailReturnEmail ;
	private String status ;
	private String statusMessage ;
	private Timestamp lastUpdateDate ;
	private String lastUpdatedBy ;
	private Integer shtShipToSiteId ;
	private Integer deliverToSiteId ;
	private String dropShipAuthorised;
	private String orderLimitAuthorised;
	
	public Integer getOrderHeaderId() {
		return orderHeaderId;
	}
	public void setOrderHeaderId(Integer orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}
	public Integer getCustomeridCustomerId() {
		return customeridCustomerId;
	}
	public void setCustomeridCustomerId(Integer customeridCustomerId) {
		this.customeridCustomerId = customeridCustomerId;
	}
	public String getCustomernameCustomerName() {
		return customernameCustomerName;
	}
	public void setCustomernameCustomerName(String customernameCustomerName) {
		this.customernameCustomerName = customernameCustomerName;
	}
	public String getCpoCustomerPoNumber() {
		return cpoCustomerPoNumber;
	}
	public void setCpoCustomerPoNumber(String cpoCustomerPoNumber) {
		this.cpoCustomerPoNumber = cpoCustomerPoNumber;
	}
	public String getCustomerrefCustomerRef() {
		return customerrefCustomerRef;
	}
	public void setCustomerrefCustomerRef(String customerrefCustomerRef) {
		this.customerrefCustomerRef = customerrefCustomerRef;
	}
	public String getIcrOrderCurrency() {
		return icrOrderCurrency;
	}
	public void setIcrOrderCurrency(String icrOrderCurrency) {
		this.icrOrderCurrency = icrOrderCurrency;
	}
	public String getDsindDropshipIndicator() {
		return dsindDropshipIndicator;
	}
	public void setDsindDropshipIndicator(String dsindDropshipIndicator) {
		this.dsindDropshipIndicator = dsindDropshipIndicator;
	}
	public String getDscustDropshipCustomer() {
		return dscustDropshipCustomer;
	}
	public void setDscustDropshipCustomer(String dscustDropshipCustomer) {
		this.dscustDropshipCustomer = dscustDropshipCustomer;
	}
	public String getDsadd1DropshipAddress1() {
		return dsadd1DropshipAddress1;
	}
	public void setDsadd1DropshipAddress1(String dsadd1DropshipAddress1) {
		this.dsadd1DropshipAddress1 = dsadd1DropshipAddress1;
	}
	public String getDsadd2DropshipAddress2() {
		return dsadd2DropshipAddress2;
	}
	public void setDsadd2DropshipAddress2(String dsadd2DropshipAddress2) {
		this.dsadd2DropshipAddress2 = dsadd2DropshipAddress2;
	}
	public String getDsadd3DropshipAddress3() {
		return dsadd3DropshipAddress3;
	}
	public void setDsadd3DropshipAddress3(String dsadd3DropshipAddress3) {
		this.dsadd3DropshipAddress3 = dsadd3DropshipAddress3;
	}
	public String getDsadd4DropshipAddress4() {
		return dsadd4DropshipAddress4;
	}
	public void setDsadd4DropshipAddress4(String dsadd4DropshipAddress4) {
		this.dsadd4DropshipAddress4 = dsadd4DropshipAddress4;
	}
	public String getDsctryDropshipCountry() {
		return dsctryDropshipCountry;
	}
	public void setDsctryDropshipCountry(String dsctryDropshipCountry) {
		this.dsctryDropshipCountry = dsctryDropshipCountry;
	}
	public String getDsctrycodeDropshipCtryCode() {
		return dsctrycodeDropshipCtryCode;
	}
	public void setDsctrycodeDropshipCtryCode(String dsctrycodeDropshipCtryCode) {
		this.dsctrycodeDropshipCtryCode = dsctrycodeDropshipCtryCode;
	}
	public String getDscpoDropshipCpo() {
		return dscpoDropshipCpo;
	}
	public void setDscpoDropshipCpo(String dscpoDropshipCpo) {
		this.dscpoDropshipCpo = dscpoDropshipCpo;
	}
	public Integer getDsdelidDropshipDelViaId() {
		return dsdelidDropshipDelViaId;
	}
	public void setDsdelidDropshipDelViaId(Integer dsdelidDropshipDelViaId) {
		this.dsdelidDropshipDelViaId = dsdelidDropshipDelViaId;
	}
	public String getReturnemailReturnEmail() {
		return returnemailReturnEmail;
	}
	public void setReturnemailReturnEmail(String returnemailReturnEmail) {
		this.returnemailReturnEmail = returnemailReturnEmail;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public Integer getShtShipToSiteId() {
		return shtShipToSiteId;
	}
	public void setShtShipToSiteId(Integer shtShipToSiteId) {
		this.shtShipToSiteId = shtShipToSiteId;
	}
	public Integer getDeliverToSiteId() {
		return deliverToSiteId;
	}
	public void setDeliverToSiteId(Integer deliverToSiteId) {
		this.deliverToSiteId = deliverToSiteId;
	}
	public String getDropShipAuthorised() {
		return dropShipAuthorised;
	}
	public void setDropShipAuthorised(String dropShipAuthorised) {
		this.dropShipAuthorised = dropShipAuthorised;
	}
	public String getOrderLimitAuthorised() {
		return orderLimitAuthorised;
	}
	public void setOrderLimitAuthorised(String orderLimitAuthorised) {
		this.orderLimitAuthorised = orderLimitAuthorised;
	}
	
	@Override
	public String toString() {
		return "XmpWebOrderHeaders [orderHeaderId=" + orderHeaderId
				+ ", customeridCustomerId=" + customeridCustomerId
				+ ", customernameCustomerName=" + customernameCustomerName
				+ ", cpoCustomerPoNumber=" + cpoCustomerPoNumber
				+ ", customerrefCustomerRef=" + customerrefCustomerRef
				+ ", icrOrderCurrency=" + icrOrderCurrency
				+ ", dsindDropshipIndicator=" + dsindDropshipIndicator
				+ ", dscustDropshipCustomer=" + dscustDropshipCustomer
				+ ", dsadd1DropshipAddress1=" + dsadd1DropshipAddress1
				+ ", dsadd2DropshipAddress2=" + dsadd2DropshipAddress2
				+ ", dsadd3DropshipAddress3=" + dsadd3DropshipAddress3
				+ ", dsadd4DropshipAddress4=" + dsadd4DropshipAddress4
				+ ", dsctryDropshipCountry=" + dsctryDropshipCountry
				+ ", dsctrycodeDropshipCtryCode=" + dsctrycodeDropshipCtryCode
				+ ", dscpoDropshipCpo=" + dscpoDropshipCpo
				+ ", dsdelidDropshipDelViaId=" + dsdelidDropshipDelViaId
				+ ", returnemailReturnEmail=" + returnemailReturnEmail
				+ ", status=" + status + ", statusMessage=" + statusMessage
				+ ", lastUpdateDate=" + lastUpdateDate + ", lastUpdatedBy="
				+ lastUpdatedBy + ", shtShipToSiteId=" + shtShipToSiteId
				+ ", deliverToSiteId=" + deliverToSiteId
				+ ", dropShipAuthorised=" + dropShipAuthorised
				+ ", orderLimitAuthorised=" + orderLimitAuthorised + "]";
	}
	
}