package com.ayotta.order.model;

import java.sql.Timestamp;
import java.util.Locale;

public class XmpWebOrderLines {

	private Integer orderLineId ;
	private Integer orderHeaderId ;
	private String linCustomerLineNumber ;
	private String pnrPartNumber ;
	private Integer partId ;
	private Double unpUnitPrice ;
	private String icrOrderCurrency ;
	private Integer qtoOrderedQuantity ;
	private String untUnitOfMeasure ;
	private Timestamp ssdPromiseDate ;
	private Timestamp returnToServiceDate ;
	private Integer shtShipToSiteId ;
	private String qtnQuoteReference ;
	private Double qtpQuotePrice ;
	private String sdcShipBeforeFlag ;
	private String priShipmentPriority ;
	private String acnAircraftRegistrationNum ;
	private String pdpPriceDiscountPercentage ;
	private String lseLeaseIndicator ;
	private String inpInitialProvisioningFlag ;
	private String sprSpecialRuleIndicator ;
	private String remCustomerRemarks ;
	private String status ;
	private String statusMessage ;
	private Timestamp lastUpdateDate ;
	private String lastUpdatedBy ;
	private String lineLimitAuthorised ;
	private String qtnAuthorised ;
	private String ipAuthorised ;
	private String sprAuthorised ;
	private String focAuthorised ;
	private String aogAuthorised ;
	private Integer alternativePartId ;
	private String cat7Authorised ;
	private String leaseAuthorised ;
	private Integer shipFromOrgId ;
	private String ssdPromiseDateStr;
	private String returnToServiceDateStr;
	
	private Locale locale;
	
	public Integer getOrderLineId() {
		return orderLineId;
	}
	public void setOrderLineId(Integer orderLineId) {
		this.orderLineId = orderLineId;
	}
	public Integer getOrderHeaderId() {
		return orderHeaderId;
	}
	public void setOrderHeaderId(Integer orderHeaderId) {
		this.orderHeaderId = orderHeaderId;
	}
	public String getLinCustomerLineNumber() {
		return linCustomerLineNumber;
	}
	public void setLinCustomerLineNumber(String linCustomerLineNumber) {
		this.linCustomerLineNumber = linCustomerLineNumber;
	}
	public String getPnrPartNumber() {
		return pnrPartNumber;
	}
	public void setPnrPartNumber(String pnrPartNumber) {
		this.pnrPartNumber = pnrPartNumber;
	}
	public Integer getPartId() {
		return partId;
	}
	public void setPartId(Integer partId) {
		this.partId = partId;
	}	
	
	public Double getUnpUnitPrice() {
		return unpUnitPrice;
	}
	public void setUnpUnitPrice(Double unpUnitPrice) {
		this.unpUnitPrice = unpUnitPrice;
	}
	public String getIcrOrderCurrency() {
		return icrOrderCurrency;
	}
	public void setIcrOrderCurrency(String icrOrderCurrency) {
		this.icrOrderCurrency = icrOrderCurrency;
	}
	public Integer getQtoOrderedQuantity() {
		return qtoOrderedQuantity;
	}
	public void setQtoOrderedQuantity(Integer qtoOrderedQuantity) {
		this.qtoOrderedQuantity = qtoOrderedQuantity;
	}
	public String getUntUnitOfMeasure() {
		return untUnitOfMeasure;
	}
	public void setUntUnitOfMeasure(String untUnitOfMeasure) {
		this.untUnitOfMeasure = untUnitOfMeasure;
	}
	public Timestamp getSsdPromiseDate() {
		return ssdPromiseDate;
	}
	public void setSsdPromiseDate(Timestamp ssdPromiseDate) {
		this.ssdPromiseDate = ssdPromiseDate;
	}
	public Integer getShtShipToSiteId() {
		return shtShipToSiteId;
	}
	public void setShtShipToSiteId(Integer shtShipToSiteId) {
		this.shtShipToSiteId = shtShipToSiteId;
	}
	public String getQtnQuoteReference() {
		return qtnQuoteReference;
	}
	public void setQtnQuoteReference(String qtnQuoteReference) {
		this.qtnQuoteReference = qtnQuoteReference;
	}	
	public Double getQtpQuotePrice() {
		return qtpQuotePrice;
	}
	public void setQtpQuotePrice(Double qtpQuotePrice) {
		this.qtpQuotePrice = qtpQuotePrice;
	}
	public String getSdcShipBeforeFlag() {
		return sdcShipBeforeFlag;
	}
	public void setSdcShipBeforeFlag(String sdcShipBeforeFlag) {
		this.sdcShipBeforeFlag = sdcShipBeforeFlag;
	}
	public String getPriShipmentPriority() {
		return priShipmentPriority;
	}
	public void setPriShipmentPriority(String priShipmentPriority) {
		this.priShipmentPriority = priShipmentPriority;
	}
	public String getAcnAircraftRegistrationNum() {
		return acnAircraftRegistrationNum;
	}
	public void setAcnAircraftRegistrationNum(String acnAircraftRegistrationNum) {
		this.acnAircraftRegistrationNum = acnAircraftRegistrationNum;
	}
	public String getPdpPriceDiscountPercentage() {
		return pdpPriceDiscountPercentage;
	}
	public void setPdpPriceDiscountPercentage(String pdpPriceDiscountPercentage) {
		this.pdpPriceDiscountPercentage = pdpPriceDiscountPercentage;
	}
	public String getLseLeaseIndicator() {
		return lseLeaseIndicator;
	}
	public void setLseLeaseIndicator(String lseLeaseIndicator) {
		this.lseLeaseIndicator = lseLeaseIndicator;
	}
	public String getInpInitialProvisioningFlag() {
		return inpInitialProvisioningFlag;
	}
	public void setInpInitialProvisioningFlag(String inpInitialProvisioningFlag) {
		this.inpInitialProvisioningFlag = inpInitialProvisioningFlag;
	}
	public String getSprSpecialRuleIndicator() {
		return sprSpecialRuleIndicator;
	}
	public void setSprSpecialRuleIndicator(String sprSpecialRuleIndicator) {
		this.sprSpecialRuleIndicator = sprSpecialRuleIndicator;
	}
	public String getRemCustomerRemarks() {
		return remCustomerRemarks;
	}
	public void setRemCustomerRemarks(String remCustomerRemarks) {
		this.remCustomerRemarks = remCustomerRemarks;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getStatusMessage() {
		return statusMessage;
	}
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	public Timestamp getLastUpdateDate() {
		return lastUpdateDate;
	}
	public void setLastUpdateDate(Timestamp lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}
	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}
	public String getLineLimitAuthorised() {
		return lineLimitAuthorised;
	}
	public void setLineLimitAuthorised(String lineLimitAuthorised) {
		this.lineLimitAuthorised = lineLimitAuthorised;
	}
	public String getQtnAuthorised() {
		return qtnAuthorised;
	}
	public void setQtnAuthorised(String qtnAuthorised) {
		this.qtnAuthorised = qtnAuthorised;
	}
	public String getIpAuthorised() {
		return ipAuthorised;
	}
	public void setIpAuthorised(String ipAuthorised) {
		this.ipAuthorised = ipAuthorised;
	}
	public String getSprAuthorised() {
		return sprAuthorised;
	}
	public void setSprAuthorised(String sprAuthorised) {
		this.sprAuthorised = sprAuthorised;
	}
	public String getFocAuthorised() {
		return focAuthorised;
	}
	public void setFocAuthorised(String focAuthorised) {
		this.focAuthorised = focAuthorised;
	}
	public String getAogAuthorised() {
		return aogAuthorised;
	}
	public void setAogAuthorised(String aogAuthorised) {
		this.aogAuthorised = aogAuthorised;
	}
	public Integer getAlternativePartId() {
		return alternativePartId;
	}
	public void setAlternativePartId(Integer alternativePartId) {
		this.alternativePartId = alternativePartId;
	}
	public String getCat7Authorised() {
		return cat7Authorised;
	}
	public void setCat7Authorised(String cat7Authorised) {
		this.cat7Authorised = cat7Authorised;
	}
	public String getLeaseAuthorised() {
		return leaseAuthorised;
	}
	public void setLeaseAuthorised(String leaseAuthorised) {
		this.leaseAuthorised = leaseAuthorised;
	}
	public Integer getShipFromOrgId() {
		return shipFromOrgId;
	}
	public void setShipFromOrgId(Integer shipFromOrgId) {
		this.shipFromOrgId = shipFromOrgId;
	}
	public String getSsdPromiseDateStr() {
		return ssdPromiseDateStr;
	}
	public void setSsdPromiseDateStr(String ssdPromiseDateStr) {
		this.ssdPromiseDateStr = ssdPromiseDateStr;
	}
	public Locale getLocale() {
		return locale;
	}
	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	public String getReturnToServiceDateStr() {
		return returnToServiceDateStr;
	}
	public void setReturnToServiceDateStr(String returnToServiceDateStr) {
		this.returnToServiceDateStr = returnToServiceDateStr;
	}
	public Timestamp getReturnToServiceDate() {
		return returnToServiceDate;
	}
	public void setReturnToServiceDate(Timestamp returnToServiceDate) {
		this.returnToServiceDate = returnToServiceDate;
	}
	@Override
	public String toString() {
		return "XmpWebOrderLines [orderLineId=" + orderLineId
				+ ", orderHeaderId=" + orderHeaderId
				+ ", linCustomerLineNumber=" + linCustomerLineNumber
				+ ", pnrPartNumber=" + pnrPartNumber + ", partId=" + partId
				+ ", unpUnitPrice=" + unpUnitPrice + ", icrOrderCurrency="
				+ icrOrderCurrency + ", qtoOrderedQuantity="
				+ qtoOrderedQuantity + ", untUnitOfMeasure=" + untUnitOfMeasure
				+ ", ssdPromiseDate=" + ssdPromiseDate + ", shtShipToSiteId="
				+ shtShipToSiteId + ", qtnQuoteReference=" + qtnQuoteReference
				+ ", qtpQuotePrice=" + qtpQuotePrice + ", sdcShipBeforeFlag="
				+ sdcShipBeforeFlag + ", priShipmentPriority="
				+ priShipmentPriority + ", acnAircraftRegistrationNum="
				+ acnAircraftRegistrationNum + ", pdpPriceDiscountPercentage="
				+ pdpPriceDiscountPercentage + ", lseLeaseIndicator="
				+ lseLeaseIndicator + ", inpInitialProvisioningFlag="
				+ inpInitialProvisioningFlag + ", sprSpecialRuleIndicator="
				+ sprSpecialRuleIndicator + ", remCustomerRemarks="
				+ remCustomerRemarks + ", status=" + status
				+ ", statusMessage=" + statusMessage + ", lastUpdateDate="
				+ lastUpdateDate + ", lastUpdatedBy=" + lastUpdatedBy
				+ ", lineLimitAuthorised=" + lineLimitAuthorised
				+ ", qtnAuthorised=" + qtnAuthorised + ", ipAuthorised="
				+ ipAuthorised + ", sprAuthorised=" + sprAuthorised
				+ ", focAuthorised=" + focAuthorised + ", aogAuthorised="
				+ aogAuthorised + ", alternativePartId=" + alternativePartId
				+ ", cat7Authorised=" + cat7Authorised + ", leaseAuthorised="
				+ leaseAuthorised + ", shipFromOrgId=" + shipFromOrgId
				+ ", ssdPromiseDateStr=" + ssdPromiseDateStr
				+ ", returnToServiceDateStr=" + returnToServiceDateStr
				+ "]";
	}
}