package com.ayotta.order.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name="XXM_SP_PARAMS")
public class XxmSpParams {
	@Id
	@Column (name="PARAMETER_ID") private Long parameterId;
	@Column (name="PARAMETER_NAME") private String parameterName;
	@Column (name="PARAMETER_VALUE") private String parameterValue;
	@Column (name="IS_ACTIVE") private String isActive;
	public Long getParameterId() {
		return parameterId;
	}
	public void setParameterId(Long parameterId) {
		this.parameterId = parameterId;
	}
	public String getParameterName() {
		return parameterName;
	}
	public void setParameterName(String parameterName) {
		this.parameterName = parameterName;
	}
	public String getParameterValue() {
		return parameterValue;
	}
	public void setParameterValue(String parameterValue) {
		this.parameterValue = parameterValue;
	}
	public String getIsActive() {
		return isActive;
	}
	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}
	@Override
	public String toString() {
		return "XxmSpParams [parameterId=" + parameterId + ", parameterName=" + parameterName + ", parameterValue="
				+ parameterValue + ", isActive=" + isActive + "]";
	}
	
}
