package com.ayotta.order.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Customer;
import com.ayotta.order.model.EtaSapCSales;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long>{

	public Customer findByCustomerId(Long customerId);
	
//	@Query(nativeQuery = true, value =
//	           "SELECT * FROM ETA_SAP_CUSTOMERS C WHERE UPPER(C.CUSTOMER_NAME) LIKE :customerName AND UPPER(C.CUSTOMER_NUMBER) LIKE :customerNumber")
	
	@Query(name="findEtaSapCust")
	List<Customer> getMatchingCust(String customerName, String customerNumber);
	
	@Query(nativeQuery = true, value =
	           "SELECT * FROM ETA_SAP_CUSTOMERS C WHERE UPPER(C.CUSTOMER_NAME) LIKE :customerName")
	List<Customer> getMatchingCustByCustName(@Param("customerName") String customerName);
	
	@Query(nativeQuery = true, value =
	           "SELECT * FROM ETA_SAP_CUSTOMERS C WHERE UPPER(C.CUSTOMER_NUMBER) LIKE :customerNumber")
	List<Customer> getMatchingCustByCustNum(@Param("customerNumber") String customerNumber);
	
}
