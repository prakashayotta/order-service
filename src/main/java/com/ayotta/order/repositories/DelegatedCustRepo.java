package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Customer;
import com.ayotta.order.model.DelegatedCustomer;

@Repository("delegatedCustRepo")
public interface DelegatedCustRepo extends JpaRepository<DelegatedCustomer, Long>{

	@Query(name="findDelegatedCust")
	public List<DelegatedCustomer> fetchDelegatedCust(Long customerId,String custName, String cic);
}
