package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.EtaSapCSales;

@Repository("etaSapCSalesRepository")
public interface EtaSapCSalesRepository extends JpaRepository<EtaSapCSales, Long>{
	
	@Query(name="findSapCSales")
	public List<EtaSapCSales> fetchDivisions(String customerId);

}
