package com.ayotta.order.repositories;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ayotta.order.model.EtaSapCSales;
import com.ayotta.order.model.Order;
import com.ayotta.order.model.OrderLine;
import java.lang.Integer;

@Repository("orderLineRepository")
public interface OrderLineRepository extends JpaRepository<OrderLine,Integer>{
	
	@Query(nativeQuery = true, value =
    "SELECT * FROM XMP_WEB_ORDER_LINES l where l.ORDER_HEADER_ID = :orderHeaderId"
    )
	public List<OrderLine> getOrderLine(@Param("orderHeaderId") Integer orderHeaderId);
	
	Set<OrderLine> findByOrderLineId(Integer orderlineid);

//	@Query(nativeQuery = true, value =
//		    "DELETE FROM XMP_WEB_ORDER_LINES l where ORDER_HEADER_ID = :orderHeaderId"
//		    )
//	void deleteByOrderHeaderId(@Param("orderHeaderId") Integer orderHeaderId);

}
