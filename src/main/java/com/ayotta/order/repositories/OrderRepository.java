package com.ayotta.order.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ayotta.order.model.Order;
import java.lang.Integer;
import java.util.List;
import java.lang.String;

@Repository("orderRepository")
public interface OrderRepository extends JpaRepository<Order,Integer>{

	Order findByCustomerPoNumberAndCustomerId(String cpo,Integer custId);
	
	Order findByOrderHeaderIdNotAndCustomerPoNumberAndCustomerId(Integer orderHeaderId,String cpo,Integer custId);
	
	List<Order> findByCustomerPoNumber(String customerponumber);
	
//	@Query(nativeQuery = true, value =
//		    "DELETE FROM XMP_WEB_ORDER_HEADERS H where ORDER_HEADER_ID = :orderHeaderId"
//		    )
//	void deleteByOrderHeaderId(@Param("orderHeaderId") Integer orderHeaderId);
	
	// NOTE: you have return void
	@Modifying
	@Transactional
	@Query(value="delete from Order h where orderHeaderId = ?1")
	void deleteByOrderHeaderId(Integer orderHeaderId);
}
