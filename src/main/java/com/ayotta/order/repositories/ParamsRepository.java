package com.ayotta.order.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Parameters;
import java.lang.String;
import java.util.List;

@Repository("paramsRepository")
public interface ParamsRepository extends JpaRepository<Parameters, Integer>{

	List<Parameters> findByParameterName(String parametername);
}
