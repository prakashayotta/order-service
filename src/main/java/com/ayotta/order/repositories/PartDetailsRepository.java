package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.PartDetails;

@Repository("partDetailsRepo")
public interface PartDetailsRepository extends JpaRepository<PartDetails, Integer>{
	
	@Query(name="findPartDetails")
	PartDetails getPart(Integer itemId,int orgId,String distChannel);

	@Query(name="findResellerInfo")
	String getResellerInfor(Integer itemId);
	
	@Query(name="findThirdPartyId")
	Long getThirdPartyId(Integer itemId);
	
	@Query(name="findDuesOut")
	Integer getDuesOut(Integer itemId);
	
	@Query(name="findTotalSales") 
	Integer getTotalSales(Integer itemId);
	
	@Query(name="findDelgPartnerCustId") 
	Integer getDelgPartnerCustId(String delgPartnerRef);
	
	@Query(name="findCustNumber") 
	Integer getCustNumber(Integer custId);
	
	@Query(name="findOrgId") 
	Long getOrgId(String vendorCode);
	
	@Query(name="findStrategyGroup") 
	String getStrategyGroup(Integer itemId);
	
	@Query(name="findFleet") 
	Long getFleet(String custId, String airProg, String divLine);
	
}
