package com.ayotta.order.repositories;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Part;
import com.ayotta.order.model.PartDetails;

@Repository("partRepository")
public interface PartRepository extends JpaRepository<Part, Integer>{
	
	@Query(name="findParts")
	List<Part> getAllParts(String distChannel , List<String> vendorCode, String partNumber);
	
	@Query(name="findSuperSeededParts")
	List<Part> getSuperSeededParts(ArrayList<Integer> itemIds);
	
//	@Query(name="findStocknPrice")
//	Part getNewStock(Integer itemId);
	
	

}
