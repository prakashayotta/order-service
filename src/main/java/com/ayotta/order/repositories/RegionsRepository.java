package com.ayotta.order.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Regions;
import java.lang.String;
import java.util.List;

@Repository("regionsRepository")
public interface RegionsRepository extends JpaRepository<Regions, String>{
	
	List<Regions> findByName(String name);

}
