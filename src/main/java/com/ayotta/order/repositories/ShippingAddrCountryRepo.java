package com.ayotta.order.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.ShippingAddrCountry;
import java.lang.String;
import java.util.List;

@Repository("shippingAddrCountryRepo")
public interface ShippingAddrCountryRepo extends JpaRepository<ShippingAddrCountry, String>{

	@Query(name="findShipAddrCountry")
	List<ShippingAddrCountry> findByName(String name);
}
