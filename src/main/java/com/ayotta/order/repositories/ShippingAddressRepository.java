package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.ShippingAddress;

@Repository("shippingAddrRepo")
public interface ShippingAddressRepository extends JpaRepository<ShippingAddress, Integer>{
	
	
	@Query(name="findShipToAddress")
	List<ShippingAddress> getShipToAddress(String customerId, List<String> supplierCode);
	
	@Query(name="fetchShipToAddress")
	List<ShippingAddress> fetchShipToAddress(String customerId, List<String> supplierCode, Integer site);
	
	@Query(name="findShipToAddressByDivision")
	List<ShippingAddress> getShipToAddressByDivision(String customerId, String division);
	
//	@Query(name="fetchShipToAddressNew")
//	List<ShippingAddress> fetchShipToAddressNew();

}
