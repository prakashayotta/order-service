package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.Stock;

@Repository("stockRepo")
public interface StockRepository extends JpaRepository<Stock, Integer>{

	@Query(name="findStockPart")
	List<Stock> getStockPart(Integer itemId);
}
