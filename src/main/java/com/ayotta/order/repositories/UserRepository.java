package com.ayotta.order.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ayotta.order.model.User;

@Repository
public interface UserRepository extends JpaRepository<User,Long>{
	
	public User findByUsrName(String userName);
	
	@Query(nativeQuery = true, value =
	           "SELECT * FROM XXM_SP_USERS u WHERE UPPER(u.usr_name) LIKE :username")
	List<User> findUsersWithPartOfName(@Param("username") String username);

}
