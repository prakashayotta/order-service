package com.ayotta.order.services;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Stream;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.jdbc.Work;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ayotta.order.model.Customer;
import com.ayotta.order.model.DelegatedCustomer;
import com.ayotta.order.model.EtaSapCSales;
import com.ayotta.order.model.OrderEntryBean;
import com.ayotta.order.model.Parameters;
import com.ayotta.order.model.Part;
import com.ayotta.order.model.PartDetails;
import com.ayotta.order.model.PartPriceBean;
import com.ayotta.order.model.ShippingAddress;
import com.ayotta.order.model.Stock;
import com.ayotta.order.model.XxmSpParams;
import com.ayotta.order.repositories.CustomerRepository;
import com.ayotta.order.repositories.DelegatedCustRepo;
import com.ayotta.order.repositories.EtaSapCSalesRepository;
import com.ayotta.order.repositories.PartDetailsRepository;
import com.ayotta.order.repositories.PartRepository;
import com.ayotta.order.repositories.ShippingAddressRepository;
import com.ayotta.order.repositories.StockRepository;
import com.ayotta.order.repositories.XxmSpParamsRepo;
import com.ayotta.order.web.CustomerController;

import oracle.jdbc.internal.OracleTypes;

@Service
public class CustomerService {
	
	private static final Logger logger = LoggerFactory.getLogger(CustomerService.class);
	private static Integer ORG_ID = -1;
	
	@Autowired
	CustomerRepository customerRepository;
	
	@Autowired
	DelegatedCustRepo delegatedCustRepo;
	
	@Autowired
	EtaSapCSalesRepository etaSapCSalesRepository;
	
	@Autowired
	ShippingAddressRepository shippingAddrRepo;
	
	@Autowired
	PartRepository partRepository;
	
	@Autowired
	XxmSpParamsRepo xxmSpParamsRepo;
	
	@Autowired
	PartDetailsRepository partDetailsRepo;
	
	@Autowired
	StockRepository stockRepo;
	
	@PersistenceContext
    private EntityManager em;
	
	@Autowired
	ParamsService paramsService;
	
	String distributionChannel = "";
	List<String> selectedDivisionLine = new ArrayList<String>();
	
	int totalQty = 0;
	int aogStock = 0;
	double price = 0;
	private static boolean useDuesIn = false;
	
	public List<Customer> getAllMatchingCustomers(String customerName,String customerNumber){
		List<Customer> listOfCustomers = new ArrayList<Customer>();
		String custName = "";
		String custNumber = "";
		if(customerName!= null && !customerName.equals("")) {
			custName = customerName.toUpperCase();
		}
		if(customerNumber!= null && !customerNumber.equals("")) {
			custNumber = customerNumber.toUpperCase();
		}
		
		listOfCustomers = customerRepository.getMatchingCust("%"+custName+"%","%"+custNumber+"%");
		
//		if(!custName.equals("") && !custNumber.equals("")) {
//			listOfCustomers = customerRepository.getMatchingCust("%"+custName+"%","%"+custNumber+"%");
//		}else if(!custName.equals("")){
//			listOfCustomers = customerRepository.getMatchingCustByCustName("%"+custName+"%");
//		}else if(!custNumber.equals("")) {
//			listOfCustomers = customerRepository.getMatchingCustByCustNum("%"+custNumber+"%");
//		}
		return listOfCustomers;
	}
	
	public Customer getCustomerBycustId(Long customerId) {
	    Customer customer = customerRepository.findByCustomerId(customerId);
		return customer;
	}
	
	public List<DelegatedCustomer> getDelegatedCust(Long customerId,String custName, String cic) {
		String customerName = "";
		String cicCode = "";
		if(custName!= null && !custName.equals("")) {
			customerName = custName.toUpperCase();
		}
		if(cic!= null && !cic.equals("")) {
			cicCode = cic.toUpperCase();
		}
		List<DelegatedCustomer> customer = delegatedCustRepo.fetchDelegatedCust(customerId,"%"+customerName+"%","%"+cicCode+"%");
		return customer;
	}
	
	public List<ShippingAddress> getShippingAddress(String customerId,Integer site,String partDivision){
		
		 List<EtaSapCSales> etaSapCSalesList = etaSapCSalesRepository.fetchDivisions(customerId) ;
		 String divisionLine = "";
		 
		 String salesOrg = "";
		 boolean distChl = false;
		for (int i = 0; i < etaSapCSalesList.size(); i++) {
				distributionChannel = etaSapCSalesList.get(i).getDistributionChannel();
				salesOrg = etaSapCSalesList.get(i).getOrganization();
			if (etaSapCSalesList.get(i).getBlockedStatus() == null
					|| !etaSapCSalesList.get(i).getBlockedStatus().equals("B")) {
				divisionLine += etaSapCSalesList.get(i).getDivisionLine() + ",";
					if(distributionChannel.equals("02")){
						distChl = true;
					}
			}
		}
		if (salesOrg.equals("MB")) {
			if(distChl){
				distributionChannel = "02";
			}
		}
		divisionLine = divisionLine.substring(0, divisionLine.length() - 1);
		if (divisionLine.indexOf("07") != -1 && (divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
				|| divisionLine.indexOf("03") != -1)) {
			selectedDivisionLine.add("FAP92");
			selectedDivisionLine.add("F6137");
		} else {

			if (divisionLine.indexOf("07") != -1) {
				selectedDivisionLine.add("FAP92");
			} else if ((divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
					|| divisionLine.indexOf("03") != -1)) {
				selectedDivisionLine.add("F6137");
			}
		}
		
		System.out.print("divisionLine :" +divisionLine);
		List<String> supplierCodeList = null; 
		if(divisionLine != null && divisionLine != ""){
			supplierCodeList = new ArrayList<String>(Arrays.asList(divisionLine.split(",")));
		}
		List<ShippingAddress> shipList = new ArrayList<ShippingAddress>();
		if(site!=null) {
			shipList = shippingAddrRepo.fetchShipToAddress(customerId,supplierCodeList,site);
			//shipList = shippingAddrRepo.fetchShipToAddressNew();
		}else if(partDivision!=null && !partDivision.equals("")){
			shipList = shippingAddrRepo.getShipToAddressByDivision(customerId,partDivision);
		}else {
			shipList = shippingAddrRepo.getShipToAddress(customerId,supplierCodeList);
		}
		site = null;
		partDivision = null;
		return shipList;
	}
	
	public boolean isDropShipmentAllowed(Long custId) {
		Customer customer = customerRepository.findByCustomerId(custId);
		if(customer.getDropshipmentAllowedFlag()!= null && customer.getDropshipmentAllowedFlag().equals("Y")) {
			return true;
		}
		return false;
	}
	
	public List<Part> getAllParts(String customerId,String partNumber){
		partNumber = partNumber.toUpperCase();
		List<EtaSapCSales> etaSapCSalesList = etaSapCSalesRepository.fetchDivisions(customerId) ;
		 String divisionLine = "";
		 
		 String salesOrg = "";  
		 boolean distChl = false;
		for (int i = 0; i < etaSapCSalesList.size(); i++) {
				distributionChannel = etaSapCSalesList.get(i).getDistributionChannel();
				salesOrg = etaSapCSalesList.get(i).getOrganization();
			if (etaSapCSalesList.get(i).getBlockedStatus() == null
					|| !etaSapCSalesList.get(i).getBlockedStatus().equals("B")) {
				divisionLine += etaSapCSalesList.get(i).getDivisionLine() + ",";
					if(distributionChannel.equals("02")){
						distChl = true;
					}
			}
		}
		if (salesOrg.equals("MB")) {
			if(distChl){
				distributionChannel = "02";
			}
		}
		divisionLine = divisionLine.substring(0, divisionLine.length() - 1);
		if (divisionLine.indexOf("07") != -1 && (divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
				|| divisionLine.indexOf("03") != -1)) {
			selectedDivisionLine.add("FAP92");
			selectedDivisionLine.add("F6137");
		} else {

			if (divisionLine.indexOf("07") != -1) {
				selectedDivisionLine.add("FAP92");
			} else if ((divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
					|| divisionLine.indexOf("03") != -1)) {
				selectedDivisionLine.add("F6137");
			}
		}
		
	  List<Part> finalList = new ArrayList<Part>();
	  ArrayList<Integer> itemIds = new ArrayList<Integer>();
	  Hashtable<Integer, Part> partList = new Hashtable<Integer, Part>();
	  List<String> divisionLineList = null; 
		if(divisionLine != null && divisionLine != ""){
			divisionLineList = new ArrayList<String>(Arrays.asList(divisionLine.split(",")));
		}
	  finalList = partRepository.getAllParts(distributionChannel,divisionLineList,partNumber+ "%");
	  
	  if(!finalList.isEmpty()) {
	  Stream<Part> streamFinalList = finalList.stream();
	  streamFinalList.limit(200).forEach(p -> {
		  itemIds.add(p.getItemId());
		  partList.put(p.getItemId(), p);
	  });
	  
	  List<Part> superSeedsParts = partRepository.getSuperSeededParts(itemIds);
	  superSeedsParts.forEach(superParts ->{
		  if (!partList.containsKey(superParts.getItemId())) {
			  partList.put(superParts.getItemId(),superParts);
		  }

	  });
	  ArrayList<Part> finalLst = new ArrayList(partList.values());
	  return finalLst;
	  }else {
	  return finalList;
	  }
	}
	
	public PartDetails getPartStatus(OrderEntryBean orderEntryBean) {
		orderEntryBean.setCustomerName(orderEntryBean.getCustId()+"");
		
		distributionChannel = "";
		List<String> selectedDivisionLine = new ArrayList<String>();
		String custStr = String.valueOf(orderEntryBean.getCustId());
		
		List<EtaSapCSales> etaSapCSalesList = etaSapCSalesRepository.fetchDivisions(custStr) ;
		 String divisionLine = "";
		 
		 String salesOrg = "";
		 boolean distChl = false;
		for (int i = 0; i < etaSapCSalesList.size(); i++) {
				distributionChannel = etaSapCSalesList.get(i).getDistributionChannel();
				salesOrg = etaSapCSalesList.get(i).getOrganization();
			if (etaSapCSalesList.get(i).getBlockedStatus() == null
					|| !etaSapCSalesList.get(i).getBlockedStatus().equals("B")) {
				divisionLine += etaSapCSalesList.get(i).getDivisionLine() + ",";
					if(distributionChannel.equals("02")){
						distChl = true;
					}
			}
		}
		if (salesOrg.equals("MB")) {
			if(distChl){
				distributionChannel = "02";
			}
		}
		divisionLine = divisionLine.substring(0, divisionLine.length() - 1);
		if (divisionLine.indexOf("07") != -1 && (divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
				|| divisionLine.indexOf("03") != -1)) {
			selectedDivisionLine.add("FAP92");
			selectedDivisionLine.add("F6137");
		} else {

			if (divisionLine.indexOf("07") != -1) {
				selectedDivisionLine.add("FAP92");
			} else if ((divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
					|| divisionLine.indexOf("03") != -1)) {
				selectedDivisionLine.add("F6137");
			}
		}
		if(orderEntryBean.getDelgRef() != null && orderEntryBean.getDelgRef() != "") {
			orderEntryBean.setDelgRef("DELG"+orderEntryBean.getDelgRef());
		}
		orderEntryBean.setDistChannel(distributionChannel);
		orderEntryBean.setSalesOrg(salesOrg);
		orderEntryBean.setSelectedDivisionLine(selectedDivisionLine);
		
		PartPriceBean partPriceBean =new PartPriceBean();
		Part part = null;
		
		PartDetails localPart = getNewStock(orderEntryBean.getItemId());
		Long orgId = 0L;
		EtaSapCSales custSales = new EtaSapCSales();
		
		Integer customerNumber = null;
		Integer delgCustomerId = null;
		String delgPartnerRef = "";
		Integer delgCustomerNumber = null;
		if(orderEntryBean.getDelgRef() != null && !orderEntryBean.getDelgRef().equals("")){
			orderEntryBean.setIsDelgFlag(true);
			if(orderEntryBean.getDelgRef().lastIndexOf("DELG") > -1){
				delgPartnerRef = orderEntryBean.getDelgRef().replace("DELG","");
			}
			delgCustomerId = getDelgPartnerCustId(delgPartnerRef);
			if(delgCustomerId != null) {
				delgCustomerNumber = getCustomerNumber(delgCustomerId);
			}

		}else{
			
		}
		customerNumber = getCustomerNumber(new Integer(orderEntryBean.getCustId()));
		Integer tpsCustChk = null;
		if(delgCustomerId != null && delgCustomerId > 0){
			tpsCustChk = delgCustomerId;
		}else{
			tpsCustChk = Integer.valueOf(orderEntryBean.getCustomerName());
		}
		
		if( localPart.getTpsCustId().intValue() >0 && tpsCustChk != localPart.getTpsCustId().intValue() ){
			localPart.setTps(true);
			localPart = loadPriceMsgnAvailability(localPart,orderEntryBean);
			return localPart;
		}
		
		if (orderEntryBean.getIcr() == null || orderEntryBean.getIcr().length() <= 0 || orderEntryBean.getCustomerName() == null
				|| "".equals(orderEntryBean.getCustomerName())) {
			localPart = loadPriceMsgnAvailability(localPart,orderEntryBean);
			return localPart;
		}
		
		if(orderEntryBean.getSelectedDivisionLine() != null) {
			for(String vendor : orderEntryBean.getSelectedDivisionLine()){
				orgId = getOrgId(vendor);
				if(orgId>0){
					break;
				}
			}
		}
		
		custSales = getCustSalesData(new Integer(orderEntryBean.getCustId()), orgId, localPart.getDivisionLine());
		
		HashMap<String,String> parameters = new HashMap<String,String>();
		parameters.put("p_priceProc", "ZRECHA");
		parameters.put("p_salesOrg", orderEntryBean.getSalesOrg());
		parameters.put("p_distChannel", orderEntryBean.getDistChannel());
		parameters.put("p_airProg", localPart.getAirProg());
		parameters.put("p_part", localPart.getPartNumber());
		parameters.put("p_customer", customerNumber+"");
		parameters.put("p_currency", orderEntryBean.getIcr());		
		if (custSales != null) {
			parameters.put("p_keyCustomer", custSales.getKeyCustomer());
			parameters.put("p_priceList", custSales.getPriceList());
		}
		parameters.put("p_custId", orderEntryBean.getCustomerName());
		parameters.put("p_divLine", orderEntryBean.getDistChannel());
		
		if(orderEntryBean.getIsDelgFlag() != null && orderEntryBean.getIsDelgFlag()){
			if(orderEntryBean.getIsAogOrderFlag()){
				parameters.put("p_ordTypeUId", "ZDAO");
			}else{
				parameters.put("p_ordTypeUId", "ZDCO");
			}
			parameters.put("p_endUser", delgCustomerNumber+"");
		}
		
		Double retVal = 0.0d;
		boolean priceFlag = false;
		
		try {
			boolean isFleet = false;
			
			if(localPart.getDivisionLine() != null && !localPart.getDivisionLine().equals("")){
				if(localPart.getDivisionLine().equals("01") || localPart.getDivisionLine().equals("02")){
					if(orderEntryBean.getItemId() > 0){
						localPart.setPartStrategyGrp(fetchStrategyGroup(orderEntryBean.getItemId()));
					}
					if (localPart.getPartStrategyGrp() != null
							&& ("ZZ".equals(localPart.getPartStrategyGrp()) || "CAT5"
									.equals(localPart.getPartStrategyGrp()))) {
						logger.debug("IF price");
						retVal = getPriceProcDelgAog(parameters);
					}
				}else{
					logger.debug("ELSE price");
					retVal = getPriceProcDelgAog(parameters);
				}
			}
			
			//retVal = getPriceProc(parameters);
			if(retVal > 0){
				priceFlag = true;
			}else{
				priceFlag = false;
			}
			if(localPart.getDivisionLine() != null && !localPart.getDivisionLine().equals("03") && !priceFlag){
				Integer custId  = orderEntryBean.getCustId();
				isFleet = getFleet(String.valueOf(custId), localPart.getAirProg(), localPart.getDivisionLine());
				if(isFleet){
					localPart.setPrice(retVal.doubleValue());
				}
			} else {
				localPart.setPrice(retVal.doubleValue());
			}
		} catch (Exception ex){
			ex.printStackTrace();
		}
		localPart.setIcr(orderEntryBean.getIcr());
		localPart.setPublish(true);
		localPart.setIsAogOrderFlag(orderEntryBean.getIsAogOrderFlag());
		localPart.setIsDelgFlag(orderEntryBean.getIsDelgFlag());
		
		
		//price msg and availability
		localPart = loadPriceMsgnAvailability(localPart,orderEntryBean);
		return localPart;
	}
	
	public PartDetails loadPriceMsgnAvailability(PartDetails localPart,OrderEntryBean orderEntryBean) {
		if(localPart.getPrice() == null || localPart.getPrice() == 0.0) {
			localPart.setPriceMsg("This part does not have a price enabled for "+orderEntryBean.getCustomerName()+", you may however place an order,"
			 		+ "<br>the order acknowledgment will advise pricing details.");
			 if(orderEntryBean.getDelgRef() != ""){
				 localPart.setPriceMsg("The End User/Airline selected<br> has no available price for this part.");
			 }
		 }
		 
		 if(localPart.isTps() && localPart.getReseller() != null) {
			 List<Parameters>  params = paramsService.getParamValue("THIRD_PARTY_MESSAGE_"+localPart.getReseller().toUpperCase());
			 params.forEach(param -> {
				 if(param.getParameterValue() != null && !param.getParameterValue().equals("")) {
					 localPart.setPriceMsg(param.getParameterValue());
				 }else {
					 if(localPart.getReseller() != null) {
						 localPart.setPriceMsg("This part is supplied by "+localPart.getReseller()+". Please contact the sales team.");
					 }else {
						 localPart.setPriceMsg("This part is supplied by third party service, contact the sales team.");
					 }
				 }
			 });
		 }
		 
		 if(localPart.getTotalStock()<= 0) {
			 localPart.setAvailability("0");
		 }else if(localPart.getStockRatio() == 0) {
			 if(localPart.getTotalStock() > 0) {
				 localPart.setAvailability("AVAILABLE");
			 }
		 }else {
			 if(localPart.getStockRatio() >= 1) {
				 localPart.setAvailability("AVAILABLE");
			 }else {
				 localPart.setAvailability("LIMITED AVAILABILITY");
			 }
		 }
		 
		 return localPart;
	}
	private boolean getFleet(String custId, String airProg, String divLine) throws SQLException{
		
		String divText = "";
		
		logger.debug("Entered getFleet");
		
		Map<String, String> parameters = new HashMap<String, String>();
		
		if(divLine.equals("07")){
			divText = "CS_FLEET_LG";
		}else if(divLine.contains("01") && (divLine.contains("02"))){
			divText = "CS_FLEET_SE";
		}else if(divLine.equals("03")){
			divText = "CS_FLEET_WB";
		}else{
			divText = "";
		}
		
		Long retVal = partDetailsRepo.getFleet(custId, airProg, divText);
		
		if(retVal > 0){
			return true;
		}
		return false;
	}
	
	private double getPriceProc(HashMap<String,String> parameters) throws SQLException{
		logger.debug("Entered getPrice");
		logger.debug("Parameter Values :"+parameters);
		price = 0;
		Session session = getSession();
    	session.doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
				
		CallableStatement cstmt = connection
				.prepareCall("CALL ETA.ETA_PRICING.pricing_procedure(?,?,?,?,?,?,?,?,?,?)");
		cstmt.setString(1, parameters.get("p_priceProc"));
		cstmt.setString(2, parameters.get("p_salesOrg"));
		cstmt.setString(3, parameters.get("p_distChannel"));
		cstmt.setString(4, parameters.get("p_airProg"));
		cstmt.setString(5, parameters.get("p_part"));
		cstmt.setString(6, parameters.get("p_customer"));
		cstmt.setString(7, parameters.get("p_currency"));
		cstmt.setString(8, parameters.get("p_keyCustomer"));
		cstmt.setString(9, parameters.get("p_priceList"));
		cstmt.registerOutParameter(10, OracleTypes.NUMBER);
		cstmt.execute();
		price = cstmt.getDouble(10);
		cstmt.close();
		connection.close();		
		logger.debug("SQL Parameters : " + parameters);
		logger.debug("getPrice Result: " + price);
		if(price < 0)			
			price = 0.0d;
         }
    	});
		
		return price;
	}
	
	private double getPriceProcDelgAog(HashMap<String,String> parameters) throws SQLException{
		price = 0;
		logger.debug("Entered getPriceProcDelgAog");
		logger.debug("Parameter Values :"+parameters);
		Session session = getSession();
    	session.doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
		CallableStatement cstmt = connection
				.prepareCall("CALL ETA.ETA_PRICING.pricing_procedure(?,?,?,?,?,?,?,?,?,?,?,?)");
		cstmt.setString(1, parameters.get("p_priceProc"));
		cstmt.setString(2, parameters.get("p_salesOrg"));
		cstmt.setString(3, parameters.get("p_distChannel"));
		cstmt.setString(4, parameters.get("p_airProg"));
		cstmt.setString(5, parameters.get("p_part"));
		cstmt.setString(6, parameters.get("p_customer"));
		cstmt.setString(7, parameters.get("p_currency"));
		cstmt.setString(8, parameters.get("p_keyCustomer"));
		cstmt.setString(9, parameters.get("p_priceList"));
		cstmt.setString(10, parameters.get("p_ordTypeUId"));
		cstmt.setString(11, parameters.get("p_endUser"));
		cstmt.registerOutParameter(12, OracleTypes.NUMBER);
		cstmt.execute();
		price = cstmt.getDouble(12);
		cstmt.close();
//		connection.close();		
		logger.debug("SQL Parameters : " + parameters);
		logger.debug("getPriceProcDelgAog(getPrice Result) : " + price);
		if(price < 0)			
			price = 0.0d;
			}
			
    	});
		return price;
	}

	public String fetchStrategyGroup(Integer partId) {
		String returnVal= null;
		returnVal = partDetailsRepo.getStrategyGroup(partId);
		
		return returnVal;
	}

    private EtaSapCSales getCustSalesData(Integer custId, Long orgId, String divisionLine) {
    	EtaSapCSales sapCustSales = new EtaSapCSales();
    	
    	Session session = getSession();
    	session.doWork(new Work() {

			@Override
			public void execute(Connection connection) throws SQLException {
				// TODO Auto-generated method stub
				
				CallableStatement cstmt = connection
						.prepareCall("CALL ETA.get_cust_sales_data(?,?,?,?,?,?,?)");
				cstmt.setInt(1, custId);
				cstmt.setLong(2, orgId);
				cstmt.setString(3, divisionLine);
				cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(6, OracleTypes.VARCHAR);
				cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
				cstmt.execute();
				sapCustSales.setDistributionChannel(cstmt.getString(4));
				sapCustSales.setCivilMilitaryIndicator(cstmt.getString(5));
				sapCustSales.setPriceList(cstmt.getString(6));
				sapCustSales.setKeyCustomer(cstmt.getString(7));
				cstmt.close();
//				connection.close();
			}
    			
    	});
    
		
				
		logger.debug("getPrice Result: " + sapCustSales.toString());
		
		return sapCustSales;
	}
    
    private Session getSession() {
        // get session from entitymanager. Assuming hibernate
        return em.unwrap(org.hibernate.Session.class);
    }

	public PartDetails getNewStock(Integer itemId) {
		Part part = null;
		totalQty = 0;
		aogStock = 0;
		PartDetails returnPart = getPart(itemId,getMasterOrgId(),"02");
		returnPart.setReseller(getResellerInfo(itemId));
		returnPart.setTpsCustId(getThirdPartyId(itemId));
		
		List<Stock> stockList = stockRepo.getStockPart(itemId);
		
		if (stockList != null){
			stockList.forEach(stock -> {
				boolean displayable = false;
				if(stock.getDisplayInPortal() != null && stock.getDisplayInPortal().equals("Y") &&
						stock.getSalableIndicator() != null && stock.getSalableIndicator().equals("NEW")) {
					displayable = true;
					stock.setDisplayForExternal(displayable);
				}
				
				if(stock.isDisplayForExternal()) {
					totalQty += stock.getStockQty();
					aogStock += stock.getAogStock();
					if (totalQty <= 1) {
						stock.setStockQty(0);
						
					}
				}
				
				boolean add = true;
				if (stock.getStockQty() > 1) {
//					System.out.println(returnPart.getStockList().size());
//					for (int j = 0; j < returnPart.getStockList().size(); j++) {
//						if (j == 0)
//							add = false;
//						Stock tempStock = returnPart.getStockList().get(j);
//						if (stock.subEquals(tempStock)) {
//							
//							tempStock.setStockQty(tempStock.getStockQty()
//									+ stock.getStockQty());
//							add = false;
//							j = returnPart.getStockList().size();
//						} else {
//							add = true;
//						}
//					}
//					if (add && stock.isDisplayForExternal()) {
//						returnPart.getStockList().add(stock);
//					}
				}
			});
			
		}
		returnPart.setTotalStock(totalQty);
		
       if (returnPart.getTotalStock() > 0){
			
			Integer duesOut = getDuesOut(returnPart.getItemId());
			returnPart.setTotalStock((int) (returnPart.getTotalStock()-duesOut-aogStock));

		}
       
       int totalSales = getTotalSales(itemId);
       double totalSalesFact= 0.0d;
       double leadTime = 0.0d;
       
       if(returnPart.getLeadTime() != null){
			leadTime = returnPart.getLeadTime();
		}
       
       if(leadTime == 0){
			leadTime = 1;
		}
       
       double catalogLeadTime = 730/leadTime;
       
       if(totalSales > 0){
			
			//sales factor
			totalSalesFact = Math.floor(totalSales/catalogLeadTime);
			logger.debug("Sales Factor: "+totalSalesFact);
		}
	   
       double salesFactor = 0.0d;
		if(totalSalesFact>0){
			salesFactor = returnPart.getTotalStock()/totalSalesFact;
		}		
		returnPart.setStockRatio(salesFactor);	
		return returnPart;
	}
	
	PartDetails getPart(Integer itemId, int orgId, String distChannel) {
		PartDetails returnPart = partDetailsRepo.getPart(itemId,orgId,distChannel);
		return returnPart;
	}
	
	@SuppressWarnings("unchecked")
	public int getMasterOrgId() {
		try {
			if (ORG_ID == -1) {
			 XxmSpParams param = xxmSpParamsRepo.findByParameterName("XXM_MASTER_ORG");
			 if(param.getParameterValue() != null){
				ORG_ID = Integer.valueOf(param.getParameterValue()); 
				return ORG_ID;
			 }
			}
		} catch (Exception ex) {
			ex.printStackTrace();

		}
		return 306;
	}

	
	public String getResellerInfo(Integer itemId) {
		return partDetailsRepo.getResellerInfor(itemId);
	}
	
	public Long getThirdPartyId(Integer itemId) {
		 Long thirdPartyId = partDetailsRepo.getThirdPartyId(itemId);
		 if(thirdPartyId == null)
			 thirdPartyId = 0l;
		 return thirdPartyId;
	}
	
	public Integer getDuesOut(Integer itemId) {
		Integer duesOut = partDetailsRepo.getDuesOut(itemId);
		if(duesOut==null) {
			duesOut = 0;
		}
		return duesOut;
	}
	
	public int getTotalSales(Integer itemId) {
		Integer retVal = 0;
		retVal = partDetailsRepo.getTotalSales(itemId);
		if(retVal == null) {
			retVal = 0;
		}
		return retVal;
	}
	
	public Integer getDelgPartnerCustId(String delgPartnerRef) {
		return partDetailsRepo.getDelgPartnerCustId(delgPartnerRef);
	}
	
	private Integer getCustomerNumber(Integer custId){
		if (custId != null){
			return partDetailsRepo.getCustNumber(custId);
		}
		return null;
	}
	
	public Long getOrgId(String vendorCode) {
		return partDetailsRepo.getOrgId(vendorCode);
	}
	
	public String getDistributionChannel() {
		return distributionChannel;
	}

	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}

	public List<String> getSelectedDivisionLine() {
		return selectedDivisionLine;
	}

	public void setSelectedDivisionLine(List<String> selectedDivisionLine) {
		this.selectedDivisionLine = selectedDivisionLine;
	}
	

}
