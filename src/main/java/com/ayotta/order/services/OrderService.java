package com.ayotta.order.services;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ayotta.order.config.MailProperties;
import com.ayotta.order.config.MessageQProperties;
import com.ayotta.order.exception.RecordNotFoundException;
import com.ayotta.order.exception.ValidationException;
import com.ayotta.order.model.Currency;
import com.ayotta.order.model.Customer;
import com.ayotta.order.model.DelegatedCustomer;
import com.ayotta.order.model.EtaSapCSales;
import com.ayotta.order.model.Order;
import com.ayotta.order.model.OrderEntryBean;
import com.ayotta.order.model.OrderLine;
import com.ayotta.order.model.Parameters;
import com.ayotta.order.model.Part;
import com.ayotta.order.model.PartDetails;
import com.ayotta.order.model.Regions;
import com.ayotta.order.model.ShippingAddrCountry;
import com.ayotta.order.model.ShippingAddress;
import com.ayotta.order.model.WebOrder;
import com.ayotta.order.model.XmpWebOrderHeaders;
import com.ayotta.order.model.XmpWebOrderLines;
import com.ayotta.order.repositories.EtaSapCSalesRepository;
import com.ayotta.order.repositories.OrderLineRepository;
import com.ayotta.order.repositories.OrderRepository;
import com.ayotta.order.repositories.PartRepository;
import com.ayotta.order.repositories.RegionsRepository;
import com.ayotta.order.repositories.ShippingAddrCountryRepo;
import com.google.common.base.CharMatcher;
import com.sun.messaging.ConnectionFactory;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;


@Service
public class OrderService {
	
	private static final Logger logger = LoggerFactory.getLogger(OrderService.class);
	
	@Autowired
    private JavaMailSender javaMailSender;
	
	@Autowired
	OrderRepository orderRepository;
	
	@Autowired
	OrderLineRepository orderLineRepository;
	
	@Autowired
	CustomerService customerService;
	
	@Autowired
	ShippingAddrCountryRepo shippingAddrCountryRepo;
	
	@Autowired
	RegionsRepository regionsRepository;
	
	@Autowired
	ParamsService paramsService;
	
	@Autowired
	EntityManager em;
	
	@Autowired
	ConnectionFactory remoteJms;
	
	@Autowired
    MessageQProperties mqProperties;
	
	@Autowired
	MailProperties mailProperties;
	
	@Autowired
	EtaSapCSalesRepository etaSapCSalesRepository;
	
	@Autowired
	PartRepository partRepository;
	
	boolean isDivision = false;
	
	//Fetch order details:
	@SuppressWarnings("unchecked")
	public Order getOrderDetails(String customerPoNumber) {

		List<Order> orderHeader = orderRepository.findByCustomerPoNumber(customerPoNumber);
		if(!orderHeader.isEmpty()) {
			String sql = "SELECT l.orderLineId FROM OrderLine l WHERE ORDER_HEADER_ID=" + orderHeader.get(0).getOrderHeaderId();
			List<Integer> idList = new ArrayList<Integer>();
			idList = em.createQuery(sql).getResultList();
			System.out.println(idList);
			Set<OrderLine> ordLine = new HashSet<OrderLine>();
			idList.forEach((docIdList) -> {// one notification id may contain multiple notification documents
				Set<OrderLine> ordLineOfId = new HashSet<OrderLine>();
				OrderLine orderLine = new OrderLine();
				ordLineOfId =  orderLineRepository.findByOrderLineId(docIdList);
				if (ordLineOfId != null && !ordLineOfId.isEmpty()) {
					orderLine.setOrderLineId(ordLineOfId.iterator().next().getOrderLineId());
					orderLine.setPartId(ordLineOfId.iterator().next().getPartId());
					orderLine.setPartNumber(ordLineOfId.iterator().next().getPartNumber());
					orderLine.setCustomerLineNumber(ordLineOfId.iterator().next().getCustomerLineNumber());
					orderLine.setOrderedQuantity(ordLineOfId.iterator().next().getOrderedQuantity());
					System.out.println("date: "+ordLineOfId.iterator().next().getPromiseDateStr());
					if(ordLineOfId.iterator().next().getPromiseDateStr()!= null) {
						String dateStr = changeDateFormat(ordLineOfId.iterator().next().getPromiseDateStr());
						orderLine.setPromiseDateStr(dateStr);
					}
					orderLine.setCustomerRemarks(ordLineOfId.iterator().next().getCustomerRemarks());
					if(ordLineOfId.iterator().next().getFreeOfCharge()!= null && 
							ordLineOfId.iterator().next().getFreeOfCharge().equals("100")) {
						orderLine.setFreeOfCharge("Y");
					}
					orderLine.setInitialProvisioning(ordLineOfId.iterator().next().getInitialProvisioning());
					orderLine.setContractNumber(ordLineOfId.iterator().next().getContractNumber());
					orderLine.setShipToSiteId(ordLineOfId.iterator().next().getShipToSiteId());
					orderLine.setDelgRef(ordLineOfId.iterator().next().getDelgRef());
					orderLine.setDropshipIndicator(ordLineOfId.iterator().next().getDropshipIndicator());
					orderLine.setDropshipCustomer(ordLineOfId.iterator().next().getDropshipCustomer());
					orderLine.setDropshipAddress1(ordLineOfId.iterator().next().getDropshipAddress1());
					orderLine.setDropshipAddress2(ordLineOfId.iterator().next().getDropshipAddress2());
					orderLine.setDropshipAddress3(ordLineOfId.iterator().next().getDropshipAddress3());
					orderLine.setDropshipAddress4(ordLineOfId.iterator().next().getDropshipAddress4());
					orderLine.setDropshipCountry(ordLineOfId.iterator().next().getDropshipCountry());
					orderLine.setDsRegn(ordLineOfId.iterator().next().getDsRegn());
					orderLine.setDsZip(ordLineOfId.iterator().next().getDsZip());
					orderLine.setDropshipCpo(ordLineOfId.iterator().next().getDropshipCpo());
//					orderLine.setLastUpdateDate(ordLineOfId.iterator().next().getLastUpdateDate());
					orderLine.setPriShipmentPriority(ordLineOfId.iterator().next().getPriShipmentPriority());
					
					orderLine.setOrder(null);
					ordLine.add(orderLine);
					
				}
			});
			orderHeader.get(0).setOrderLine(ordLine);
			return orderHeader.get(0);
		}else {
			throw new RecordNotFoundException("customerPoNumber :-" + customerPoNumber);
		}
	}
	
	public String changeDateFormat(String dateStr) {
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd");
	    SimpleDateFormat format2 = new SimpleDateFormat("dd-MMM-yyyy");
	    try {
			Date date = format1.parse(dateStr.substring(0,10));
			return format2.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    return null;
	}
	//Save order details:
	public Order save(Order webOrder){
		if(webOrder != null ) {
			
			Order order= validate(webOrder);
            
			if(order.getOrderLine()!=null && !order.getOrderLine().isEmpty()) {
				Set<OrderLine> orderLineList = new HashSet<OrderLine>();
				OrderLine orderLine = new OrderLine();
				orderLineList = order.getOrderLine();
				Iterator<OrderLine> iteratorList = orderLineList.iterator();
				while(iteratorList.hasNext()) {
					orderLine = iteratorList.next();
					orderLine.setOrder(order);
					orderLine.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
					System.out.println("orderLine:"+orderLine);
					orderLineRepository.save(orderLine);
				}
				
//				order = orderRepository.save(order);
//				logger.info("Order header Id : "+ order.getOrderHeaderId());
//				for (OrderLine orderLine: order.getOrderLine()) {
//					orderLine.setOrderHeaderId(order.getOrderHeaderId());
//					orderLineRepository.save(orderLine);
//				}
				
			}
			order.setCreatedDate(new Timestamp(System.currentTimeMillis()));
			orderRepository.save(order);
			logger.debug("Order saved successfully...");
		}else {
			logger.debug("Order not saved...");
		}
		return webOrder;
	}
	
	public Order validate(Order webOrder) {
		
		logger.debug("validation on");
		Customer customer = customerService.getCustomerBycustId(Long.valueOf(webOrder.getCustomerId()));
		
		if (customer == null)
		      throw new RecordNotFoundException("customerId-" + webOrder.getCustomerId());
		
		String[] customerSplit = customer.getCustomerName().split("\\(");
		webOrder.setCustomerName(customerSplit[0]);
		
		String cpo = webOrder.getCustomerPoNumber();
		Pattern special = Pattern.compile("[!@#$%&*()+=|<>?{}\\[\\]~]");
		Matcher returnChar  = special.matcher(cpo);
		boolean flaVal = returnChar.find();
		Order order = null;
		if(webOrder.getOrderHeaderId()!=null && !webOrder.getOrderHeaderId().equals("") && webOrder.getOrderHeaderId()>0) {
			order = orderRepository.findByOrderHeaderIdNotAndCustomerPoNumberAndCustomerId(webOrder.getOrderHeaderId(),
					webOrder.getCustomerPoNumber(),webOrder.getCustomerId());
		}else {
			order = orderRepository.findByCustomerPoNumberAndCustomerId(webOrder.getCustomerPoNumber(),webOrder.getCustomerId());
		}
		if(flaVal) {
			 throw new ValidationException("customerPoNumber -" + webOrder.getCustomerPoNumber()+" "+"The use of special characters is limited to the following /\\.-_");
		}
		if(order!= null)
			throw new ValidationException("customerPoNumber -" + webOrder.getCustomerPoNumber()+" "+"This Purchase Order Number already exists.");
		
		boolean currencyFlag = contains(webOrder.getOrderCurrency());
		if(!currencyFlag) {
			throw new ValidationException("orderCurrency -" + webOrder.getOrderCurrency());
		}
		
		validateShipAddr(webOrder);
		validateOrdLine(webOrder);
		return webOrder;
	}
	public void validateShipAddr(Order webOrder) {
		
		boolean isDropShip = customerService.isDropShipmentAllowed(Long.valueOf(webOrder.getCustomerId()));
		if(isDropShip) {
			if((webOrder.getShipToSiteId() == null || webOrder.getShipToSiteId() <= 0) && 
					(webOrder.getDropshipIndicator() == null || webOrder.getDropshipIndicator().trim().equals(""))	) {
				throw new ValidationException("Please enter shiptosideId or set dropshipindicator Y and enter dropship address fields");
			}
		}else {
			if(webOrder.getShipToSiteId() == null || webOrder.getShipToSiteId() <= 0) {
				
				if(webOrder.getDropshipIndicator()!= null && !webOrder.getDropshipIndicator().equals("")) {
					System.out.println(webOrder.getDropshipIndicator()); 
					throw new ValidationException("Dropship is not allowed for this customer");
				}else {
					throw new ValidationException("Please enter shiptosideId ");	
				}
			}
		}
		
		
		if((webOrder.getShipToSiteId() != null && webOrder.getShipToSiteId() > 0) &&
			(webOrder.getDropshipIndicator() != null && webOrder.getDropshipIndicator().trim().toUpperCase().equals("Y")) ) {
			throw new ValidationException("Please enter any one of the address either shipToSiteId or dropship address");
		}else if(webOrder.getShipToSiteId() != null && webOrder.getShipToSiteId() != 0) {
		List<ShippingAddress> shipList = customerService.getShippingAddress(String.valueOf(webOrder.getCustomerId()),null,null);
		ShippingAddress shippingAddress = shipList.stream().filter(addr -> webOrder.getShipToSiteId() == (int) (long)addr.getSite())
				  .findAny()
				  .orElse(null);
		  if(shippingAddress ==null || !shippingAddress.getSite().equals(Integer.valueOf(webOrder.getShipToSiteId()))) {
			throw new ValidationException("ShiptoSite is not valid");
		   }
		}else if(webOrder.getDropshipIndicator().equals("Y")) {
			boolean isDropShipment = customerService.isDropShipmentAllowed(Long.valueOf(webOrder.getCustomerId()));
			if(isDropShipment) {
				if(webOrder.getDropshipCountry() !=null && !webOrder.getDropshipCountry().equals("")) {
					List<ShippingAddrCountry> countryList = shippingAddrCountryRepo.findByName(webOrder.getDropshipCountry().toUpperCase());
					if(countryList.isEmpty()) {
						throw new RecordNotFoundException("dropshipCountry:-" + webOrder.getDropshipCountry());
					}else {
						List<Regions> regionList = regionsRepository.findByName(webOrder.getDsRegn());
						if(regionList.isEmpty()) {
							throw new RecordNotFoundException("dsRegn:-" + webOrder.getDsRegn());
						}else {
							webOrder.setDropshipCtryCode(regionList.get(0).getCountryCode());
							webOrder.setDsRegn(regionList.get(0).getRegionCode());
							for(ShippingAddrCountry c :countryList) {
								for(Regions g : regionList) {
									if(c.getCode().equals(g.getCountryCode())) {
										String exp = regExGeneration(c.getRule_exp());
										if(c.getRule()!=null && c.getRule().equals("1")) {
											
											if (!(webOrder.getDsZip().indexOf(' ') == -1)) {
												
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "No Spaces Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("2")) {
											if (!(webOrder.getDsZip().indexOf(' ') == -1) || !isNumeric(webOrder.getDsZip())) {
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "Only Numbers. No Spaces Allowed");		
											}
										}else if(c.getRule()!=null && c.getRule().equals("3")) {
											if (!(webOrder.getDsZip().indexOf(' ') == -1)
													|| webOrder.getDsZip().length() != Integer.valueOf(c.getLength())) {
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "No Spaces Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("4")) {
											if (!(webOrder.getDsZip().indexOf(' ') == -1) || !isNumeric(webOrder.getDsZip())
													|| webOrder.getDsZip().length() != Integer.valueOf(c.getLength())) {
												
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "Only Numbers. No Spaces Allowed."
														+ "The Length Should be of "+c.getLength());
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("5")) {
											
										}else if(c.getRule()!=null && c.getRule().equals("6")) {
											if (!isNumeric(webOrder.getDsZip())) {
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "Only Numbers Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("7")) {
											
											if (webOrder.getDsZip().length() != Integer.valueOf(c.getLength())) {
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("8")) {
											if (!isNumeric(webOrder.getDsZip()) || webOrder.getDsZip().length() != Integer.valueOf(c.getLength())) {
												throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "Only Numbers Allowed");
												
											}
										}else if(c.getRule()!=null && c.getRule().equals("9")) {
											if(exp!= null && exp !=""){
												Pattern retExp = Pattern.compile(exp);
												
												Matcher returnChr  = retExp.matcher(webOrder.getDsZip());
												boolean retVal = returnChr.find();
												if(!retVal) {
													throw new ValidationException("dsZip -" + webOrder.getDsZip() +" "+ "The Entered Zipcode Doesnot Match the Country Specific Format");
												}
											}
											
										}
									}else {
										throw new RecordNotFoundException("dsRegn:-" + webOrder.getDsRegn());
									}
								}
								
							}
						}
						
					}
				}
			}else {
				throw new ValidationException("Drop shipment address not allowed for this customer.Please remove your dropshipment address.");
				
			}
		}else {
			throw new ValidationException("Please select the any one of the address mode");
		}
	}
	
    public void validateShipAddrLine(Order webOrd,OrderLine orderLine, String partDivisionLine) {
		
		if((orderLine.getShipToSiteId() != null && orderLine.getShipToSiteId() > 0) &&
			(orderLine.getDropshipIndicator() != null && orderLine.getDropshipIndicator().trim().toUpperCase().equals("Y")) ) {
			throw new ValidationException("Please enter any one of the address either shipToSiteId or dropship address");
		}else if(orderLine.getShipToSiteId() != null && orderLine.getShipToSiteId() != 0) {
		List<ShippingAddress> shipList = customerService.getShippingAddress(String.valueOf(webOrd.getCustomerId()),null,null);
		ShippingAddress shippingAddress = shipList.stream().filter(addr -> orderLine.getShipToSiteId() == (int) (long)addr.getSite())
				  .findAny()
				  .orElse(null);
		  if(shippingAddress ==null || !shippingAddress.getSite().equals(Integer.valueOf(orderLine.getShipToSiteId()))) {
			throw new ValidationException("ShiptoSite is not valid");
		   }else {
			checkDivisionwithLine(String.valueOf(webOrd.getCustomerId()),webOrd.getShipToSiteId(),partDivisionLine);
		   }
		}else if(orderLine.getDropshipIndicator().equals("Y")) {
			boolean isDropShip = customerService.isDropShipmentAllowed(Long.valueOf(webOrd.getCustomerId()));
			
			if(isDropShip) {
				if(orderLine.getDropshipCountry() !=null && !orderLine.getDropshipCountry().equals("")) {
					List<ShippingAddrCountry> countryList = shippingAddrCountryRepo.findByName(orderLine.getDropshipCountry().toUpperCase());
					if(countryList.isEmpty()) {
						throw new RecordNotFoundException("dropshipCountry:-" + orderLine.getDropshipCountry());
					}else {
						List<Regions> regionList = regionsRepository.findByName(orderLine.getDsRegn());
						if(regionList.isEmpty()) {
							throw new RecordNotFoundException("dsRegn:-" + orderLine.getDsRegn());
						}else {
							orderLine.setDropshipCtryCode(regionList.get(0).getCountryCode());
							orderLine.setDsRegn(regionList.get(0).getRegionCode());
							for(ShippingAddrCountry c :countryList) {
								for(Regions g : regionList) {
									if(c.getCode().equals(g.getCountryCode())) {
										String exp = regExGeneration(c.getRule_exp());
										if(c.getRule()!=null && c.getRule().equals("1")) {
											
											if (!(orderLine.getDsZip().indexOf(' ') == -1)) {
												
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "No Spaces Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("2")) {
											if (!(orderLine.getDsZip().indexOf(' ') == -1) || !isNumeric(orderLine.getDsZip())) {
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "Only Numbers. No Spaces Allowed");		
											}
										}else if(c.getRule()!=null && c.getRule().equals("3")) {
											if (!(orderLine.getDsZip().indexOf(' ') == -1)
													|| orderLine.getDsZip().length() != Integer.valueOf(c.getLength())) {
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "No Spaces Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("4")) {
											if (!(orderLine.getDsZip().indexOf(' ') == -1) || !isNumeric(orderLine.getDsZip())
													|| orderLine.getDsZip().length() != Integer.valueOf(c.getLength())) {
												
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "Only Numbers. No Spaces Allowed."
														+ "The Length Should be of "+c.getLength());
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("5")) {
											
										}else if(c.getRule()!=null && c.getRule().equals("6")) {
											if (!isNumeric(orderLine.getDsZip())) {
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "Only Numbers Allowed");
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("7")) {
											
											if (orderLine.getDsZip().length() != Integer.valueOf(c.getLength())) {
											}
											
										}else if(c.getRule()!=null && c.getRule().equals("8")) {
											if (!isNumeric(orderLine.getDsZip()) || orderLine.getDsZip().length() != Integer.valueOf(c.getLength())) {
												throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "Only Numbers Allowed");
												
											}
										}else if(c.getRule()!=null && c.getRule().equals("9")) {
											if(exp!= null && exp !=""){
												Pattern retExp = Pattern.compile(exp);
												
												Matcher returnChr  = retExp.matcher(orderLine.getDsZip());
												boolean retVal = returnChr.find();
												if(!retVal) {
													throw new ValidationException("dsZip -" + orderLine.getDsZip() +" "+ "The Entered Zipcode Doesnot Match the Country Specific Format");
												}
											}
											
										}
									}else {
										throw new RecordNotFoundException("dsRegn:-" + orderLine.getDsRegn());
									}
								}
								
							}
						}
						
					}
				}
			}else {
				throw new ValidationException("Drop shipment address not allowed for this customer.Please remove your dropshipment address.");
				
			}
		}else {
			if((orderLine.getShipToSiteId() == null || orderLine.getShipToSiteId() <= 0) &&
					(orderLine.getDropshipIndicator() == null || orderLine.getDropshipIndicator().trim().equals("")) ) {
					
				  if(webOrd.getShipToSiteId()!=null && webOrd.getShipToSiteId() > 0) {
					  checkDivisionwithLine(String.valueOf(webOrd.getCustomerId()),webOrd.getShipToSiteId(),partDivisionLine);
					  
					  orderLine.setShipToSiteId(webOrd.getShipToSiteId());
					  isDivision = false;
				  }else if(webOrd.getDropshipIndicator()!= null && !webOrd.getDropshipIndicator().trim().equals("")) {
					  orderLine.setDropshipIndicator(webOrd.getDropshipIndicator());
				  }
				  
				}
		}
		
	}

    public boolean checkDivisionwithLine(String custId,Integer site,String partDivisionLine) {
    	List<ShippingAddress> shipList=customerService.getShippingAddress(custId,null,null);
		 
		  shipList.forEach(shipaddr -> {
			  if(shipaddr.getSite().equals(site)) {
				  if(shipaddr.getDivisionLine().equals(partDivisionLine)) {
					  isDivision = true;
				  }
			  }
		  });
		  if (!isDivision) {
				throw new ValidationException(
						"Please change the Shipping address for the line as, <br> this part cannot be shipped to Selected Default Address");
		  }
		  return isDivision;
    }
    
	public Order validateOrdLine(Order webOrder) {
		List<Integer> lineNos = new ArrayList<Integer>();
		
		webOrder.getOrderLine().forEach(line -> {
			Integer count = null;
			Integer moq = 1;
			Integer spq = 1;
			boolean isMoq = false;
			if(line.getPartNumber() != null && !line.getPartNumber().equals("")) {
		    
			if(line.getCustomerLineNumber()!=null & !line.getCustomerLineNumber().equals("")) {
			    count = Integer.valueOf(line.getCustomerLineNumber());
			}else {
				throw new ValidationException("Line no:-"+count+" "+ "Customer line number is mandatory");
			}
			if (!lineNos.contains(Integer.valueOf(line.getCustomerLineNumber()))) {
				 lineNos.add(Integer.valueOf(line.getCustomerLineNumber()));
			} else {
				 throw new ValidationException("Duplicate customer line number");
			}
			String custIdStr = String.valueOf(webOrder.getCustomerId());
			List<Part> listOfPart = checkPartNo(custIdStr, line.getPartNumber());
			if(listOfPart.isEmpty()) {
				throw new ValidationException("Line no:-"+count+" "+ "Part number is not valid ");
			}else {
				listOfPart.forEach(part->{
					line.setPartId(part.getItemId());
				});
			}
		    
		    if(line.getOrderedQuantity()== null ||  line.getOrderedQuantity().equals("")) {
		    	throw new ValidationException("Line no:-"+count+" "+ "Order quantity is mandatory");
		    }
		    
		    if(line.getPromiseDateStr() == null || line.getPromiseDateStr().equals("")) {
		    	throw new ValidationException("Line no:-"+count+" "+ "Promise Date/Customer Required Date is mandatory");
		    }
		   
			OrderEntryBean orderEntryBean = new OrderEntryBean();
			orderEntryBean.setCustId(webOrder.getCustomerId());
			orderEntryBean.setCustomerName(webOrder.getCustomerName());
			orderEntryBean.setIcr(webOrder.getOrderCurrency());
			if(line.getDelgRef()!=null && !line.getDelgRef().equals("")) {
			  orderEntryBean.setDelgRef(line.getDelgRef().toUpperCase());
			  orderEntryBean.setIsDelgFlag(true);
			  line.setIsDelgFlag(true);
			}
			orderEntryBean.setIsAogOrderFlag(line.getIsAogOrderFlag());
			orderEntryBean.setItemId(line.getPartId());
			
			PartDetails partstatus = customerService.getPartStatus(orderEntryBean);
			validateShipAddrLine(webOrder,line,partstatus.getDivisionLine());
			if(line.getDelgRef()!= null && !line.getDelgRef().equals("")) {
				List<DelegatedCustomer> customerList = customerService.getDelegatedCust(Long.valueOf(webOrder.getCustomerId()), "", line.getDelgRef());
				
				if(customerList != null && customerList.size()>0) {
					for(DelegatedCustomer  cust: customerList){
						if(cust.getDelegatedPartRef().equals(line.getDelgRef().toUpperCase()) && (partstatus.getPrice() == null || partstatus.getPrice() == 0 )) {
							throw new ValidationException("Line no:-"+count+ " " +"delgRef: "+line.getDelgRef()+" The End User/Airline selected has no available price for this part");
						}else {
							throw new ValidationException("Line no:-"+count+ " " +"delgRef: "+line.getDelgRef()+" <span>This End User/Airline not valid.</span><BR>");
						}
					}
				}else {
					throw new ValidationException("Line no:-"+count+ " " +"delgRef: "+line.getDelgRef()+" <span>This End User/Airline not valid for this customer.</span><BR>");
				}
			}
			
			if(count == 1) {
			if (partstatus.getDivisionLine().equals("07")){
				webOrder.setSupplierCode("FAP92");
			}else if((partstatus.getDivisionLine().equals("01") || partstatus.getDivisionLine().equals("02") ||partstatus.getDivisionLine().equals("03"))){
				webOrder.setSupplierCode("F6137");
			}
			}
			
			if(partstatus.isTps() && partstatus.getReseller() != null) {
				 List<Parameters>  params = paramsService.getParamValue("THIRD_PARTY_MESSAGE_"+partstatus.getReseller().toUpperCase());
				 params.forEach(param -> {
					 if(param.getParameterValue() != null && !param.getParameterValue().equals("")) {
						 partstatus.setPriceMsg(param.getParameterValue());
						 throw new ValidationException(param.getParameterValue());
					 }else {
						 if(partstatus.getReseller() != null) {
							 throw new ValidationException("This part is supplied by "+partstatus.getReseller()+". Please contact the sales team.");
						 }else {
							 throw new ValidationException("This part is supplied by third party service, contact the sales team.");
						 }
					 }
				 });
			 }
			
			if(partstatus.getPrice()!=null && (partstatus.getPrice()>0.0 || partstatus.getPrice()>0)) {
				line.setUnitPrice(partstatus.getPrice().floatValue());
			}
			
			if(partstatus.getPartNumber()!=null && !partstatus.equals("")) {
				line.setPartNumber(partstatus.getPartNumber());
			}
			
			if(partstatus.getUom()!=null && !partstatus.getUom().equals("")) {
				line.setUntUnitOfMeasure(partstatus.getUom());
			}
			
			moq = partstatus.getMinimumOrderQuantity();
			moq=(moq.equals("") || moq==0) ? 1: moq;
			spq = partstatus.getStandardPackageQuantity();
			spq=(spq.equals("") || spq==0) ? 1: spq;
			
			if((partstatus.getMinimumOrderQuantity() != null && partstatus.getMinimumOrderQuantity() > 1) || 
					(partstatus.getStandardPackageQuantity() != null && partstatus.getStandardPackageQuantity() > 1)){
				isMoq = true;
			}
			moq = moq.equals("")?0:moq;
			spq = spq.equals("")?0:spq;
			
			if(moq != null && spq != null){
				if(moq < spq){
					moq = spq;
				}
			}
			
			if(moq == null || moq<=0
					|| moq == 0.0){
				throw new ValidationException("Line no:-"+count+ "Invalid Quantity");
			}else{
				// For removing leading zeros
//				var reqQty = parseInt(qtyVal);
				if(isMoq){
					if(Integer.valueOf(line.getOrderedQuantity())<moq){
						throw new ValidationException("Line no:-"+count+ " " +"MOQ applicable");
					}else{
						Integer mod = (Integer.valueOf(line.getOrderedQuantity())-moq)%spq;
						if(mod > 0){
							throw new ValidationException("Line no:-"+count+ "Please order in multiples of SPQ");
						}
					}
				}
			}
			System.out.println("date: "+line.getPromiseDateStr());
			Date ssdDate = null;
			SimpleDateFormat excelDf = new SimpleDateFormat("dd-MMM-yyyy");
			String priority = null;
			if(line.getPromiseDateStr() != null && !line.getPromiseDateStr().equals("")){
				try {
					int dateNumber = 0;
				Calendar cal = Calendar.getInstance();
				cal.set(1900,  Calendar.JANUARY, 1);
				
				if (line.getPromiseDateStr().length() <= 7) {
					try {
						dateNumber = (int) Float.parseFloat(line.getPromiseDateStr());
						cal.add(Calendar.DATE, dateNumber);
						ssdDate =cal.getTime();
					} catch (Exception ex){
						dateNumber = -1;
					}
				}
				if (ssdDate == null && line.getPromiseDateStr().length() > 7){
					ssdDate = excelDf.parse(line.getPromiseDateStr());
				}
				} catch(Exception ex){
					logger.error("Date error "+ ex);
				}
				if (ssdDate == null) {
					throw new ValidationException("Line no:-"+count+ " " +"Invalid Customer Required Date: "+line.getPromiseDateStr()); 
					
				}
				if (ssdDate != null){
//    				line.setSsdPromiseDateStr(excelDf.format(ssdDate));
    				priority = linesPriority(ssdDate);
				}
				
				if(priority != null && !priority.equals("")){
					line.setPriShipmentPriority(priority);
				}else{
					throw new ValidationException("Line no:-"+count+ " " +"Customer Required Date : "+line.getPromiseDateStr()+" <span>Customer Required Date cannot be in the past.</span><BR>"); 
				}
			}else{
				throw new ValidationException("Line no:-"+count+ " " +"Customer Required Date : "+line.getPromiseDateStr()+" <span>This field is required.</span><BR>");  
			}
			
			if(line.getCustomerRemarks() != null && line.getCustomerRemarks().length()>2000) {
				throw new ValidationException("Line no:-"+count+ " " +"Remarks contains more than 2000 characters....");  
			}
			
			checkExtendedAsciiChar(line.getCustomerRemarks(),count); 
			if(line.getFreeOfCharge() != null && !line.getFreeOfCharge().equals("") && 
					(line.getFreeOfCharge().equals("Y") || line.getFreeOfCharge().equals("N"))){
				if(line.getFreeOfCharge().equals("Y")){
					line.setFreeOfCharge("100");        					
    			}
    		}
			
			if(line.getFreeOfCharge()!=null && line.getFreeOfCharge().equals("100")){
				line.setUnitPrice(0f);
			}
			
			if(webOrder.getOrderCurrency()!=null && !webOrder.getOrderCurrency().equals("")) {
				line.setOrderCurrency(webOrder.getOrderCurrency());
			}
			
			if(line.getInitialProvisioning() == null || line.getInitialProvisioning().equals("")){
				if(partstatus.getDivisionLine().equals("01") || partstatus.getDivisionLine().equals("02") || partstatus.getDivisionLine().equals("07")){
				   throw new ValidationException("Line no:-"+count+" "+ "Initial Provisioning is mandatory"); 
				}else {
					line.setInitialProvisioning("N"); 
				}
			}else {
				if(!containsIpVals(line.getInitialProvisioning())) {
					throw new ValidationException("Line no:-"+count+" "+ "Invalid Initial Provisioning"); 
				}
			}
			
			
			}else {
				throw new ValidationException("Line no:-"+count+" "+ "Part number is mandatory");
			}
		});
		
		
		return webOrder;
	}
	
	public void checkDivisionLine() {
		
	}
	
	public List<Part> checkPartNo(String customerId, String partNumber) {
		String distributionChannel = "";
		List<String> selectedDivisionLine = new ArrayList<String>();
		
		partNumber = partNumber.toUpperCase();
		List<EtaSapCSales> etaSapCSalesList = etaSapCSalesRepository.fetchDivisions(customerId) ;
		 String divisionLine = "";
		 
		 String salesOrg = "";
		 boolean distChl = false;
		for (int i = 0; i < etaSapCSalesList.size(); i++) {
				distributionChannel = etaSapCSalesList.get(i).getDistributionChannel();
				salesOrg = etaSapCSalesList.get(i).getOrganization();
			if (etaSapCSalesList.get(i).getBlockedStatus() == null
					|| !etaSapCSalesList.get(i).getBlockedStatus().equals("B")) {
				divisionLine += etaSapCSalesList.get(i).getDivisionLine() + ",";
					if(distributionChannel.equals("02")){
						distChl = true;
					}
			}
		}
		if (salesOrg.equals("MB")) {
			if(distChl){
				distributionChannel = "02";
			}
		}
		divisionLine = divisionLine.substring(0, divisionLine.length() - 1);
		if (divisionLine.indexOf("07") != -1 && (divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
				|| divisionLine.indexOf("03") != -1)) {
			selectedDivisionLine.add("FAP92");
			selectedDivisionLine.add("F6137");
		} else {

			if (divisionLine.indexOf("07") != -1) {
				selectedDivisionLine.add("FAP92");
			} else if ((divisionLine.indexOf("01") != -1 || divisionLine.indexOf("02") != -1
					|| divisionLine.indexOf("03") != -1)) {
				selectedDivisionLine.add("F6137");
			}
		}
	  List<Part> finalList = new ArrayList<Part>();
	  ArrayList<Integer> itemIds = new ArrayList<Integer>();
	  Hashtable<Integer, Part> partList = new Hashtable<Integer, Part>();
	  List<String> divisionLineList = null; 
		if(divisionLine != null && divisionLine != ""){
			divisionLineList = new ArrayList<String>(Arrays.asList(divisionLine.split(",")));
		}
	  finalList = partRepository.getAllParts(distributionChannel,divisionLineList,partNumber);
	  
	  if(!finalList.isEmpty()) {
	  Stream<Part> streamFinalList = finalList.stream();
	  streamFinalList.limit(200).forEach(p -> {
		  itemIds.add(p.getItemId());
		  partList.put(p.getItemId(), p);
	  });
	  
	  List<Part> superSeedsParts = partRepository.getSuperSeededParts(itemIds);
	  superSeedsParts.forEach(superParts ->{
		  if (!partList.containsKey(superParts.getItemId())) {
			  partList.put(superParts.getItemId(),superParts);
		  }

	  });
	  ArrayList<Part> finalLst = new ArrayList(partList.values());
	  return finalLst;
	  }else {
	  return finalList;
	  }
	  
	}
	
	public void checkExtendedAsciiChar(String remarks,int count){
		if(remarks != null) {
			List<Integer> charCode = new ArrayList<Integer>();
			for (int i=0;i<remarks.length();i++){
				charCode.add((int)remarks.charAt(i));
			}
			logger.debug("charCode: "+charCode);
			boolean cm = CharMatcher.inRange((char)1, (char)128).matchesAllOf(remarks);
			if(!cm){
				throw new ValidationException("Line no:-"+count+ " " +"Special Characters not allowed in remarks.");
			}
		}
	}
	
	public Integer specialCharValidation(String fieldValueStr) {
		try {			
			if(fieldValueStr != null && !fieldValueStr.equals("")){
				//SPRECIAL CHAR CHK
				Pattern pattern = Pattern.compile("[a-zA-Z0-9]*");
				Matcher matcher = pattern.matcher(fieldValueStr);
				if (matcher.matches()) {
					if (fieldValueStr.matches("\\-?\\d+")) {
						return (Integer.valueOf(fieldValueStr));					
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public String linesPriority(Date ssdDate) {
		logger.debug("Ssd_date : "+ ssdDate);
		String priority = "";
		try {

			Date todayDate = new Date();
			todayDate = priorityDate(todayDate);
			
			Date wsp = new Date();
			wsp.setDate(wsp.getDate()+1);
			wsp = priorityDate(wsp);
			
			Date usr = new Date();
			usr.setDate(usr.getDate()+2);
			usr = priorityDate(usr);
			
			Date lastSevenDays = new Date();
			lastSevenDays.setDate(lastSevenDays.getDate()+7);
			lastSevenDays = priorityDate(lastSevenDays);
			
			if(ssdDate.getTime() == todayDate.getTime()){
				priority = "AOG";
			}else if(ssdDate.getTime() == wsp.getTime()){
				priority = "WSP";
			}else if(ssdDate.getTime() >= usr.getTime() && ssdDate.getTime() <= lastSevenDays.getTime()){
				priority = "USR";
			}else if(ssdDate.getTime() < todayDate.getTime()){
				priority = "";
			}else {
				priority = "NLT";
			}
			logger.debug("priority :"+ priority);
		} catch (Exception e) {
			logger.error("linesPriority "+ e);
		}
		return priority;
	}
	
	public static Date priorityDate(Date date){
		DateFormat format = new SimpleDateFormat("dd-MMM-yyyy");
		try {
			Date priDate = format.parse(format.format(date));
			return priDate;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public boolean isNumeric(String str) {
		try{
			  int num = Integer.parseInt(str);
			  return true;
			} catch (NumberFormatException e) {
			  return false;
			}
	}
	public String regExGeneration(String ruleExp){
		String result="";
		if(ruleExp !=null && ruleExp!=""){
//			result="^";
			char[] ruleArray = ruleExp.toCharArray(); 
			for (int i=0;i<ruleArray.length;i++){
				if(ruleArray[i]=='A'){
					result+="([a-zA-Z])";
				}else if(ruleArray[i]=='N'){
					result+="([0-9])";
				}else if(ruleArray[i]==' '){
					result+=" ";
				}else{
					result+=ruleArray[i];
				}
			}
//			result+="$";
		}
		return result;		
	}
	//Update order details:
	/*public void updateOrderDetails(Integer orderHeaderId, Order webOrder) throws Exception {
		Order orderObj = new Order();
		orderObj =  orderRepository.getOne(orderHeaderId);
		if(orderObj != null && checkNull(orderObj.getOrderHeaderId())) {
			webOrder.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
			//webOrder.setLastUpdatedBy(lastUpdatedBy);
			orderRepository.save(webOrder);
			logger.debug("Order updated successfully...");
		}else {
			logger.debug("Order not updated...");
		}
	}*/
	
	@SuppressWarnings("unchecked")
	public Order updateOrderDetails(Order webOrder) {
		
		Order orderObj = new Order();
		//orderObj =  orderRepository.getOne(orderHeaderId);
		orderObj = getOrderDetails(webOrder.getCustomerPoNumber());
		
		if((orderObj != null && (orderObj.getOrderHeaderId()!= null && orderObj.getOrderHeaderId()>0)) && 
				(orderObj.getSubmittedDate() == null || orderObj.getSubmittedDate().equals(""))) {
			Order webOrderHdr= validate(webOrder);
			
			webOrder.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
			//webOrder.setLastUpdatedBy(lastUpdatedBy);
			
			if(webOrder.getCustomerId()!= null) {
				orderObj.setCustomerId(webOrderHdr.getCustomerId());
			}
			if(webOrder.getCustomerPoNumber()!=null) {
				orderObj.setCustomerPoNumber(webOrderHdr.getCustomerPoNumber());
			}
			if(webOrder.getOrderCurrency()!=null) {
				orderObj.setOrderCurrency(webOrderHdr.getOrderCurrency());
			}
			if(webOrder.getShipToSiteId()!=null) {
				orderObj.setShipToSiteId(webOrderHdr.getShipToSiteId());
			}
			if(webOrder.getSupplierCode()!=null) {
				orderObj.setSupplierCode(webOrderHdr.getSupplierCode());
			}
			if(webOrder.getDropshipIndicator()!=null) {
				orderObj.setDropshipIndicator(webOrderHdr.getDropshipIndicator());
			}
			if(webOrder.getDropshipCustomer()!=null) {
				orderObj.setDropshipCustomer(webOrderHdr.getDropshipCustomer());
			}
			if(webOrder.getDropshipAddress1()!=null) {
				orderObj.setDropshipAddress1(webOrderHdr.getDropshipAddress1());
			}
			if(webOrder.getDropshipAddress2()!=null) {
				orderObj.setDropshipAddress2(webOrderHdr.getDropshipAddress2());
			}
			if(webOrder.getDropshipAddress3()!=null) {
				orderObj.setDropshipAddress3(webOrderHdr.getDropshipAddress3());
			}
			if(webOrder.getDropshipAddress4()!=null) {
				orderObj.setDropshipAddress4(webOrderHdr.getDropshipAddress4());
			}
			if(webOrder.getDropshipCountry()!=null) {
				orderObj.setDropshipCountry(webOrderHdr.getDropshipCountry());
			}
			if(webOrder.getDsRegn()!=null) {
				orderObj.setDsRegn(webOrderHdr.getDsRegn());
			}
			if(webOrder.getDsZip()!=null) {
				orderObj.setDsZip(webOrderHdr.getDsZip());
			}
			if(webOrder.getDropshipCpo()!=null) {
				orderObj.setDropshipCpo(webOrderHdr.getDropshipCpo());
			}
			
			if(webOrderHdr.getOrderLine()!=null && !webOrderHdr.getOrderLine().isEmpty()) {
				orderObj.setOrderLine(webOrderHdr.getOrderLine());
				List<Integer> idList = new ArrayList<Integer>();
				String sql = "SELECT l.orderLineId FROM OrderLine l WHERE ORDER_HEADER_ID=" + webOrder.getOrderHeaderId();
				idList = em.createQuery(sql).getResultList();
				if(idList == null || idList.isEmpty()) {
					Set<OrderLine> lines = new HashSet<OrderLine>();	
					OrderLine  line= new OrderLine();
					lines=webOrderHdr.getOrderLine();
					Iterator<OrderLine> it = lines.iterator();
					while(it.hasNext()) {
						line = it.next();
						line.setOrder(orderObj);
						line.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
						orderLineRepository.save(line);
					}
				}else {
					Set<OrderLine> lines = new HashSet<OrderLine>();	
					OrderLine  line= new OrderLine();
					lines=webOrderHdr.getOrderLine();
					Iterator<OrderLine> it = lines.iterator();
					while(it.hasNext()) {
						line = it.next();
						if(line.getOrderLineId()!= null) {
							OrderLine orderLine = new OrderLine();
							orderLine = orderLineRepository.getOne(line.getOrderLineId());
							
							if (line.getPartId() != null)
								orderLine.setPartId(line.getPartId());
							
							if (line.getPartNumber() != null)
								orderLine.setPartNumber(line.getPartNumber());
							
							if (line.getCustomerLineNumber() != null)
								orderLine.setCustomerLineNumber(line.getCustomerLineNumber());
							
							if (line.getOrderedQuantity() != null)
								orderLine.setOrderedQuantity(line.getOrderedQuantity());
							
							if (line.getPromiseDateStr() != null)
								orderLine.setPromiseDateStr(line.getPromiseDateStr());
							
							if (line.getCustomerRemarks() != null)
								orderLine.setCustomerRemarks(line.getCustomerRemarks());
							
							if (line.getFreeOfCharge() != null)
								orderLine.setFreeOfCharge(line.getFreeOfCharge());
	
							if (line.getInitialProvisioning() != null)
								orderLine.setInitialProvisioning(line.getInitialProvisioning());
							
							if (line.getContractNumber() != null)
								orderLine.setContractNumber(line.getContractNumber());
							
							if (line.getShipToSiteId() != null)
								orderLine.setShipToSiteId(line.getShipToSiteId());
							
							if (line.getDelgRef() != null)
								orderLine.setDelgRef(line.getDelgRef());
							
							if (line.getIsDelgFlag() != null)
								orderLine.setIsDelgFlag(line.getIsDelgFlag());
							
							if (line.getDropshipIndicator() != null)
								orderLine.setDropshipIndicator(line.getDropshipIndicator());
							
							if (line.getDropshipCustomer() != null)
								orderLine.setDropshipCustomer(line.getDropshipCustomer());
							
							if (line.getDropshipAddress1() != null)
								orderLine.setDropshipAddress1(line.getDropshipAddress1());
							
							if (line.getDropshipAddress2() != null)
								orderLine.setDropshipAddress2(line.getDropshipAddress2());
							
							if (line.getDropshipAddress3() != null)
								orderLine.setDropshipAddress3(line.getDropshipAddress3());
							
							if (line.getDropshipAddress4() != null)
								orderLine.setDropshipAddress4(line.getDropshipAddress4());
							
							if (line.getDropshipCountry() != null)
								orderLine.setDropshipCountry(line.getDropshipCountry());
							
							if (line.getDsRegn() != null)
								orderLine.setDsRegn(line.getDsRegn());
							
							if (line.getDsZip() != null)
								orderLine.setDsZip(line.getDsZip());
							
							if (line.getDropshipCpo() != null)
								orderLine.setDropshipCpo(line.getDropshipCpo());
							
							orderLine.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
							orderLineRepository.save(orderLine);
						}else {
							line.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
							line.setOrder(orderObj);
							orderLineRepository.save(line);
						}
					}
//					idList.forEach(lineId -> {
//						OrderLine orderLine = new OrderLine();
//						orderLine = orderLineRepository.getOne(lineId);
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getPartId() != null)
//							orderLine.setPartId(webOrderHdr.getOrderLine().iterator().next().getPartId());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getCustomerLineNumber() != null)
//							orderLine.setCustomerLineNumber(webOrderHdr.getOrderLine().iterator().next().getCustomerLineNumber());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getOrderedQuantity() != null)
//							orderLine.setOrderedQuantity(webOrderHdr.getOrderLine().iterator().next().getOrderedQuantity());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getPromiseDateStr() != null)
//							orderLine.setPromiseDateStr(webOrderHdr.getOrderLine().iterator().next().getPromiseDateStr());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getCustomerRemarks() != null)
//							orderLine.setCustomerRemarks(webOrderHdr.getOrderLine().iterator().next().getCustomerRemarks());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getFreeOfCharge() != null)
//							orderLine.setFreeOfCharge(webOrderHdr.getOrderLine().iterator().next().getFreeOfCharge());
//
//						if (webOrderHdr.getOrderLine().iterator().next().getInitialProvisioning() != null)
//							orderLine.setInitialProvisioning(webOrderHdr.getOrderLine().iterator().next().getInitialProvisioning());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getContractNumber() != null)
//							orderLine.setContractNumber(webOrderHdr.getOrderLine().iterator().next().getContractNumber());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getShipToSiteId() != null)
//							orderLine.setShipToSiteId(webOrderHdr.getOrderLine().iterator().next().getShipToSiteId());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDelgRef() != null)
//							orderLine.setDelgRef(webOrderHdr.getOrderLine().iterator().next().getDelgRef());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getIsDelgFlag() != null)
//							orderLine.setIsDelgFlag(webOrderHdr.getOrderLine().iterator().next().getIsDelgFlag());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipIndicator() != null)
//							orderLine.setDropshipIndicator(webOrderHdr.getOrderLine().iterator().next().getDropshipIndicator());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipCustomer() != null)
//							orderLine.setDropshipCustomer(webOrderHdr.getOrderLine().iterator().next().getDropshipCustomer());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipAddress1() != null)
//							orderLine.setDropshipAddress1(webOrderHdr.getOrderLine().iterator().next().getDropshipAddress1());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipAddress2() != null)
//							orderLine.setDropshipAddress2(webOrderHdr.getOrderLine().iterator().next().getDropshipAddress2());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipAddress3() != null)
//							orderLine.setDropshipAddress3(webOrderHdr.getOrderLine().iterator().next().getDropshipAddress3());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipAddress4() != null)
//							orderLine.setDropshipAddress4(webOrderHdr.getOrderLine().iterator().next().getDropshipAddress4());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipCountry() != null)
//							orderLine.setDropshipCountry(webOrderHdr.getOrderLine().iterator().next().getDropshipCountry());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDsRegn() != null)
//							orderLine.setDsRegn(webOrderHdr.getOrderLine().iterator().next().getDsRegn());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDsZip() != null)
//							orderLine.setDsZip(webOrderHdr.getOrderLine().iterator().next().getDsZip());
//						
//						if (webOrderHdr.getOrderLine().iterator().next().getDropshipCpo() != null)
//							orderLine.setDropshipCpo(webOrderHdr.getOrderLine().iterator().next().getDropshipCpo());
//						
//						orderLine.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
//						orderLineRepository.save(orderLine);
//					});
				}
			}
			webOrder.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
			orderRepository.save(orderObj);
			logger.debug("Order updated successfully...");
			return orderObj;
		}else {
     		logger.debug("Order not updated...");
     		if(orderObj.getSubmittedDate()!= null && !orderObj.getSubmittedDate().equals("")) {
     			throw new ValidationException("Record already submitted");
     		}
     		throw new RecordNotFoundException("customerPoNumber-" + webOrder.getCustomerPoNumber());
		}
	}
	
	//Delete order details:
	@Transactional
	public String deleteOrderDetails(String customerPoNumber) {
		if (customerPoNumber != null && !customerPoNumber.equals("")) {
			List<Order> order = orderRepository.findByCustomerPoNumber(customerPoNumber);
			if(!order.isEmpty()) {
				if((order.get(0).getSubmittedDate()== null || order.get(0).getSubmittedDate().equals(""))) {
					orderRepository.deleteByOrderHeaderId(order.get(0).getOrderHeaderId());
					
					Query query = em.createNativeQuery("DELETE FROM XMP_WEB_ORDER_LINES WHERE ORDER_HEADER_ID = " + order.get(0).getOrderHeaderId());
					int returnVal = query.executeUpdate();
				
					logger.debug("Order deleted successfully...");
				}else {
					throw new ValidationException("customerPoNumber-" + customerPoNumber +" "+ "is already submitted");
				}
			}else {
				logger.debug("Order not deleted...");
				throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
			}
		}else {
			logger.debug("Order not deleted...");
			throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
		}
		return customerPoNumber;
	}
	
	@Transactional
	public String submitOrder(String customerPoNumber) {
		if (customerPoNumber != null && !customerPoNumber.equals("")) {
			List<Order> order = orderRepository.findByCustomerPoNumber(customerPoNumber);
			if(!order.isEmpty()) {
                if((order.get(0).getSubmittedDate()== null || order.get(0).getSubmittedDate().equals(""))) {
                	order.get(0).setSubmittedDate(new Timestamp(System.currentTimeMillis()));
                	orderRepository.save(order.get(0));
                	Order orderObj = getOrderDetails(customerPoNumber);
                	if(orderObj.getOrderLine() != null && orderObj.getOrderLine().size() > 0) {
                		processAOG(orderObj);
                		sendMessage(orderObj);
                	}else {
                		logger.debug("Line is Empty for CPO: "+customerPoNumber);
                	}
                	logger.debug("Order submitted successfully...");
                }else {
                	throw new ValidationException("customerPoNumber-" + customerPoNumber +" "+ "is already submitted");
                }
			}else {
				logger.debug("Order not submitted...");
				throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
			}
		}else {
			logger.debug("Order not submitted...");
			throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
		}
		return customerPoNumber;
	}
	
	public void processAOG(Order orderObj) {

		boolean aogFlag= false;;
		for(OrderLine linesData : orderObj.getOrderLine()){
			if(linesData.getPriShipmentPriority()!=null && linesData.getPriShipmentPriority().equals("AOG")){
				aogFlag =true;
			}
		}
		
		if(aogFlag){
			ObjectMapper mapper = new ObjectMapper();
			try {
				String orderAsString = mapper.writeValueAsString(orderObj);
				
				MimeMessage message = javaMailSender.createMimeMessage();
				logger.debug("MailFrom: "+mailProperties.getMailFrom());
				MimeMessageHelper helper = null;
				helper = new MimeMessageHelper(message, true);
				helper.setFrom(mailProperties.getMailFrom());
					if (mailProperties.getMailAogTo() != null && !mailProperties.getMailAogTo().equals("")) {
						if (mailProperties.getMailAogTo().indexOf(",") >0){
							helper.setTo(mailProperties.getMailAogTo().split(","));
						} else 
							helper.setTo(mailProperties.getMailAogTo());
					}
					else
						helper.setTo(mailProperties.getSchemaMailTo());
					
					helper.setSubject("B2B MESSAGE: "
							+ "WEB"
							+ " INBOUND AOG MESSAGE @"
							+ new SimpleDateFormat("dd-MM-yyyy hh:mm:ss")
									.format(new Date()));
					
					helper.setText(
							"The following AOG Message has arrived and will be sent to Oracle.\n"
									+ orderAsString + "");
					
					javaMailSender.send(message);
					logger.info("SENT_AOG_MAIL TO \t" + mailProperties.getMailAogTo() + ": ");
				
			} catch (JsonMappingException e) {
				e.printStackTrace();
	        } catch( IOException ex) {
	        	ex.printStackTrace();
	        } catch (MailException exception) {
	            exception.printStackTrace();
	        } catch (MessagingException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void sendMessage(Order orderObj) {
		logger.debug("Enter the sendUserMessage...");
		
		
		WebOrder messageBean = new WebOrder();
		XmpWebOrderHeaders orderHdr = new XmpWebOrderHeaders();
		
		orderHdr.setOrderHeaderId(orderObj.getOrderHeaderId());
		orderHdr.setCustomeridCustomerId(orderObj.getCustomerId());
		orderHdr.setCustomernameCustomerName(orderObj.getCustomerName());
		orderHdr.setCpoCustomerPoNumber(orderObj.getCustomerPoNumber());
		orderHdr.setIcrOrderCurrency(orderObj.getOrderCurrency());
		orderHdr.setDsindDropshipIndicator(orderObj.getDropshipIndicator());
		orderHdr.setDscustDropshipCustomer(orderObj.getDropshipCustomer());
		orderHdr.setDsadd1DropshipAddress1(orderObj.getDropshipAddress1());
		orderHdr.setDsadd2DropshipAddress2(orderObj.getDropshipAddress2());
		orderHdr.setDsadd3DropshipAddress3(orderObj.getDropshipAddress3());
		orderHdr.setDsadd4DropshipAddress4(orderObj.getDropshipAddress4());
		orderHdr.setDsctryDropshipCountry(orderObj.getDropshipCountry());
		orderHdr.setDsctrycodeDropshipCtryCode(orderObj.getDropshipCtryCode());
		orderHdr.setDscpoDropshipCpo(orderObj.getDropshipCpo());
		orderHdr.setLastUpdateDate(orderObj.getLastUpdateDate());
		orderHdr.setShtShipToSiteId(orderObj.getShipToSiteId());

		messageBean.setOrderHdr(orderHdr);
		List<XmpWebOrderLines> linesList = new ArrayList<XmpWebOrderLines>();
		
		orderObj.getOrderLine().forEach(lineObj ->{
			XmpWebOrderLines line = new XmpWebOrderLines();
			
			line.setOrderLineId(lineObj.getOrderLineId());
			line.setOrderHeaderId(orderObj.getOrderHeaderId());
			line.setLinCustomerLineNumber(lineObj.getCustomerLineNumber());
			line.setPnrPartNumber(lineObj.getPartNumber());
			line.setPartId(lineObj.getPartId());
			if(lineObj.getUnitPrice()!=null) {
				line.setUnpUnitPrice(Double.valueOf(lineObj.getUnitPrice()));
			}
			line.setIcrOrderCurrency(lineObj.getOrderCurrency());
			line.setQtoOrderedQuantity(Integer.valueOf(lineObj.getOrderedQuantity()));
			line.setUntUnitOfMeasure(lineObj.getUntUnitOfMeasure());
//			line.setSsdPromiseDate(lineObj.getPromiseDate());
			line.setSsdPromiseDateStr(lineObj.getPromiseDateStr());
			line.setShtShipToSiteId(lineObj.getShipToSiteId());
//			line.setQtnQuoteReference(rs.getString("QTN_QUOTE_REFERENCE"));
//			line.setQtpQuotePrice(rs.getInt("QTP_QUOTE_PRICE"));
//			line.setSdcShipBeforeFlag(rs.getString("SDC_SHIP_BEFORE_FLAG"));
			line.setPriShipmentPriority(lineObj.getPriShipmentPriority());
			line.setPdpPriceDiscountPercentage(lineObj.getFreeOfCharge());
//			line.setLseLeaseIndicator(rs.getString("LSE_LEASE_INDICATOR"));
			line.setInpInitialProvisioningFlag(lineObj.getInitialProvisioning());
//			line.setSprSpecialRuleIndicator(rs.getString("SPR_SPECIAL_RULE_INDICATOR"));
			line.setRemCustomerRemarks(lineObj.getCustomerRemarks());
			line.setLastUpdateDate(lineObj.getLastUpdateDate());

			linesList.add(line);
		});
		
		
		if(linesList!=null && linesList.size()>0){
			messageBean.setOrderLines(linesList);
		}
		
		
		ObjectMapper mapper = new ObjectMapper();
		
		String orderAsString;
	    try {
			orderAsString = mapper.writeValueAsString(messageBean);
		
			JmsTemplate jmsTemplate = new JmsTemplate(remoteJms);
			logger.debug("sendRegisterMq host : "+ mqProperties.getHost());
			jmsTemplate.send(mqProperties.getOutQName(), new MessageCreator() {
	
				@Override
				public Message createMessage(Session session) throws JMSException {
					javax.jms.MapMessage mapMsg =  session.createMapMessage();
					logger.debug("Enter the createMessage...");
					mapMsg.setString("order", orderAsString);
					logger.info("message "+orderAsString);
					
					logger.debug("End the createMessage...");
					return mapMsg;
				}
			});
			
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	
	}
	//CHECK NOT NULL & NOT EQUALS
	public Boolean checkNull(Object fieldName) {
		if (fieldName != null && !fieldName.equals("")) {
			try {
				return true;
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return false;
	}
	
	public static boolean contains(String currency) {

	    for (Currency c : Currency.values()) {
	        if (c.name().equals(currency)) {
	            return true;
	        }
	    }

	    return false;
	}
	
	public static boolean containsIpVals(String ipVals) {
		if (ipVals.equals("Y")) {
            return true;
        }else if(ipVals.equals("N")) {
        	return true;
        }
		
		return false;
	}
	
	//Save order line details:
		public Order saveOrderLine(String customerPoNumber, OrderLine orderLine){
			Order orderObj = new Order();
			orderObj =  getOrderDetails(customerPoNumber);
			if((orderObj != null && orderObj.getOrderHeaderId()>0) &&
				(orderObj.getSubmittedDate() == null || orderObj.getSubmittedDate().equals("")) &&
			    orderLine != null ) {
				orderObj.getOrderLine().add(orderLine);
				Order order= validateOrdLine(orderObj);
	            
				if(order.getOrderLine()!=null && !order.getOrderLine().isEmpty()) {
					Set<OrderLine> orderLineList = new HashSet<OrderLine>();
					OrderLine ordLine = new OrderLine();
					orderLineList = order.getOrderLine();
					Iterator<OrderLine> iteratorList = orderLineList.iterator();
					while(iteratorList.hasNext()) {
						ordLine = iteratorList.next();
						ordLine.setOrder(order);
						ordLine.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
						orderLineRepository.save(ordLine);
					}
				}
				order.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
				orderRepository.save(order);
				logger.debug("Order line saved successfully...");
			}else {
				logger.debug("Order line not saved...");
				if(orderObj.getSubmittedDate()!= null && !orderObj.getSubmittedDate().equals("")) {
	     			throw new ValidationException("Record already submitted");
	     		}
				throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
			}
			return orderObj;
		}
		
		@Transactional
		public Order updateOrderLineDetails(String customerPoNumber,Integer lineId,OrderLine orderLine) {
			
			Order orderObj = new Order();
			orderObj =  getOrderDetails(customerPoNumber);
			OrderLine orderLineObj = new OrderLine();
			orderLineObj = orderLineRepository.getOne(lineId);
			
			if((orderObj != null && orderObj.getOrderHeaderId()>0) && 
					(orderObj.getSubmittedDate() == null || orderObj.getSubmittedDate().equals("")) &&
					(orderLineObj != null && orderLineObj.getOrderLineId() >0)) {

					orderObj.getOrderLine().forEach( line ->{
						
					if(line.getOrderLineId().equals(lineId)) {
		
						if (orderLine.getPartId() != null)
							line.setPartId(orderLine.getPartId());
						
						if (orderLine.getPartNumber() != null)
							line.setPartNumber(orderLine.getPartNumber());

						if (orderLine.getCustomerLineNumber() != null)
							line.setCustomerLineNumber(orderLine.getCustomerLineNumber());

						if (orderLine.getOrderedQuantity() != null)
							line.setOrderedQuantity(orderLine.getOrderedQuantity());

						if (orderLine.getPromiseDateStr() != null)
//							DateStr = changeDateFormat(orderLine.getPromiseDateStr());
							line.setPromiseDateStr(orderLine.getPromiseDateStr());

						if (orderLine.getCustomerRemarks() != null)
							line.setCustomerRemarks(orderLine.getCustomerRemarks());

						if (orderLine.getFreeOfCharge() != null)
							line.setFreeOfCharge(orderLine.getFreeOfCharge());

						if (orderLine.getInitialProvisioning() != null)
							line.setInitialProvisioning(
									orderLine.getInitialProvisioning());

						if (orderLine.getContractNumber() != null)
							line.setContractNumber(orderLine.getContractNumber());

						if (orderLine.getShipToSiteId() != null)
							line.setShipToSiteId(orderLine.getShipToSiteId());

						if (orderLine.getDelgRef() != null)
							line.setDelgRef(orderLine.getDelgRef());

						if (orderLine.getIsDelgFlag() != null)
							line.setIsDelgFlag(orderLine.getIsDelgFlag());

						if (orderLine.getDropshipIndicator() != null)
							line.setDropshipIndicator(orderLine.getDropshipIndicator());

						if (orderLine.getDropshipCustomer() != null)
							line.setDropshipCustomer(orderLine.getDropshipCustomer());

						if (orderLine.getDropshipAddress1() != null)
							line.setDropshipAddress1(orderLine.getDropshipAddress1());

						if (orderLine.getDropshipAddress2() != null)
							line.setDropshipAddress2(orderLine.getDropshipAddress2());

						if (orderLine.getDropshipAddress3() != null)
							line.setDropshipAddress3(orderLine.getDropshipAddress3());

						if (orderLine.getDropshipAddress4() != null)
							line.setDropshipAddress4(orderLine.getDropshipAddress4());

						if (orderLine.getDropshipCountry() != null)
							line.setDropshipCountry(orderLine.getDropshipCountry());

						if (orderLine.getDsRegn() != null)
							line.setDsRegn(orderLine.getDsRegn());

						if (orderLine.getDsZip() != null)
							line.setDsZip(orderLine.getDsZip());

						if (orderLine.getDropshipCpo() != null)
							line.setDropshipCpo(orderLine.getDropshipCpo());
					}
				});
				Order webOrder= validateOrdLine(orderObj);
				orderLineSave(webOrder,lineId);
//				webOrder.getOrderLine().forEach(line -> {
//					if(line.getOrderLineId().equals(lineId)) {
//					line.setOrderLineId(lineId);
//					line.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
//					orderLineRepository.save(line);
//					}
//				});
					
					
				orderObj.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
				orderRepository.save(orderObj);
				logger.debug("Order line updated successfully...");
				return orderObj;
			}else {
	     		logger.debug("Order not updated...");
	     		if(orderObj.getSubmittedDate()!= null && !orderObj.getSubmittedDate().equals("")) {
	     			throw new ValidationException("Record already submitted");
	     		}
				throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
			}
		}
		
	
		public void orderLineSave(Order webOrder,Integer lineId) {
			webOrder.getOrderLine().forEach(line -> {
				if(line.getOrderLineId().equals(lineId)) {
				line.setOrder(webOrder);
				line.setOrderLineId(lineId);
				line.setLastUpdateDate(new Timestamp(System.currentTimeMillis()));
				orderLineRepository.save(line);
				}
			});
		}
		//Fetch order line details:
		@SuppressWarnings("unchecked")
		public OrderLine getOrderLineDetails(String customerPoNumber, Integer lineId) {
			List<Order> orderObj = new ArrayList();
			orderObj =  orderRepository.findByCustomerPoNumber(customerPoNumber);
			OrderLine orderLineObj = new OrderLine();
			Set<OrderLine> orderLines = orderLineRepository.findByOrderLineId(lineId);
			if((orderObj != null && orderObj.get(0).getOrderHeaderId()>0) && 
					(orderObj.get(0).getSubmittedDate() == null || orderObj.get(0).getSubmittedDate().equals("")) &&
					(orderLines != null)) {
				orderLineObj.setOrderLineId(orderLines.iterator().next().getOrderLineId());
				orderLineObj.setPartId(orderLines.iterator().next().getPartId());
				orderLineObj.setPartNumber(orderLines.iterator().next().getPartNumber());
				orderLineObj.setCustomerLineNumber(orderLines.iterator().next().getCustomerLineNumber());
				orderLineObj.setOrderedQuantity(orderLines.iterator().next().getOrderedQuantity());
				if(orderLines.iterator().next().getPromiseDateStr()!= null) {
					String dateStr = changeDateFormat(orderLines.iterator().next().getPromiseDateStr());
					orderLineObj.setPromiseDateStr(dateStr);
				}
				orderLineObj.setCustomerRemarks(orderLines.iterator().next().getCustomerRemarks());
				if (orderLines.iterator().next().getFreeOfCharge() != null
						&& orderLines.iterator().next().getFreeOfCharge().equals("100")) {
					orderLines.iterator().next().setFreeOfCharge("Y");
				}
				orderLineObj.setInitialProvisioning(orderLines.iterator().next().getInitialProvisioning());
				orderLineObj.setContractNumber(orderLines.iterator().next().getContractNumber());
				orderLineObj.setShipToSiteId(orderLines.iterator().next().getShipToSiteId());
				orderLineObj.setDelgRef(orderLines.iterator().next().getDelgRef());
				orderLineObj.setDropshipIndicator(orderLines.iterator().next().getDropshipIndicator());
				orderLineObj.setDropshipCustomer(orderLines.iterator().next().getDropshipCustomer());
				orderLineObj.setDropshipAddress1(orderLines.iterator().next().getDropshipAddress1());
				orderLineObj.setDropshipAddress2(orderLines.iterator().next().getDropshipAddress2());
				orderLineObj.setDropshipAddress3(orderLines.iterator().next().getDropshipAddress3());
				orderLineObj.setDropshipAddress4(orderLines.iterator().next().getDropshipAddress4());
				orderLineObj.setDropshipCountry(orderLines.iterator().next().getDropshipCountry());
				orderLineObj.setDsRegn(orderLines.iterator().next().getDsRegn());
				orderLineObj.setDsZip(orderLines.iterator().next().getDsZip());
				orderLineObj.setDropshipCpo(orderLines.iterator().next().getDropshipCpo());
				orderLineObj.setLastUpdateDate(orderLines.iterator().next().getLastUpdateDate());
				orderLineObj.setPriShipmentPriority(orderLines.iterator().next().getPriShipmentPriority());
				orderLineObj.setOrder(null);
				return orderLineObj;
			}else {
				throw new RecordNotFoundException("lineId :-" + lineId);
			}
		}
		
		//Delete order line details:
		@Transactional
		public Integer deleteOrderLine(String customerPoNumber,Integer lineId) {
			if ((customerPoNumber != null && !customerPoNumber.equals("")) && (lineId != null && lineId >0) ) {
				List<Order> order = orderRepository.findByCustomerPoNumber(customerPoNumber);
				if(!order.isEmpty()) {
					if((order.get(0).getSubmittedDate()== null || order.get(0).getSubmittedDate().equals(""))) {
						
						Query query = em.createNativeQuery("DELETE FROM XMP_WEB_ORDER_LINES WHERE ORDER_LINE_ID = " + lineId);
						int returnVal = query.executeUpdate();
					
						logger.debug("Order line deleted successfully...");
					}else {
						throw new ValidationException("customerPoNumber-" + customerPoNumber +" "+ "is already submitted");
					}
				}else {
					logger.debug("Order line not deleted...");
					throw new RecordNotFoundException("customerPoNumber-" + customerPoNumber);
				}
			}else {
				logger.debug("Order line not deleted...");
				throw new RecordNotFoundException("lineId-" + lineId);
			}
			return lineId;
		}
}
