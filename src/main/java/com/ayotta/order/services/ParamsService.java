package com.ayotta.order.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ayotta.order.model.Parameters;
import com.ayotta.order.repositories.ParamsRepository;

@Service
public class ParamsService {
	
	private static final Logger logger = LoggerFactory.getLogger(ParamsService.class);
	
    @Autowired
    ParamsRepository paramsRepository;
	
	public List<Parameters> getParamValue(String paramName) {
		return paramsRepository.findByParameterName(paramName);
	}
}
