package com.ayotta.order.services;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ayotta.order.model.User;
import com.ayotta.order.repositories.UserRepository;
import com.ayotta.order.web.UserController;

@Service
public class UserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserRepository userRepository;
	
	public List<User> getAllMatchingUsers(String usrName){
		String userName = usrName.toUpperCase();
		List<User> listOfUsers = userRepository.findUsersWithPartOfName("%"+userName+"%");
		return listOfUsers;
	}
	
	public User getUserByUserName(String userName){
		User user = userRepository.findByUsrName(userName);
		return user;
	}
}
