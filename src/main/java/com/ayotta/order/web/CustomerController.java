package com.ayotta.order.web;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import com.ayotta.order.exception.RecordNotFoundException;
import com.ayotta.order.exception.ValidationException;
import com.ayotta.order.model.Customer;
import com.ayotta.order.model.DelegatedCustomer;
import com.ayotta.order.model.OrderEntryBean;
import com.ayotta.order.model.Parameters;
import com.ayotta.order.model.Part;
import com.ayotta.order.model.PartDetails;
import com.ayotta.order.model.PartPriceBean;
import com.ayotta.order.model.ShippingAddress;
import com.ayotta.order.services.CustomerService;
import com.ayotta.order.services.ParamsService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/customers")
@Api(value = "Customer Management", description = "Operations with customer", produces = "application/json")
@Validated
public class CustomerController {

	private static final Logger logger = LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	CustomerService customerService;
	
	@GetMapping("/")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Returns all matching customers.")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"),
			@ApiResponse(code = 400, message = "Invalid customer name/customer number Supplied"),
			@ApiResponse(code = 404, message = "Customer Not Found"),
			})
	public ResponseEntity<List<Customer>> getAllMatchingCustomers(@RequestParam(required = false,name="customerName") String customerName,
			@RequestParam(required = false,name="customerNumber") String customerNumber){
		List<Customer> listOfCustomers = customerService.getAllMatchingCustomers(customerName,customerNumber);
		if(listOfCustomers.isEmpty()) {
			throw new RecordNotFoundException("customer name :-" + customerName +" "+"customer number :-"+customerNumber);
		}
		logger.debug("LIST OF CUSTOMERS:" +listOfCustomers);
		return new ResponseEntity<List<Customer>>(listOfCustomers, HttpStatus.OK);
	}
	
	@GetMapping("/{customerId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Returns a customer by customer Id.")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid customer Id Supplied"),
			@ApiResponse(code = 404, message = "Customer Not Found"),
			})
	public ResponseEntity<Customer> getCustomerByCustId(@PathVariable("customerId") Long customerId) {
		Customer customer = customerService.getCustomerBycustId(customerId);
		if (customer == null)
		      throw new RecordNotFoundException("customerId-" + customerId);
		logger.debug("CUSTOMER OBJ:"+ customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.OK);
	}
	
	@GetMapping("/delegated-customers")
	@ResponseStatus(HttpStatus.OK)
//	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns a customer by customer Id.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No customer Available"),
			@ApiResponse(code = 405, message = "Method Not Allowed"),
			@ApiResponse(code = 200, message = "") })
	public ResponseEntity<List<DelegatedCustomer>> getDelegatedCustomers(@RequestParam("customerId") Long customerId,
			@RequestParam(required = false,name="customerName") String customerName,@RequestParam(required = false,name="cic") String cic) {
		Customer customer = customerService.getCustomerBycustId(customerId);
		if (customer == null)
		      throw new RecordNotFoundException("customerId-" + customerId);
		List<DelegatedCustomer> custList = customerService.getDelegatedCust(customerId,customerName,cic);
		if(custList.isEmpty()) {
			throw new RecordNotFoundException("customerId-" + customerId);
		}
		
		logger.debug("DELEG CUSTOMERS:"+ customer);
		return new ResponseEntity<List<DelegatedCustomer>>(custList, HttpStatus.OK);
	}
	
	@GetMapping("/shippingAddress")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns a shipping address by customer Id.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No shipping address Available"),
			@ApiResponse(code = 200, message = "") })
	public ResponseEntity<List<ShippingAddress>> getShippingAddress(@Valid @RequestParam("customerId") String customerId,
			Integer site, @RequestParam(required = false,name="divisionLine") String divisionLine){
		if(customerId!=null && !customerId.equals("")) {
			long custId = Long.valueOf(customerId);
			Customer customer = customerService.getCustomerBycustId(custId);
			if (customer == null)
				throw new RecordNotFoundException("customerId-" + customerId);
			logger.debug("SHIPPING ADDRESS:"+ customer);
			ShippingAddress shippingAddress = new ShippingAddress();
			List<ShippingAddress> shipList = customerService.getShippingAddress(customerId,site,divisionLine);
			if(shipList.isEmpty()) {
				throw new RecordNotFoundException("customerId-" + customerId);
			}
			return new ResponseEntity<List<ShippingAddress>>(shipList, HttpStatus.OK);
		}else {
			throw new ValidationException("Customer id cannot be empty");
		}
		
	}
	
	@GetMapping("{customerId}/dropshipment")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns a shipping address by customer Id.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No shipping address Available"),
			@ApiResponse(code = 200, message = "") })
	public boolean isDropShipmentAllowed(@PathVariable("customerId") Long customerId) {
		return customerService.isDropShipmentAllowed(customerId);
	}
	
	@GetMapping("{customerId}/parts/{partNumber}")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns all matching parts which is available for the customer.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No parts Available"),
			@ApiResponse(code = 200, message = "") })
	public List<Part> getMatchingParts(@PathVariable("customerId") String customerId,@PathVariable("partNumber") 
	         @Size(min = 4, message = "Please enter the first four digit") String partNumber){
		long custId = Long.valueOf(customerId);
		Customer customer = customerService.getCustomerBycustId(custId);
		if (customer == null)
		      throw new RecordNotFoundException("customerId- " + customerId);
		List<Part> partList = customerService.getAllParts(customerId, partNumber);
		if(partList.isEmpty()) {
			  throw new RecordNotFoundException("Part- " + partList);
		}
		logger.debug("MATCHING PARTS:"+ partList);
		return partList;
	}
	
	
	@GetMapping("/parts/status")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns part status which is available for the customer.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No parts Available"),
			@ApiResponse(code = 200, message = "") })
	public PartDetails getPartStatus(@Valid @RequestParam("customerId") Integer custId,@RequestParam("customerName") String customerName,
			@RequestParam(required = false,name="delgRef") String delgRef,@RequestParam("currency") String icr, @RequestParam("partId") Integer itemId){
		
		 OrderEntryBean orderEntryBean = new OrderEntryBean();
		 orderEntryBean.setCustId(custId);
		 orderEntryBean.setCustomerName(customerName);
		 orderEntryBean.setDelgRef(delgRef);
		 orderEntryBean.setIcr(icr);
		 orderEntryBean.setItemId(itemId);
		 PartDetails partDetails = customerService.getPartStatus(orderEntryBean);
		 logger.debug("PART STATUS:"+ partDetails);
		 return partDetails;
	}
	
	
}
