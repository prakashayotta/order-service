package com.ayotta.order.web;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ayotta.order.exception.RecordNotFoundException;
import com.ayotta.order.model.Customer;
import com.ayotta.order.model.Order;
import com.ayotta.order.model.OrderLine;
import com.ayotta.order.services.CustomerService;
import com.ayotta.order.services.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/orders")
@Api(value = "Order Management", description = "Operations with order", produces = "application/json")
public class OrderController {

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@Autowired
	OrderService orderService;

	@GetMapping("/{customerPoNumber}")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Get an order by order header Id")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Order> getWebOrder(
			@ApiParam("Customer PO number to retrieve an order object") @PathVariable("customerPoNumber") String customerPoNumber) {
		
			logger.debug("Enter the GET method CPO is :" + customerPoNumber);
			Order order = orderService.getOrderDetails(customerPoNumber);
			return new ResponseEntity<Order>(order,HttpStatus.OK);
	}

	@PostMapping("/")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Create an order")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"),
			@ApiResponse(code = 400, message = "Invalid Order Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			 })
	public ResponseEntity<Order> createWebOrder(
			@ApiParam(value = "Instructions for create order:\r\n" + 
					"01. Currently, Order create functionality supports only one order/header in an api call. So please use only one header (Customer ID, Purchase Order Number, Order Currency).\r\n" + 
					"02. The use of special characters is limited to the following for Purchase Order Number\r\n" + 
					"    / \\ . - _\r\n" + 
					"03. Please enter shipping address per line. \r\n"+
					"04. In case of using the same shipping address as previous line you do not need to enter the shipping address. \r\n"+
					"05. Please fill the Customer Required Date field in the format DD-MMM-YYYY.(Example:01-Jan-2019) \r\n" +
					"06. Please remove the default values 'string' from fields \r\n") 
			@Valid @RequestBody Order webOrder) {
		
			logger.debug("Enter the order header POST method");
			
			orderService.save(webOrder);
			
		return new ResponseEntity<Order>(HttpStatus.OK);
	}

	@PutMapping("/")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update an order")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Order> updateWebOrder(
			@ApiParam(value = "Instructions for update order:\r\n" + 
					"01. Currently, Order create functionality supports only one order/header in an api call. So please use only one header (Customer ID, Purchase Order Number, Order Currency).\r\n" + 
					"02. The use of special characters is limited to the following for Purchase Order Number\r\n" + 
					"    / \\ . - _\r\n" + 
					"03. Please enter shipping address per line. \r\n"+
					"04. In case of using the same shipping address as previous line you do not need to enter the shipping address. \r\n"+
					"05. Please fill the Customer Required Date field in the format DD-MMM-YYYY.(Example:01-Jan-2019) \r\n" +
					"06. Please remove the default values 'string' from fields \r\n") 
			@Valid @RequestBody Order webOrder) throws ParseException {
		
			logger.debug("Enter the UPDATE method :" + webOrder.toString());
			orderService.updateOrderDetails(webOrder);
			
		return new ResponseEntity<Order>(HttpStatus.OK);
	}
	
	@DeleteMapping("/{customerPoNumber}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete an order")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Integer> delete(@ApiParam("Customer PO number to delete an order object")
		@PathVariable("customerPoNumber") String customerPoNumber) {
		
			logger.debug("Enter the DELETE method CPO:" + customerPoNumber);
			orderService.deleteOrderDetails(customerPoNumber);
		
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
	
	@PostMapping("/submit/{customerPoNumber}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Submit an order")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Integer> submitOrder(@ApiParam("Order header Id to submit order object")
		@PathVariable("customerPoNumber") String customerPoNumber) {
		
			logger.debug("Enter the submit method :" + customerPoNumber);
			orderService.submitOrder(customerPoNumber);
		
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
	
	//Operations with order line
	@PostMapping("/{customerPoNumber}/lines")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Create an order line")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"),
			@ApiResponse(code = 400, message = "Invalid Order line Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			 })
	public ResponseEntity<Order> createOrderLine(
			@Valid @PathVariable("customerPoNumber") String customerPoNumber,
			@Valid @RequestBody OrderLine orderLine) {
		
			logger.debug("Enter the order line POST method");
			
			orderService.saveOrderLine(customerPoNumber,orderLine);
			
		return new ResponseEntity<Order>(HttpStatus.OK);
	}
	
	@PutMapping("/{customerPoNumber}/lines/{lineId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Update an order line")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Order> updateWebOrderLine(
			
			@Valid @PathVariable("customerPoNumber") String customerPoNumber,@Valid @PathVariable("lineId") Integer lineId,
			@Valid @RequestBody OrderLine orderLine) throws ParseException {
		
			logger.debug("Enter the order line UPDATE method :" + customerPoNumber);
			orderService.updateOrderLineDetails(customerPoNumber,lineId,orderLine);
			
		return new ResponseEntity<Order>(HttpStatus.OK);
	}
	
	@GetMapping("/{customerPoNumber}/lines/{lineId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Get an order line by order header Id and order line Id")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<OrderLine> getWebOrderLine(
			@PathVariable("customerPoNumber") String customerPoNumber,
			@PathVariable("lineId") Integer lineId) {
		
			logger.debug("Enter the order line GET method lineId is :" + lineId);
			OrderLine orderLine = orderService.getOrderLineDetails(customerPoNumber,lineId);
			return new ResponseEntity<OrderLine>(orderLine,HttpStatus.OK);
	}
	
	@DeleteMapping("/{customerPoNumber}/lines/{lineId}")
	@ResponseStatus(HttpStatus.OK)
	@ApiOperation(value = "Delete an order")
	@ApiResponses(value = { 
			@ApiResponse(code = 200, message = "Successful Operation"), 
			@ApiResponse(code = 400, message = "Invalid Order Header ID Supplied"),
			@ApiResponse(code = 404, message = "Order Not Found")
			})
	public ResponseEntity<Integer> deleteLine(@ApiParam("Order header Id to delete order object")
		@PathVariable("customerPoNumber") String customerPoNumber,
		@PathVariable("lineId") Integer lineId) {
		
			logger.debug("Enter the order  line DELETE method headerId is :" + lineId);
			orderService.deleteOrderLine(customerPoNumber,lineId);
		
		return new ResponseEntity<Integer>(HttpStatus.OK);
	}
	
}
