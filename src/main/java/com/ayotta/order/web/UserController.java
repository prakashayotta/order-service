package com.ayotta.order.web;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Email;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ayotta.order.exception.RecordNotFoundException;
import com.ayotta.order.model.User;
import com.ayotta.order.services.UserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/users")
@Api(value = "User Management", description = "Operations with user", produces = "application/json")
@Validated
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	UserService userService;
	
	@GetMapping("/matchingUsers/{userName}")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns all matching users.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No user Available"),
			@ApiResponse(code = 200, message = "") })
	public List<User> getAllMatchingUsers(@PathVariable("userName") String userName){
		List<User> listOfUsers = userService.getAllMatchingUsers(userName);
		logger.debug("LIST OF USERS:" +listOfUsers);
		return listOfUsers;
	}
	
	@GetMapping("/{userName}")
	@ResponseStatus(HttpStatus.OK)
	@ApiModelProperty(required = true)
	@ApiOperation(value = "Returns a user by user name.")
	@ApiResponses(value = { @ApiResponse(code = 400, message = "No user Available"),
			@ApiResponse(code = 200, message = "") })
	public User getUserByUserName(@PathVariable("userName") @Email String userName){
		User user = userService.getUserByUserName(userName);
		if (user == null)
		      throw new RecordNotFoundException("User Name-" + userName);
		System.out.println(user);
		logger.debug("USER OBJ :" +user);
		return user;
	}
	
	
}
