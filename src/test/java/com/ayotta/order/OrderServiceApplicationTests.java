package com.ayotta.order;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import com.ayotta.order.model.Customer;
import com.ayotta.order.model.EtaSapCSales;
import com.ayotta.order.model.Parameters;
import com.ayotta.order.model.PartDetails;
import com.ayotta.order.model.Stock;
import com.ayotta.order.model.XxmSpParams;
import com.ayotta.order.repositories.ParamsRepository;
import com.ayotta.order.repositories.PartDetailsRepository;
import com.ayotta.order.repositories.PartRepository;
import com.ayotta.order.repositories.StockRepository;
import com.ayotta.order.repositories.XxmSpParamsRepo;
import com.ayotta.order.services.ParamsService;

import oracle.jdbc.internal.OracleTypes;

import java.net.HttpURLConnection;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceApplicationTests {

	private static Integer ORG_ID = -1;
	
	@Autowired
	XxmSpParamsRepo xxmSpParamsRepo;
	
	@Autowired
	PartRepository partRepo;
	
	@Autowired
	PartDetailsRepository partDetailsRepo;
	
	@Autowired
	StockRepository stockRepo;
	
	@Autowired
	ParamsService paramsService;
	
	@Autowired
    private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Autowired
    JdbcTemplate jdbcTemplate;
	
//	@Test
//	public void testparameterJdbcTemplate() {
//		EtaSapCSales sapCustSales = new EtaSapCSales();
//		try {
//			Connection connection = jdbcTemplate.getDataSource().getConnection();
//			CallableStatement cstmt = connection
//					.prepareCall("CALL ETA.get_cust_sales_data(?,?,?,?,?,?,?)");
//			cstmt.setInt(1, 13021);
//			cstmt.setLong(2, 306);
//			cstmt.setString(3, "07");
//			cstmt.registerOutParameter(4, OracleTypes.VARCHAR);
//			cstmt.registerOutParameter(5, OracleTypes.VARCHAR);
//			cstmt.registerOutParameter(6, OracleTypes.VARCHAR);
//			cstmt.registerOutParameter(7, OracleTypes.VARCHAR);
//			cstmt.execute();
//			sapCustSales.setDistributionChannel(cstmt.getString(4));
//			sapCustSales.setCivilMilitaryIndicator(cstmt.getString(5));
//			sapCustSales.setPriceList(cstmt.getString(6));
//			sapCustSales.setKeyCustomer(cstmt.getString(7));
//			cstmt.close();
//			
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//    }
	
//	@Test
//	public void testparameterJdbcTemplate() {
//		Long id = 13021l;
//		System.out.println("ENTER IN testparameterJdbcTemplate");
//		Optional<Customer> customer = namedParameterJdbcTemplate.queryForObject("select * from eta_sap_customers where CUSTOMER_ID = :id",
//				new MapSqlParameterSource("id", id),
//				(rs, rowNum) -> 
//		        Optional.of(new Customer(
//                rs.getLong("CUSTOMER_ID"),
//                rs.getString("CUSTOMER_NAME")
//             ))
//		);
//		System.out.println("o/p :" + customer );
//	}
//	@Test
//	public void contextLoads() {
//		XxmSpParams param = xxmSpParamsRepo.findByParameterName("XXM_MASTER_ORG");
//		ORG_ID = Integer.valueOf(param.getParameterValue());
//		System.out.println("GETORGMASTERID :" + ORG_ID);
//	}

	
//	@Test
//	public void checkPartDetails() {
//	
//		 PartDetails partDetail = partDetailsRepo.getPart(611578, 306, "02");
//		 System.out.println("PART DETAIL :"+ partDetail);
//	}
	
//	@Test
//	public void checkResellerInfo() {
//		//613213 saywell part
// 		String str = partDetailsRepo.getResellerInfor(613213);
//		 System.out.println("RESELLER INFO :"+ str);
//	}
	
	/*@Test
	public void checkStockPart() {
 		List<Stock> stockList= stockRepo.getStockPart(613200);
		 System.out.println("STOCK PART :"+ stockList);
	}*/
	
//	@Test
//	public void checkDuesInBeforeCLTAll() {
// 		partDetailsRepo.getDuesInBeforeCLTAll(itemId, leadTime, orgId)
//	}
	
//	@Test
//	public void checkParams() {
//		List<Parameters> params= paramsService.getParamValue("THIRD_PARTY_MESSAGE_SAYWELL");
//	    params.forEach(param -> System.out.println(param.getParameterValue()));
//	}
	private final String USER_AGENT = "Mozilla/5.0";
	private static final String POST_PARAMS = "content=Content-02";
	@Test
	public void checkStatus() throws IOException {
		
//		try {
//			URL url = new URL("http://localhost:7077/api/notification/get/901");
//			BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
//			String strTemp = "";
//			while (null != (strTemp = br.readLine())) {
//				System.out.println(strTemp);
//			}
//		} catch (Exception ex) {
//			ex.printStackTrace();
//		}
	//	http://localhost:7077/api/notification/post
		
		/*try {
			
			HttpURLConnection urlConn;
			URL mUrl = new URL("http://localhost:7077/api/notification/post");
			urlConn = (HttpURLConnection) mUrl.openConnection();
			String query = "content:Content-02";
			//query is your body
			urlConn.addRequestProperty("Content-Type", "application/" + "POST");
			if (query != null) {
				urlConn.setRequestProperty("Content-Length", Integer.toString(query.length()));
				urlConn.getOutputStream().write(query.getBytes("UTF8"));
				urlConn.setDoOutput(true);
			}
		}catch(Exception e) {
			
		}*/
//		try {
//	    //URL url = new URL(null, "https://redmine.xxx.cz/time_entries.xml", new sun.net.www.protocol.https.Handler());
//		String url = "http://localhost:7077/api/notification/post";
//		URL obj = new URL(null,url,new sun.net.www.protocol.https.Handler());
//		HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
//
//		//add reuqest header
//		con.setRequestMethod("POST");
//		con.setRequestProperty("User-Agent", USER_AGENT);
//		con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
//
//		String urlParameters = "sn=C02G8416DRJM&cn=&locale=&caller=&num=12345";
//		
//		// Send post request
//		con.setDoOutput(true);
//		DataOutputStream wr = new DataOutputStream(con.getOutputStream());
//		wr.writeBytes(urlParameters);
//		wr.flush();
//		wr.close();
//
//		int responseCode = con.getResponseCode();
//		System.out.println("\nSending 'POST' request to URL : " + url);
//		System.out.println("Post parameters : " + urlParameters);
//		System.out.println("Response Code : " + responseCode);
//
//		BufferedReader in = new BufferedReader(
//		        new InputStreamReader(con.getInputStream()));
//		String inputLine;
//		StringBuffer response = new StringBuffer();
//
//		while ((inputLine = in.readLine()) != null) {
//			response.append(inputLine);
//		}
//		in.close();
//		
//		//print result
//		System.out.println(response.toString());
//		}catch (Exception e) {
//			e.printStackTrace();
//		}
		
		/*try {
			String json = "{\"content\": \"Content-02\",\n" + 
					"\"endDate\": \"20-07-2019\",\n" + 
					"\"groupId\": 485,\n" + 
					"\"shortDesc\": \"Description-02\",\n" + 
					"\"startDate\": \"18-07-2019\",\n" + 
					"\"title\": \"Title-02\"}";
			
			URL obj = new URL("http://localhost:7077/api/notification/post");
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			
			// For POST only - START
			con.setDoOutput(true);
			OutputStream os = con.getOutputStream();
			os.write(json.getBytes());
			os.flush();
			os.close();
			// For POST only - END
			
			int responseCode = con.getResponseCode();
			System.out.println("POST Response Code :: " + responseCode);
			
			if (responseCode == HttpURLConnection.HTTP_OK) { //success
				BufferedReader in = new BufferedReader(new InputStreamReader(
						con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				
				// print result
				System.out.println(response.toString());
			} else {
				System.out.println("POST request not worked");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		//worked
//		try {
//			URL url = new URL ("http://localhost:7077/api/notification/post");
//			HttpURLConnection con = (HttpURLConnection)url.openConnection();
//			con.setRequestMethod("POST");
//			con.setRequestProperty("Content-Type", "application/json; utf-8");
//			con.setRequestProperty("Accept", "application/json");
//			con.setDoOutput(true);
//		//	String jsonInputString = "{'name': 'Upendra', 'job': 'Programmer'}";
//			String jsonInputString = "{\"content\": \"Content-02\",\n" + 
//			"\"endDate\": \"20-07-2019\",\n" + 
//			"\"groupId\": 485,\n" + 
//			"\"shortDesc\": \"Description-02\",\n" + 
//			"\"startDate\": \"18-07-2019\",\n" + 
//			"\"title\": \"Title-02\"}";
//			try(OutputStream os = con.getOutputStream()) {
//			    byte[] input = jsonInputString.getBytes("utf-8");
//			    os.write(input, 0, input.length);           
//			}
//			
//			try(BufferedReader br = new BufferedReader(
//					  new InputStreamReader(con.getInputStream(), "utf-8"))) {
//					    StringBuilder response = new StringBuilder();
//					    String responseLine = null;
//					    while ((responseLine = br.readLine()) != null) {
//					        response.append(responseLine.trim());
//					    }
//					    System.out.println(response.toString());
//					}
//		} catch (MalformedURLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
		HttpClient client = new DefaultHttpClient();
        HttpConnectionParams.setConnectionTimeout(client.getParams(), 1000); //Timeout Limit
        HttpResponse response;
        JSONObject json = new JSONObject();

            try {
                HttpPost post = new HttpPost("http://localhost:7077/api/notification/post");
                json.put("content", "Content-02");
                json.put("endDate", "20-07-2019");
                json.put("groupId", 485);
                json.put("shortDesc", "Description-02");
                json.put("startDate", "19-07-2019");
                json.put("title", "Title-02");
                StringEntity se = new StringEntity(json.toString());
                se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                post.setEntity(se);
                response = client.execute(post);

                /*Checking response */
                if (response != null) {
                	System.out.println(response.getStatusLine().getStatusCode());
                    
                }

            } catch (Exception e) {
                e.printStackTrace();
           
            }
		}
	}

